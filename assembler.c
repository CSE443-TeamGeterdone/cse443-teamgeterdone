#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


struct Node {
	int line_id;
	bool add, sub;
	char *var, *var2, *var3;
	int arg1, arg2, arg3;
	int code;
	struct Node *next;
	int offset;
};

int line_id = 1;
struct Node* head;
void init_list(){
	struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));
	printf("\nCreating new node\n");
	new_node->line_id = line_id;
	new_node->add = false;
	new_node->sub = false;
	new_node->var = "";
	new_node->var2 = "";
	new_node->var3 = "";
	new_node->arg1 = -1;
	new_node->arg2 = -1;
	new_node->arg3 = -1;
	new_node->code = -1;
	new_node->next = NULL;
	new_node->offset = 0;
	line_id++;

	head = (struct Node*) malloc(sizeof(struct Node));
	head = new_node;
}

void add_new_line(){
	if (line_id == 1) init_list();
	else {
		struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));
		printf("\nInserting new node at line id: %i\n", line_id);
		new_node->line_id = line_id;
		new_node->add = false;
		new_node->sub = false;
		new_node->var = "";
		new_node->var2 = "";
		new_node->var3 = "";
		new_node->arg1 = -1;
		new_node->arg2 = -1;
		new_node->arg3 = -1;
		new_node->code = -1;
		new_node->next = NULL;
		new_node->offset = 0;
	
		line_id++;
		new_node->next = head;
		head = new_node;
	}
}

void set_offset(int off){
	head->off_set = off;
}

void set_add(){
	head->add = true;
}

void set_sub(){
	head->sub = true;
}

void set_var1(char *var){
	head->var = var;
}

void set_var2(char *var2){
	head->var2 = var2;
}

void set_arg1(int a1){
	head->arg1 = a1;
}

void set_arg2(int a2){
	head->arg2 = a2;
}

struct Node* reverse_list(){
	struct Node* prev = NULL;
	struct Node* current = head;
	struct Node* next;

	while (current != NULL){
		next = current->next;
		current->next = prev;
		prev = current;
		current = next;
	}
	return prev;
}



void write_assembly_file(){
	struct Node* node = reverse_list();
	while(node != NULL){
	switch(node->code){
		case 1: //A = CONSTANT

			break;
		case 2: //T# = CONSTANT


			break;
		case 3: //t# = t# + constant   or    t# = constant + t#

			break;
		case -1:
			printf("\nUNITITIALIZED NODE CODE !!!!!!!!!\n");
	
	}
	
	
	node = node->next;
	}



}
