#ifndef ASSEMBLER_H
#define ASSEMBLER_H

/*

 * Node here is a container to represent a line of intermediate code
 *
 *It will store information on that line of IR, 
 *and hold a pointer to the next line of the IR file.

*/

struct Node; 
extern struct Node *head; // beginning of IR file info
extern int line_id; //tracks line numbers

//functions used to create/add to linked list
//NOTE: No info is assigned here, only handles Node creation
void init_list();
void add_new_line();


//These are functions to add info to Node
//These functions always change info in current node, so be careful
void set_off_set(int);
void set_add();
void set_sub();
void set_var1(char *);
void set_var2(char *);
void set_arg1(int);
void set_arg2(int);


struct Node* reverse_list();
void write_assembly_file();
#endif
