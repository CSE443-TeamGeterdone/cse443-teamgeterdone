#include <stdio.h>
#include <stdlib.h>

#include "error.h"

// DECLARATION ERRORS
char *throw_dec_error_1(int line, int col, char *name, FILE *file){
    if(file != NULL) {
        char result[200];
        snprintf(result, sizeof(result), "\n** ERROR:%i:%i: the name '%s', used here as a type, has not been declared at this point in the program.\n", line, col, name);
        return result;
    }
    else {
        printf("\n** ERROR:%i:%i: the name '%s', used here as a type, has not been declared at this point in the program.\n", line, col, name);
        return "FIle Not Found"; }
}

void throw_dec_error_2(int line, int col, char *name, FILE *file){
if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: the name %s, used here as a variable name, has not yet been declared in the program.\n", line, col, name);
else printf("\n** ERROR:%i:%i: the name %s, used here as a variable name, has not yet been declared in the program.\n", line, col, name);
}


void throw_no_ftype_error(int line, int col, char *name, FILE *file){
if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: the name '%s', used here as a function type, has not been declared at this point in the program.\n", line, col, name);
else printf("\n** ERROR:%i:%i: the name '%s', used here as a function type, has not been declared at this point in the program.\n", line, col, name);

}

void throw_bool_error(int line, int col, FILE *file){
	if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: there is an ill formed boolean expression where one is required\n", line, col);
	else printf("\n** ERROR:%i:%i: there is an ill formed boolean expression where one is required\n", line, col);
}

void throw_int_expression_error(int line, int col, FILE *file){
        if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: there is an ill formed integer expression where one is required\n", line, col);
	else printf("\n** ERROR:%i:%i: there is an ill formed integer expression where one is required\n", line, col);
}

void throw_switch_int_error(int line, int col, FILE *file){
	if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: Non integer expression for switch statement where integer is required\n", line, col);
	else printf("\n** ERROR:%i:%i: Non integer expression for switch statement where integer is required\n", line, col);

}

void throw_switch_case_error(int line, int col, FILE *file){
        if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: Non integer constant for switch case where integer is required\n", line, col);
        else printf("\n** ERROR:%i:%i: Non integer constant for switch case where integer is required\n", line, col);
}

void throw_itr_error(int line, int col, FILE *file){
        if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: expression for i2r is not integer\n", line, col);
        else printf("\n** ERROR:%i:%i: expression for i2r is not integer\n", line, col);
}

void throw_rti_error(int line, int col, FILE *file){
        if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: expression for r2i is not real\n", line, col);
        else printf("\n** ERROR:%i:%i: expression for r2i is not real\n", line, col);
}

void throw_real_expression_error(int line, int col, FILE *file){
        if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: ill formed real expression\n", line, col);
        else printf("\n** ERROR:%i:%i: ill formed real expression\n", line, col);
}

void throw_rem_error(int line, int col, FILE *file){
	if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: non integer modulus expression error\n", line, col);
	else printf("\n** ERROR:%i:%i: non integer modulus expression error\n", line, col);
}

void throw_math_expression_error(int line, int col, FILE *file){
        if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: ill formed mathematical expression\n", line, col);
        else printf("\n** ERROR:%i:%i: ill formed mathematical expression\n", line, col);
}

void throw_non_heap_type_error(int line, int col, FILE *file){

	if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: expected a type to be allocated on the heap\n", line, col);
	else printf("\n** ERROR:%i:%i: expected a type to be allocated on the heap\n", line, col);
}

void throw_int_assignment_error(int line, int col, FILE *file){
	if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: non integer expression for integer assignment\n", line, col);
	else printf("\n** ERROR:%i:%i: non integer expression for integer assignment\n", line, col);
}

void throw_real_assignment_error(int line, int col, FILE *file){
	if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: non real expression for real assignment\n", line, col);
	else printf("\n** ERROR:%i:%i: non real expression for real assignment\n", line, col);
}

void throw_bool_assignment_error(int line, int col, FILE *file){
	if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: non Boolean expression for Boolean assignment\n", line, col);
	else printf("\n** ERROR:%i:%i: non Boolean expression for Boolean assignment\n", line, col);
}

void throw_char_assignment_error(int line, int col, FILE *file){
	if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: non character expression for character assignment\n", line, col);
	else printf("\n** ERROR:%i:%i: non character expression for character assignment\n", line, col);
}

void throw_string_assignment_error(int line, int col, FILE *file){
	if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: non string expression for string assignment\n", line, col);
	else printf("\n** ERROR:%i:%i: non string expression for string assignment\n", line, col);
}

void throw_assignment_error(int line, int col, FILE *file){
	if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: Assignment has conflicting types\n", line, col);
	else printf("\n** ERROR:%i:%i: Assignment has conflicting types\n", line, col);
}

void throw_function_return_error(int line, int col, FILE *file){
	if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: incorrect function return type\n", line, col);
	else printf("\n** ERROR:%i:%i: incorrect function return type\n", line, col);

}

void throw_rtype_error(int line, int col, FILE *file){
	if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: expected expression to be rtype\n", line, col);
	 else printf("\n** ERROR:%i:%i: expected expression to be rtype\n", line, col);
}
