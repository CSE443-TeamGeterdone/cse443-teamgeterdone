#ifndef ERROR_H
#define ERROR_H

#include <stdio.h>
#include <stdlib.h>


//******************SYNTAX ERRORS*****************************

char *throw_dec_error_1(int, int, char *, FILE *); // type not declared error
void throw_dec_error_2(int, int, char *, FILE *); //variable not declared
void throw_no_ftype_error(int, int, char *, FILE *); // ftype not declared

//*****************SEMANTICS ERRORS***************************

void throw_bool_error(int, int, FILE *); 
void throw_int_expression_error(int, int, FILE *);
void throw_switch_int_error(int, int, FILE *);
void throw_switch_case_error(int, int, FILE *);
void throw_itr_error(int, int, FILE *);
void throw_rti_error(int, int, FILE *);
void throw_real_expression_error(int, int, FILE *);
void throw_rem_error(int, int, FILE *);
void throw_math_expression_error(int, int, FILE *);

void throw_non_heap_type_error(int, int, FILE *);

void throw_int_assignment_error(int, int, FILE *);
void throw_real_assignment_error(int, int, FILE *);
void throw_bool_assignment_error(int, int, FILE *);
void throw_char_assignment_error(int, int, FILE *);
void throw_string_assignment_error(int, int, FILE *);
void throw_assignment_error(int, int, FILE *);

void throw_function_return_error(int, int, FILE *);
void throw_rtype_error(int, int, FILE *);
#endif
