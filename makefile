#bison -v option is used to produce the filename.output file, which shows the different states of evaluating a program based on the grammar, and will elaborate on where any shift/reduce or reduce/reduce errors are occuring


#grammar with no shift/reduce or reduce/reduce errors
make:
	make clean
	flex lexer.lex
	bison -v parser.y
	gcc -o compiler symbol_table.c scope_pass.c error.c parser.tab.c runner.c

clean:
	rm -f *.o
	rm -f a.out
	rm -f lexer
	rm -f lex.yy.c
	rm -f *.tab.c
	rm -f *.output
	rm -f compiler
	rm -f prog.*
