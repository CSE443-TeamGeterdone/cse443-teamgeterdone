#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

FILE *opt = NULL;

int inputLineNumbers = 0;
char textInput[999][999];
char inputList[999][999][999];
int inputListLengthEachLine[999];

char bag[99][99];
int bagSize;

struct Label {
    char *name;
    int line[99];
    int lineSize;
    struct Label* next;
    
    char *value;
};

struct Label* head;
int lsize = 0;

void initt(char *n){
    struct Label* new_Label = (struct Label*) malloc(sizeof(struct Label));
    new_Label->name = n;
    
    head = (struct Label*) malloc(sizeof(struct Label));
    head = new_Label;
}

void insert_label(char *n) {//inserts Label at head only for now
    if (lsize == 0) {
        initt(n);
        lsize++;
    }
    else {
        struct Label* new_Label = (struct Label*) malloc(sizeof(struct Label));
        new_Label->name  = n;
        new_Label->next = head;
        head = new_Label;
        lsize++;
    }
}

struct Label* find_label(char *n){
    struct Label *label = head;
    while (label != NULL){
        if (strcmp(label->name, n) == 0) return label;
        else label = label->next;
    }
    return NULL;
};

void split_input_each_line() {
    int i = 0;
    while (i < inputLineNumbers) {
        char *temp = strtok(textInput[i], " ");
        int j = 0;
        while (temp != NULL) {
            strcpy(inputList[i][j], temp);
            temp = strtok(NULL, " \n");
            j++;
        }
        inputListLengthEachLine[i] = j;
        i++;
    }
}

void flow_of_control() {
    int i = 0;
    while (i < inputLineNumbers) {  // Access line
        char* labelStart = strchr(inputList[i][0], ':');
        int j = 0;
        while (j < inputListLengthEachLine[i]) { // Read every element in that line
            if (strcmp(inputList[i][j], "goto") == 0) {
                char *labelName;
                labelName = inputList[i][j+1];
                if (find_label(labelName) == NULL) {    // Add new label that is not existed yet
                    insert_label(labelName);
                }
                int k = find_label(labelName)->lineSize; // Current size of the array of the label
                find_label(labelName)->line[k] = i; // Store line number that the label is called
                find_label(labelName)->lineSize++; // increase size of line array
                break;
            }
            j++;
        }
        
        if (labelStart != NULL) {   // Label detected
            int temp1;
            temp1 = (int)(labelStart - inputList[i][0]);
            char *labelName;
            labelName = strtok(inputList[i][0], ":");

            if (find_label(labelName) != NULL) {
                if (find_label(labelName)->lineSize > 0) {
                    if (inputListLengthEachLine[i] == 1) {  // Those labels with empty instruction on the same line
                        if (strcmp(inputList[i+1][0], "exit") != 0) { // No need to do exit
                            if (strcmp(inputList[i+1][0], "goto") == 0) { // check for jump-to-jump
                                int k = find_label(labelName)->lineSize;
                                int l = 0;
                                while (l < k) {
                                    int m = find_label(labelName)->line[l]; // row that call labelName
                                    strcpy(inputList[m][inputListLengthEachLine[m]], inputList[i+1][1]);
                                    
                                    find_label(labelName)->lineSize-=1;
                                    
                                    if (find_label(inputList[i+1][1]) != NULL) {
                                        int n = find_label(inputList[i+1][1])->lineSize;
                                        find_label(inputList[i+1][1])->line[n] = m;
                                        find_label(inputList[i+1][1])->lineSize++;
                                    }
                                    inputListLengthEachLine[m]++;
                                    l++;
                                }
                                memset(find_label(labelName)->line, 0, sizeof find_label(labelName)->line);
                            }
                            else {
                                char* labelStart1 = strchr(inputList[i+1][0], ':');
                                if (labelStart1 != NULL) {
                                    char *labelName1;
                                    labelName1 = strtok(inputList[i+1][0], ":");
                                    if (find_label(labelName1) == NULL) insert_label(labelName1);
                                    int k = find_label(labelName)->lineSize;
                                    int l = 0;
                                    while (l < k) {
                                        int m = find_label(labelName)->line[l]; // row that call labelName
                                        strcpy(inputList[m][inputListLengthEachLine[m]], labelName1);
                                        find_label(labelName)->lineSize-=1;
                                        
                                        int n = find_label(labelName1)->lineSize;
                                        find_label(labelName1)->line[n] = m;
                                        find_label(labelName1)->lineSize++;
                                        inputListLengthEachLine[m]++;
                                        l++;
                                    }
                                    memset(find_label(labelName)->line, 0, sizeof find_label(labelName)->line);
                                    strcat(labelName1, ":");
                                }
                            }
                        }
                    }
                    else if (inputListLengthEachLine[i] > 1) {
                        if (strcmp(inputList[i][1], "exit") != 0) { // No need to do exit
                            if (strcmp(inputList[i][1], "goto") == 0) { // check for jump-to-jump
                                int k = find_label(labelName)->lineSize;
                                int l = 0;
                                while (l < k) {
                                    int m = find_label(labelName)->line[l]; // row that call labelName
                                    strcpy(inputList[m][inputListLengthEachLine[m]], inputList[i][2]);
                                    
                                    find_label(labelName)->lineSize-=1;
                                    
                                    if (find_label(inputList[i][2]) != NULL) {
                                        int n = find_label(inputList[i][2])->lineSize;
                                        find_label(inputList[i][2])->line[n] = m;
                                        find_label(inputList[i][2])->lineSize++;
                                    }
                                    inputListLengthEachLine[m]++;
                                    l++;
                                }
                                memset(find_label(labelName)->line, 0, sizeof find_label(labelName)->line);
                            }
                        }
                    }
                }
            } else {
                insert_label(labelName);
            }
            strcat(labelName, ":");
        }
        i++;
    }
    i = 0;
    while (i < inputLineNumbers) {
        if  (strcmp(inputList[i][0], "goto") == 0) {
            char* label_or_tab2 = strchr(inputList[i+1][0], ':');
            if (label_or_tab2 != NULL) {
                char *labelName2;
                labelName2 = strtok(inputList[i+1][0], ":");
                int j = inputListLengthEachLine[i] - 1;

                if (strcmp(labelName2, inputList[i][j]) == 0) {
                    i++;
                }
                strcat(labelName2, ":");
            }
        }
        
        char* label_or_tab = strchr(inputList[i][0], ':');
        if (label_or_tab == NULL) {
            fprintf(opt, "    ");
        } else {
            int temp1;
            temp1 = (int)(label_or_tab - inputList[i][0]);
            char *labelName;
            labelName = strtok(inputList[i][0], ":");
            if (isalpha(labelName[0]) != 0 && isdigit(labelName[1]) != 0) {
                if (find_label(labelName) != NULL) {
                    if (find_label(labelName)->lineSize == 0) {
                        if (inputListLengthEachLine[i] > 1) i++;
                        else {
                            if (inputListLengthEachLine[i] == 1 && strcmp(inputList[i+1][0], "goto") == 0) i+=2;
                            else { i+=1; }
                        }
                    }
                } else {
                    i++;    // Eliminate those that is never called as find_label return NULL
                    fprintf(opt, "    ");
                }
            }
            strcat(labelName, ":");
        }
        
        int j = 0;
        while (j < inputListLengthEachLine[i]) {
            fprintf(opt, "%s ", inputList[i][j]);
            if (strcmp(inputList[i][j], "goto") == 0) {
                j++;
                while (strcmp(inputList[i][j+1], "") != 0) {
                    j++;
                }
                j--;
            }
            j++;
        }
        fprintf(opt, "\n");
        i++;
    }
}

void eliminate_unreachable_code() {
    int i = 0;
    while (i < inputLineNumbers) {
        if (strcmp(inputList[i][0], "if") == 0 && inputListLengthEachLine[i] < 5) {
            if (strcmp(inputList[i+1][0], "goto") == 0) {
                fprintf(opt, "    ifFalse %s goto %s", inputList[i][1], inputList[i+1][1]);
                strcpy(bag[bagSize], inputList[i][3]);
                bagSize++;
                i++;
            }
            else if (strcmp(inputList[i][1], "true") == 0) {
                char labelTemp[99];
                strcpy(labelTemp, inputList[i][3]);
                strcat(labelTemp, ":");
                int ii = i;
                while (ii < inputLineNumbers) {
                    if (strcmp(labelTemp, inputList[ii][0]) == 0) {
                        fprintf(opt, "    goto %s", inputList[i][3]);
                        i = ii - 1;
                        break;
                    }
                    ii++;
                }
                if (ii >= inputLineNumbers) {
                    fprintf(opt, "    %s %s %s %s", inputList[i][0], inputList[i][1], inputList[i][2], inputList[i][3]);
                }
            }
        }
        else if (strcmp(inputList[i][1], "if") == 0 && inputListLengthEachLine[i] < 6) {
            if (strcmp(inputList[i+1][0], "goto") == 0) {
                fprintf(opt, "%s ifFalse %s goto %s", inputList[i][0], inputList[i][2], inputList[i+1][1]);
                strcpy(bag[bagSize], inputList[i][3]);
                bagSize++;
                i++;
            }
            else if (strcmp(inputList[i][2], "true") == 0) {
                char labelTemp[99];
                strcpy(labelTemp, inputList[i][4]);
                strcat(labelTemp, ":");
                int ii = i;
                while (ii < inputLineNumbers) {
                    if (strcmp(labelTemp, inputList[ii][0]) == 0) {
                        fprintf(opt, "%s\n      goto %s", inputList[i][0], inputList[i][4]);
                        i = ii - 1;
                        break;
                    }
                    ii++;
                }
                if (ii >= inputLineNumbers) {
                    fprintf(opt, "%s %s %s %s %s", inputList[i][0], inputList[i][1], inputList[i][2], inputList[i][3], inputList[i][4]);
                }
            }
        }
        else if (strcmp(inputList[i][0], "ifFalse") == 0 && (strcmp(inputList[i][1], "false") == 0)) {
                char labelTemp[99];
                strcpy(labelTemp, inputList[i][3]);
                strcat(labelTemp, ":");
                int ii = i;
                while (ii < inputLineNumbers) {
                    if (strcmp(labelTemp, inputList[ii][0]) == 0) {
                        fprintf(opt, "    goto %s", inputList[i][3]);
                        i = ii - 1;
                        break;
                    }
                    ii++;
                }
                if (ii >= inputLineNumbers) {
                    fprintf(opt, "    %s %s %s %s", inputList[i][0], inputList[i][1], inputList[i][2], inputList[i][3]);
                }
        }
        else if (strcmp(inputList[i][1], "ifFalse") == 0 && (strcmp(inputList[i][2], "false") == 0)) {
            char labelTemp[99];
            strcpy(labelTemp, inputList[i][4]);
            strcat(labelTemp, ":");
            int ii = i;
            while (ii < inputLineNumbers) {
                if (strcmp(labelTemp, inputList[ii][0]) == 0) {
                    fprintf(opt, "%s\n      goto %s", inputList[i][0], inputList[i][4]);
                    i = ii - 1;
                    break;
                }
                ii++;
            }
            if (ii >= inputLineNumbers) {
                fprintf(opt, "%s %s %s %s %s", inputList[i][0], inputList[i][1], inputList[i][2], inputList[i][3], inputList[i][4]);
            }
        }
        else {
            char* labelStart = strchr(inputList[i][0], ':');
            int j = 0;
            if (labelStart == NULL) {
                fprintf(opt, "    ");
            } else {
                char *labelName;
                labelName = strtok(inputList[i][0], ":");
                int temp3 = 0;
                bool labelNotPrint = false;
                while (temp3 < bagSize) {
                    if (strcmp(bag[temp3], labelName) == 0) labelNotPrint = true;
                    temp3++;
                }
                if (labelNotPrint) {
                    j = 1;
                    fprintf(opt, "    ");
                }
                strcat(labelName, ":");
            }
            while (j < inputListLengthEachLine[i]) {
                char* label_or_tab2 = strchr(inputList[i][j], '\n');
                fprintf(opt, "%s ", inputList[i][j]);
                j++;
            }
        }
        fprintf(opt, "\n");
        i++;
    }
}

void arithmetic_reduction_folding() {
    int i = 0;
    int k = 0;
    while (i < inputLineNumbers) {
        
        int j = 0;
        char* labelStart = strchr(inputList[i][0], ':');
        if (labelStart == NULL) fprintf(opt, "     ");
        else {
            if (inputListLengthEachLine[i] == 1) {
                fprintf(opt, "%s", inputList[i][0]); j = 2;
                char* newLine = strchr(inputList[i][0], '\n');
                if (newLine != NULL) goto skipLine;

            }
            else { fprintf(opt, "%s  ", inputList[i][0]); j++; }
        }
        
        
        while (j < inputListLengthEachLine[i]) {
            if (strcmp(inputList[i][j], "=") == 0 && (j == inputListLengthEachLine[i]-2)) {
                fprintf(opt, "%s %s", inputList[i][j], inputList[i][j+1]);
                j = inputListLengthEachLine[i];
            }
            else if (strcmp(inputList[i][j], "=") == 0 && (j < inputListLengthEachLine[i]-2)) {
                
                fprintf(opt, "%s ", inputList[i][j]);
                
                if (strcmp(inputList[i][j+2], "+") == 0) {
                    if (isdigit(inputList[i][j+1][0]) != 0 && isdigit(inputList[i][j+3][0]) != 0) {
                        char* intOrReal1;
                        char* intOrReal2;
                        intOrReal1 = strchr(inputList[i][j+1], '.');
                        intOrReal2 = strchr(inputList[i][j+3], '.');

                        if (intOrReal1 == NULL & intOrReal2 == NULL) {    // Integer
                            int x, y;
                            x = atoi(inputList[i][j+1]);
                            y = atoi(inputList[i][j+3]);
                            fprintf(opt, "%d", x + y);
                            j = inputListLengthEachLine[i];
                        }
                        else {    // Integer
                            double x, y;
                            x = atof(inputList[i][j+1]);
                            y = atof(inputList[i][j+3]);
                            fprintf(opt, "%f", x + y);
                            j = inputListLengthEachLine[i];
                        }
                    }
                    else {
                        if (strcmp(inputList[i][j+1], "0") == 0) { fprintf(opt, "%s", inputList[i][j+3]);}
                        else if (strcmp(inputList[i][j+3], "0") == 0) { fprintf(opt, "%s", inputList[i][j+1]);}
                        else { fprintf(opt, "%s + %s", inputList[i][j+1], inputList[i][j+3]); }
                        j = inputListLengthEachLine[i];
                    }
                }
                else if (strcmp(inputList[i][j+2], "-") == 0) {
                    if (isdigit(inputList[i][j+1][0]) != 0 && isdigit(inputList[i][j+3][0]) != 0) {
                        char* intOrReal1;
                        char* intOrReal2;
                        intOrReal1 = strchr(inputList[i][j+1], '.');
                        intOrReal2 = strchr(inputList[i][j+3], '.');
                        
                        if (intOrReal1 == NULL & intOrReal2 == NULL) {    // Integer
                            int x, y;
                            x = atoi(inputList[i][j+1]);
                            y = atoi(inputList[i][j+3]);
                            fprintf(opt, "%d", x - y);
                            j = inputListLengthEachLine[i];
                        }
                        else {    // Integer
                            double x, y;
                            x = atof(inputList[i][j+1]);
                            y = atof(inputList[i][j+3]);
                            fprintf(opt, "%f", x - y);
                            j = inputListLengthEachLine[i];
                        }
                    }
                    else {
                        if (strcmp(inputList[i][j+1], "0") == 0) { fprintf(opt, "-%s", inputList[i][j+3]);}
                        else if (strcmp(inputList[i][j+3], "0") == 0) { fprintf(opt, "%s", inputList[i][j+1]);}
                        else { fprintf(opt, "%s - %s", inputList[i][j+1], inputList[i][j+3]); }
                        j = inputListLengthEachLine[i];
                    }
                }
                else if (strcmp(inputList[i][j+2], "*") == 0) {
                    if (isdigit(inputList[i][j+1][0]) != 0 && isdigit(inputList[i][j+3][0]) != 0) {
                        char* intOrReal1;
                        char* intOrReal2;
                        intOrReal1 = strchr(inputList[i][j+1], '.');
                        intOrReal2 = strchr(inputList[i][j+3], '.');
                        
                        if (intOrReal1 == NULL & intOrReal2 == NULL) {    // Integer
                            int x, y;
                            x = atoi(inputList[i][j+1]);
                            y = atoi(inputList[i][j+3]);
                            fprintf(opt, "%d", x * y);
                            j = inputListLengthEachLine[i];
                        }
                        else {    // Integer
                            double x, y;
                            x = atof(inputList[i][j+1]);
                            y = atof(inputList[i][j+3]);
                            fprintf(opt, "%f", x * y);
                            j = inputListLengthEachLine[i];
                        }
                    }
                    else {
                        if (strcmp(inputList[i][j+1], "0") == 0 || strcmp(inputList[i][j+3], "0") == 0) { fprintf(opt, "0");}
                        else if (strcmp(inputList[i][j+1], "1") == 0) { fprintf(opt, "%s", inputList[i][j+3]); }
                        else if (strcmp(inputList[i][j+3], "1") == 0) { fprintf(opt, "%s", inputList[i][j+1]); }
                        else { fprintf(opt, "%s * %s", inputList[i][j+1], inputList[i][j+3]); }
                        j = inputListLengthEachLine[i];
                    }
                }
                else if (strcmp(inputList[i][j+2], "/") == 0) {
                    if (isdigit(inputList[i][j+1][0]) != 0 && isdigit(inputList[i][j+3][0]) != 0) {
                        char* intOrReal1;
                        char* intOrReal2;
                        intOrReal1 = strchr(inputList[i][j+1], '.');
                        intOrReal2 = strchr(inputList[i][j+3], '.');
                        
                        if (intOrReal1 == NULL & intOrReal2 == NULL) {    // Integer
                            int x, y;
                            x = atoi(inputList[i][j+1]);
                            y = atoi(inputList[i][j+3]);
                            fprintf(opt, "%d", x / y);
                            j = inputListLengthEachLine[i];
                        }
                        else {    // Integer
                            double x, y;
                            x = atof(inputList[i][j+1]);
                            y = atof(inputList[i][j+3]);
                            fprintf(opt, "%f", x / y);
                            j = inputListLengthEachLine[i];
                        }
                    }
                    else {
                        if (strcmp(inputList[i][j+1], "0") == 0) { fprintf(opt, "0");}
                        else if (strcmp(inputList[i][j+3], "1") == 0) { fprintf(opt, "%s", inputList[i][j+1]);}
                        else if (strcmp(inputList[i][j+3], "2") == 0) { fprintf(opt, "%s * 0.5", inputList[i][j+1]);}
                        else if (strcmp(inputList[i][j+3], "4") == 0) { fprintf(opt, "%s * 0.25", inputList[i][j+1]);}
                        else if (strcmp(inputList[i][j+3], "5") == 0) { fprintf(opt, "%s * 0.2", inputList[i][j+1]);}
                        else if (strcmp(inputList[i][j+3], "10") == 0) { fprintf(opt, "%s * 0.1", inputList[i][j+1]);}
                        else if (strcmp(inputList[i][j+3], "100") == 0) { fprintf(opt, "%s * 0.01", inputList[i][j+1]);}
                        else { fprintf(opt, "%s / %s", inputList[i][j+1], inputList[i][j+3]); }
                        j = inputListLengthEachLine[i];
                    }
                }
                else if (strcmp(inputList[i][j+2], "%") == 0) {
                    if (isdigit(inputList[i][j+1][0]) != 0 && isdigit(inputList[i][j+3][0]) != 0) {
                        int x, y;
                        x = atoi(inputList[i][j+1]);
                        y = atoi(inputList[i][j+3]);
                        fprintf(opt, "%d", x % y);
                        j = inputListLengthEachLine[i];
                    }
                    else {
                        if (strcmp(inputList[i][j+3], "1") == 0) { fprintf(opt, "0");}
                        else if (strcmp(inputList[i][j+1], inputList[i][j+3]) == 0) { fprintf(opt, "0");}
                        else { fprintf(opt, "%s %s %s", inputList[i][j+1], inputList[i][j+2], inputList[i][j+3]); }
                        j = inputListLengthEachLine[i];
                    }
                }
                else if (strcmp(inputList[i][j+2], "&") == 0) {
                    if (strcmp(inputList[i][j+1], "false") == 0|| strcmp(inputList[i][j+3], "false") == 0) {
                       fprintf(opt, "false");
                        j = inputListLengthEachLine[i];
                    }
                    else {
                        fprintf(opt, "%s %s %s", inputList[i][j+1], inputList[i][j+2], inputList[i][j+3]);
                        j = inputListLengthEachLine[i];
                    }
                }
                else if (strcmp(inputList[i][j+2], "|") == 0) {
                    if (strcmp(inputList[i][j+1], "true") == 0|| strcmp(inputList[i][j+3], "true") == 0) {
                        fprintf(opt, "true");
                        j = inputListLengthEachLine[i];
                    }
                    else {
                        fprintf(opt, "%s %s %s", inputList[i][j+1], inputList[i][j+2], inputList[i][j+3]);
                        j = inputListLengthEachLine[i];
                    }
                }
                else if (strcmp(inputList[i][j+1], "!") == 0) {
                    if (strcmp(inputList[i][j+2], "true") == 0) {
                        fprintf(opt, "false");
                        j = inputListLengthEachLine[i];
                    }
                    else if (strcmp(inputList[i][j+2], "false") == 0) {
                        fprintf(opt, "true");
                        j = inputListLengthEachLine[i];
                    }
                    else {
                        fprintf(opt, "%s %s", inputList[i][j+1], inputList[i][j+2]);
                        j = inputListLengthEachLine[i];
                    }
                }
                else if (strcmp(inputList[i][j+1], "-") == 0) {
                    if (isdigit(inputList[i][j+2][0]) != 0) {
                        char* intOrReal1;
                        intOrReal1 = strchr(inputList[i][j+1], '.');
                        
                        if (intOrReal1 == NULL) {    // Integer
                            int x;
                            x = atoi(inputList[i][j+1]);
                            fprintf(opt, "%d", x * (-1));
                            j = inputListLengthEachLine[i];
                        }
                        else {    // Integer
                            double x;
                            x = atof(inputList[i][j+1]);
                            fprintf(opt, "%f", x * (-1));
                            j = inputListLengthEachLine[i];
                        }
                    }
                    else {
                        fprintf(opt, "%s %s", inputList[i][j+1], inputList[i][j+2]);
                        j = inputListLengthEachLine[i];
                    }
                }
                else if (strcmp(inputList[i][j+1], "i2r") == 0) {
                    if (isdigit(inputList[i][j+2][0]) != 0) {
                        double x;
                        x = atof(inputList[i][j+1]);
                        fprintf(opt, "%f", x);
                        j = inputListLengthEachLine[i];
                    }
                    else {
                        fprintf(opt, "%s %s", inputList[i][j+1], inputList[i][j+2]);
                        j = inputListLengthEachLine[i];
                    }
                }
                else if (strcmp(inputList[i][j+1], "r2i") == 0) {
                    if (isdigit(inputList[i][j+2][0]) != 0) {
                        int x;
                        x = atoi(inputList[i][j+1]);
                        fprintf(opt, "%d", x);
                        j = inputListLengthEachLine[i];
                    }
                    else {
                        fprintf(opt, "%s %s", inputList[i][j+1], inputList[i][j+2]);
                        j = inputListLengthEachLine[i];
                    }
                }
                else if (strcmp(inputList[i][j+2], "==") == 0) {
                    if (isdigit(inputList[i][j+1][0]) != 0 && isdigit(inputList[i][j+3][0]) != 0) {
                        char* intOrReal1;
                        char* intOrReal2;
                        intOrReal1 = strchr(inputList[i][j+1], '.');
                        intOrReal2 = strchr(inputList[i][j+3], '.');
                        
                        if (intOrReal1 == NULL & intOrReal2 == NULL) {    // Integer
                            int x, y;
                            x = atoi(inputList[i][j+1]);
                            y = atoi(inputList[i][j+3]);
                            if (x == y) fprintf(opt, "true");
                            else fprintf(opt, "false");
                            j = inputListLengthEachLine[i];
                        }
                        else {    // Integer
                            double x, y;
                            x = atof(inputList[i][j+1]);
                            y = atof(inputList[i][j+3]);
                            if (x == y) fprintf(opt, "true");
                            else fprintf(opt, "false");
                            j = inputListLengthEachLine[i];
                        }
                    }
                    else if (((strcmp(inputList[i][j+1], "true") == 0) || (strcmp(inputList[i][j+1], "false") == 0))
                          && ((strcmp(inputList[i][j+3], "true") == 0) || (strcmp(inputList[i][j+3], "false") == 0))) {
                        if (strcmp(inputList[i][j+1], inputList[i][j+3]) == 0) fprintf(opt, "true");
                        else fprintf(opt, "false");
                    }
                    else {
                        if (strcmp(inputList[i][j+1], inputList[i][j+3]) == 0) fprintf(opt, "true");
                        else fprintf(opt, "%s %s %s", inputList[i][j+1], inputList[i][j+2], inputList[i][j+3]);
                        j = inputListLengthEachLine[i];
                    }
                }
                else if (strcmp(inputList[i][j+2], "<") == 0) {
                    if (isdigit(inputList[i][j+1][0]) != 0 && isdigit(inputList[i][j+3][0]) != 0) {
                        char* intOrReal1;
                        char* intOrReal2;
                        intOrReal1 = strchr(inputList[i][j+1], '.');
                        intOrReal2 = strchr(inputList[i][j+3], '.');
                        
                        if (intOrReal1 == NULL & intOrReal2 == NULL) {    // Integer
                            int x, y;
                            x = atoi(inputList[i][j+1]);
                            y = atoi(inputList[i][j+3]);
                            if (x < y) fprintf(opt, "true");
                            else fprintf(opt, "false");
                            j = inputListLengthEachLine[i];
                        }
                        else {    // Integer
                            double x, y;
                            x = atof(inputList[i][j+1]);
                            y = atof(inputList[i][j+3]);
                            if (x < y) fprintf(opt, "true");
                            else fprintf(opt, "false");
                            j = inputListLengthEachLine[i];
                        }
                    }
                    else if (((strcmp(inputList[i][j+1], "true") == 0) || (strcmp(inputList[i][j+1], "false") == 0))
                             && ((strcmp(inputList[i][j+3], "true") == 0) || (strcmp(inputList[i][j+3], "false") == 0))) {
                        if (strcmp(inputList[i][j+1], inputList[i][j+3]) == 0) fprintf(opt, "false");
                        else if (strcmp(inputList[i][j+1], "true") == 0) fprintf(opt, "false");
                        else fprintf(opt, "true");
                    }
                    else {
                        fprintf(opt, "%s %s %s", inputList[i][j+1], inputList[i][j+2], inputList[i][j+3]);
                        j = inputListLengthEachLine[i];
                    }
                }
            }
            else {
                fprintf(opt, "%s ", inputList[i][j]);
            }
            j++;
        }
        fprintf(opt, "\n");
        skipLine:{}
        i++;
    }
}

int main(int argc, char *argv[]) {
    FILE *file;
    char buffer[99999];
    file =fopen(argv[2],"r");
    if (!file) return 1;

    while (fgets(buffer, 1000, file)!=NULL) {
        strcpy(textInput[inputLineNumbers], buffer);
        inputLineNumbers++;
    }
    fclose(file);
    
    split_input_each_line();
    opt = fopen("prog.opt", "w");

    if (strcmp(argv[1], "32") == 0 ) {      //  Flow-Of-Control Optimization
        flow_of_control();
        fclose(opt);
        int numberofOpt = 0;

        while (numberofOpt < 3) { // Optimize 3 times
            file =fopen("prog.opt","r");
            memset(textInput, 0, sizeof textInput);
            memset(inputList,0, sizeof inputList);
            memset(inputListLengthEachLine,0, sizeof inputListLengthEachLine);

            inputLineNumbers = 0;

            if (!file) return 1;
            
            while (fgets(buffer, 1000, file)!=NULL) {
                strcpy(textInput[inputLineNumbers], buffer);
                inputLineNumbers++;
            }
            fclose(opt);
            fclose(file);
            split_input_each_line();
            opt = fopen("prog.opt", "w");
            flow_of_control();
            fclose(opt);
            fclose(file);

            numberofOpt++;
        }
        
    }
    else if (strcmp(argv[1], "16") == 0 ) { // Eliminate Unreachable Code Optimization
        eliminate_unreachable_code();
        fclose(opt);
        int numberofOpt = 0;
        
        while (numberofOpt < 3) { // Optimize 3 times
            file =fopen("prog.opt","r");
            memset(textInput, 0, sizeof textInput);
            memset(inputList,0, sizeof inputList);
            memset(inputListLengthEachLine,0, sizeof inputListLengthEachLine);
            
            inputLineNumbers = 0;
            
            if (!file) return 1;
            
            while (fgets(buffer, 1000, file)!=NULL) {
                strcpy(textInput[inputLineNumbers], buffer);
                inputLineNumbers++;
            }
            fclose(file);

            split_input_each_line();
            opt = fopen("prog.opt", "w");
            
            eliminate_unreachable_code();
            fclose(opt);
            numberofOpt++;
        }
        
    }
    else if (strcmp(argv[1], "4") == 0 ) { // Arithmetic Identities, Reduction in Strength, and Constant Folding
        arithmetic_reduction_folding();
        fclose(opt);
        return 0;
    }
    fclose(opt);
    
    return 0;
}
