%{
    #include <stdio.h>
    #include <stdbool.h>
    #include <string.h>
    
    #include "symbol_table.h"
    #include "error.h"
	#include "scope_pass.h"

    extern int lineno();
    extern int columnno();
    extern char *checkTab();
    int yylex();
    char *yytext;
    
    void yyerror(char* p);
    
    char *tempError[200];
    char *name;
    char *new_name = "";
    char *type_name;
    char *type;
    char *variable;
    int scope = 0;
    
    /*GLOBAL VARIABLES USED TO FIND SEMANTIC ERRORS*/
    bool for_statement = false;
    bool inside_function = false;
    bool bool_expression = false;
    bool chained_b_expression = false;
    bool int_const = false;
    bool int_expression = false;
    bool chained_int_expression[99] = {false};
    int chained_int_expression_level = 0;
    bool real_expression = false;
    bool chained_real_expression[99] = {false};
    bool math_expression = false;
    bool chained_math_expression = false;
    bool char_expression = false;
    bool string_expression = false;
    
    bool left_coercion = false;
    bool right_coercion = false;
    bool check_var = false;
    bool heap_type = false;
    
    
    bool int_var = false;
    bool real_var = false;
    bool bool_var = false;
    bool str_var = false;
    bool ch_var = false;
    bool a_var = false;
    bool r_var = false;
    bool f_var = false;
    bool utype = false;
    bool function_call = false;
    bool rtype = false;
	bool first = true;	    

    bool coercion = false;
    char *function_name = "";
    char *function_type = "";
    char *function_call_name = "";
    char *var_type = "";
    char *var2_type = "";
    char *rtype_name = "";
    
    /*GLOBAL VARIABLES TO BE USED IF SEMANTICS ERROR OCCURS*/
    
    bool bool_expression_error = false;
    bool switch_int_error = false;
    bool int_expression_error = false;
    bool real_expression_error = false;
    bool non_int_rem_error = false;
    bool math_expression_error = false;
    bool rtype_error = false;
    bool heap_error = false;
    bool first_time_in = true; 
    char *val1;
    char *val2;
    char *val3;
    char *val4;
    int val5;
    int val6;
    char *function_name;
    
    FILE *asc = NULL;
    
    struct Node {
        int i;
        char* s;
        bool b;
        char ch;
        float f;
    };
    
    void falsify() {
        int_expression = false;
        real_expression = false;
        bool_expression = false;
        char_expression = false;
        string_expression = false;
    }
    
    %}
/* Bison declarations */

%token ID 101

/*type names*/
%token T_INTEGER 201
%token T_REAL 202
%token T_BOOLEAN 203
%token T_CHARACTER 204
%token T_STRING 205

/*constants*/
%token C_INTEGER 301
%token C_REAL 302
%token C_CHARACTER 303
%token C_STRING 304
%token C_TRUE 305
%token C_FALSE 306

/*keywords*/
%token NULL_PTR 401
%token RESERVE 402
%token RELEASE 403
%token FOR 404
%token WHILE 405
%token IF 406
%token THEN 407
%token ELSE 408
%token SWITCH 409
%token CASE 410
%token OTHERWISE 411
%token TYPE 412
%token FUNCTION 413
%token CLOSURE 414

/*punctuation - grouping*/
%token L_PARANTHESIS 501
%token R_PARANTHESIS 502
%token L_BRACKET 503
%token R_BRACKET 504
%token L_BRACE 505
%token R_BRACE 506
%token S_QUOTE 507
%token D_QUOTE 508

/*other punctuation*/
%token SEMI_COLON 551
%token COLON 552
%token COMMA 553
%token ARROW 554
%token BACKSLASH 555

/*operators*/
%token ADD 601
%token SUB_OR_NEG 602
%token MUL 603
%token DIV 604
%token REM 605
%token DOT 606
%token LESS_THAN 607
%token EQUAL_TO 608
%token ASSIGN 609
%token INT2REAL 610
%token REAL2INT 611
%token IS_NULL 612
%token NOT 613
%token AND 614
%token OR 615

%token COMMENT 700

%union{
    int i;
    bool b;
    char* s;
    char ch;
    struct Node n;
    float f;
}

/* Define operator precedence */
%right ASSIGN DOT/* lowest precedence */
%left OR
%left AND
%left LESS_THAN EQUAL_TO NOT IS_NULL
%left ADD SUB_OR_NEG
%left MUL DIV REM /* Highest precedence */
%left INT2REAL REAL2INT
%left L_PARANTHESIS R_PARANTHESIS
/* Define types of tokens */

%type <s> ID

%type <i> C_INTEGER
%type <f> C_REAL
%type <b> C_TRUE
%type <b> C_FALSE
%type <ch> C_CHARACTER
%type <s> C_STRING
%type <s> T_INTEGER
%type <s> T_REAL
%type <s> T_BOOLEAN
%type <s> T_CHARACTER
%type <s> T_STRING
%type <s> definition
%type <s> definition_list
%type <s> identifier
%type <n> constant
%type <s> parameter_declaration
%type <s> non_empty_parameter_list
%type <s> parameter_list
%type <s> definition_option1
%type <s> pblock
%type <s> declaration
%type <s> declaration_list
%type <s> identifier_list
%type <s> assignable
%type <n> expression
%type <s> typeID

/*%type <s> COMMENT
 
 %typROR:%i:%i: the name '%s', used here as a type, has not been declared at this point in the program.", line, col, name);
 
 */
/* Where the grammar starts */
%start program

%%

program: { put("    goto Main\n"); gen(); new_scope(scope);} definition_list { put("Main"); gen(); memset(top, 0, sizeof top); topSize = 0;} sblock { put("exit"); gen(); }
;

definition_list:
|  definition definition_list
;

definition:  TYPE {scope++; new_scope(scope); if (first && asc != NULL) {fprintf(asc, "%i: ", scope);}; first = false ;if (asc != NULL) fprintf(asc, "type");} identifier COLON {new_name = $3; insert_symbol($3, 0, "", ""); if (asc != NULL) fprintf(asc, ":");} definition_option1
|   FUNCTION { put("function"); scope++; new_scope(scope); if (asc != NULL) fprintf(asc, "function");} identifier { put(yytext); } COLON {
    
    if (asc != NULL) fprintf(asc, ":");
    inside_function = true;
    function_name = $3;
    
} identifier {
    
    if (is_ftype($7) && get_return_type($7) != NULL) {
        insert_symbol($3, 0, $7, "function");
        change_return_type(function_name, get_return_type($7));
    }
    else throw_no_ftype_error(lineno(), columnno(), $7, asc);
    
    put(yytext); gen();
	scope++;
	new_scope(scope);

} sblock {
    put("finishDefineFunction"); gen();
    function_name = "";
    inside_function = false;
    int_expression_error = false;
    bool_expression_error = false;
	scope--;
	new_scope(scope);
}
;

definition_option1: {change_extra(new_name, "rtype");} dblock
|  C_INTEGER {if (asc != NULL) fprintf(asc, "%i", $1);} ARROW {if (asc != NULL) fprintf(asc, "->");} identifier { val1 = ""; } definition_option2 {
    change_extra(new_name, "atype");
    change_type(new_name, $5);
    add_array_size(new_name, $1);
    if (val1 == NULL) {
        assign_width(new_name, "",0);
    } else {
        int i = 1;
        while (val1[i]!='"') {
            i++;
        }
        i+=1;
        assign_width(new_name, "",i);
    }
    val1 = "";
    new_name = "";
}
|  pblock ARROW {if (asc != NULL) fprintf(asc, "->");} identifier {
    change_return_type(new_name, $4);
    change_extra(new_name, "ftype");
    new_name = "";
}
;
/* <------------------- 05/06/18 Have the array store the initial value */
definition_option2:
|   COLON
    L_PARANTHESIS {if (asc != NULL) fprintf(asc, ": (");}
    constant {  if (yytext[0] == '"'){ val1 = yytext; }
        char temp23[99];
        strcpy(temp23, yytext);
        assign_val(new_name, temp23);
    }
    R_PARANTHESIS {if (asc != NULL) fprintf(asc, ")");}
;

sblock: L_BRACE {if (asc != NULL) {fprintf(asc, "{");}} statement_list R_BRACE {if (asc != NULL) {fprintf(asc, "}");}}
| L_BRACE {if (asc != NULL) {fprintf(asc, "{");}} dblock statement_list R_BRACE {if (asc != NULL) {fprintf(asc, "}");}}
;

dblock: L_BRACKET{if (asc != NULL) {fprintf(asc, "["); strcpy(tempError, ""); }} declaration_list R_BRACKET {new_name = "";if (asc != NULL) {fprintf(asc, "]");}} 
;

declaration_list: declaration SEMI_COLON {if (asc != NULL) {fprintf(asc, ";");}} declaration_list
| declaration 
;

declaration: typeID {variable = $1;if (asc != NULL) {fprintf(asc, "%s", $1);}} COLON {if (asc != NULL) {fprintf(asc, ":");}} identifier_list
|  ID {variable = $1;if (asc != NULL) fprintf(asc, "%s", $1);} COLON {if (asc != NULL) fprintf(asc, ":"); } identifier_list { if (!type_exists($1)) { const char *s = throw_dec_error_1(lineno(), columnno(), $1, asc);}}
;

identifier_list: identifier { val3 = $1; val2 = "";  if (strcmp(new_name, "") != 0) add_to_type(new_name, variable, $1); insert_symbol($1, scope, variable, "local");} identifier_list_option
;

identifier_list_option:
|   COMMA {if (asc != NULL) fprintf(asc, ",");} identifier_list
|   ASSIGN {
    put(val3); put("=");
    val2 = ""; if (asc != NULL) fprintf(asc, ":=");}
    constant {
        put(yytext); gen();
        if ( yytext[0] == '"') { val2 = yytext;} } {if (strcmp(val2, "")!=0) { assign_width(val3, "", strlen(val2)); val2 = ""; assign_address(val3);}
        else { assign_width(val3, "", 0); val2 = ""; assign_address(val3);}}
    comma_option
;

comma_option:
|   COMMA {if (asc != NULL) fprintf(asc, ",");} identifier_list
;

statement_list: statement statement_list
|   statement
;
/******************BEGIN FOR STATEMENT***********************/

statement: FOR { put("for");if (asc != NULL) {fprintf(asc, "for");} }  L_PARANTHESIS {
    
    if (asc != NULL) {fprintf(asc, "(");}
    for_statement = true;
    
} statement { put("endOf1Statement"); gen(); } SEMI_COLON {if (asc != NULL) {fprintf(asc, ";");bool_expression = false;}} expression {put("endOfFirstStatement"); gen(); put("endOf2Statement");} SEMI_COLON {
    
    if (asc != NULL) fprintf(asc, ";");
    if (!bool_expression) bool_expression_error = true;
    
} statement R_PARANTHESIS {
    put("endOfSecondStatement"); gen();
    
    if (asc != NULL) fprintf(asc, ")");
    if (bool_expression_error) throw_bool_error(lineno(), columnno(), asc);
    if (non_int_rem_error) throw_rem_error(lineno(), columnno(), asc);
    
    int_expression = false;
    int_expression_error = false;
    real_expression = false;
    non_int_rem_error = false;
    bool_expression_error = false;
    bool_expression = false;
    for_statement = false;
	scope++;
	new_scope(scope);	
    
} sblock { put("endOfForLoop"); gen(); scope--; new_scope(scope); }

/******************END FOR STATEMENT**************************/

/******************START WHILE STATEMENT**********************/

|   WHILE { put("initWhile"); gen();if (asc != NULL) fprintf(asc, "while"); } L_PARANTHESIS {if (asc != NULL) fprintf(asc, "(");bool_expression = false;} expression { put("while"); gen(); } R_PARANTHESIS {
    
    if (asc != NULL) fprintf(asc, ")");
    if (!bool_expression || bool_expression_error) throw_bool_error(lineno(), columnno(), asc);
    if (non_int_rem_error) throw_rem_error(lineno(), columnno(), asc);
    if (int_expression_error) throw_rem_error(lineno(), columnno(), asc);
    if (real_expression_error) throw_real_expression_error(lineno(), columnno(), asc);
    
    /*TODO for further type checking, simply use while statement to test before writing further implementations*/
    
    int_expression = false;
    int_expression_error = false;
    real_expression = false;
    real_expression_error = false;
    bool_expression = false;
    bool_expression_error = false;

	scope++;
	new_scope(scope);

} sblock { put("endWhile"); gen(); scope--; new_scope(scope); }

/******************END WHILE STATEMENT***********************/

/******************BEGIN IF STATEMENT************************/

|   IF { put("if");if (asc != NULL) fprintf(asc, "if"); } L_PARANTHESIS {
    
    if (asc != NULL) fprintf(asc, "(");
    bool_expression = false;
    int_expression = false;
    int_expression_error = false;
    real_expression = false;
    real_expression_error = false;
    bool_expression_error = false;
    
} expression R_PARANTHESIS {if (asc != NULL) fprintf(asc, ")");} THEN {
    put("then"); gen();
    if (asc != NULL) fprintf(asc, "then");
    if (!bool_expression || bool_expression_error) throw_bool_error(lineno(), columnno(), asc);
    if (int_expression_error && real_expression_error) throw_math_expression_error(lineno(), columnno(), asc);
    if (non_int_rem_error) throw_rem_error(lineno(), columnno(), asc);
    
    int_expression = false;
    int_expression_error = false;
    real_expression = false;
    real_expression_error = false;
    bool_expression = false;
    bool_expression_error = false;

	scope++;
	new_scope(scope);
    
} sblock {scope--; new_scope(scope);} ELSE {
    put("else"); gen();
    if (asc != NULL) fprintf(asc, "else");
    
	scope++;
	new_scope(scope);

} sblock { put("endOfIf"); gen(); scope--; new_scope(scope); }

/******************END IF STATEMENT**************************/

/******************BEGIN SWITCH STATEMENT********************/


|   SWITCH { put("switch"); gen(); if (asc != NULL) fprintf(asc, "switch");} L_PARANTHESIS {if (asc != NULL) fprintf(asc, "("); int_expression = false;} expression R_PARANTHESIS {
    put("switchExpression"); gen();
    if (asc != NULL) fprintf(asc, ")");
    if (int_expression_error) throw_int_expression_error(lineno(), columnno(), asc);
	if (!int_expression) throw_switch_int_error(lineno(), columnno(), asc); 
    
    int_expression = false;
    int_expression_error = false;
    real_expression = false;
    real_expression_error = false;
    int_expression_error = false;
    
} case_singular OTHERWISE { put("otherwise"); gen();if (asc != NULL) fprintf(asc, "otherwise");} COLON {

	if (asc != NULL) fprintf(asc, ":");
	scope++; 
	new_scope(scope);

} sblock { 

	put("finishSwitch"); gen();
	scope--;
	new_scope(scope);
}

/******************END SWITCH STATEMENT**********************/

|   {scope++; new_scope(scope);if (first && asc != NULL) fprintf("%i: ", scope);} sblock
|   {
    val4 = "";
    int_var = false;
    real_var = false;
    bool_var = false;
    str_var = false;
    ch_var = false;
    a_var = false;
    r_var = false;
    f_var = false;
    var_type = "";
    var2_type = "";
    check_var = true;
    utype = false;
    rtype = false;
    rtype_name = "";
    rtype_error = false;
    falsify();
	first_time_in = false;
    
} assignable ASSIGN {
    put("="); val1 = ""; val2 = "";
	first_time_in = true;
    if (asc != NULL) fprintf(asc, " := "); check_var = false; falsify(); left_coercion = false; right_coercion = false;  }
expression { gen(); int tmp = 0; if (strcmp(val1, "string")==0) { tmp = strlen(val2);} ; val1="";val2="";val4="";}
SEMI_COLON {
    if (asc != NULL) fprintf(asc, ";");
    if (inside_function && f_var) {
        if (strcmp(var_type, "integer") == 0) { if (!int_expression && strcmp(var_type, var2_type) != 0) throw_function_return_error(lineno(), columnno(), asc);}
        else if (strcmp(var_type, "real") == 0) { if (!real_expression && strcmp(var_type, var2_type) != 0) throw_function_return_error(lineno(), columnno(), asc);}
        else if (strcmp(var_type, "Boolean") == 0) { if (!bool_expression && strcmp(var_type, var2_type) != 0) throw_function_return_error(lineno(), columnno(), asc);}
        else if (strcmp(var_type, "character") == 0) { if (!char_expression && strcmp(var_type, var2_type) != 0) throw_function_return_error(lineno(), columnno(), asc);}
        else if (strcmp(var_type, "string") == 0) { if (!string_expression && strcmp(var_type, var2_type) != 0) throw_function_return_error(lineno(), columnno(), asc);}
        else if (strcmp(var_type, var2_type) != 0) throw_function_return_error(lineno(), columnno(), asc);
	}
    else if (strcmp(var_type, "integer") == 0) { if (!int_expression && strcmp(var_type, var2_type) != 0) throw_int_assignment_error(lineno(), columnno(), asc);}
    else if (strcmp(var_type, "real") == 0) { if (!real_expression && strcmp(var_type, var2_type) != 0) throw_real_assignment_error(lineno(), columnno(), asc);}
    else if (strcmp(var_type, "Boolean") == 0) { if (!bool_expression && strcmp(var_type, var2_type) != 0) throw_bool_assignment_error(lineno(), columnno(), asc);}
    else if (strcmp(var_type, "character") == 0) { if (!char_expression && strcmp(var_type, var2_type) != 0) throw_char_assignment_error(lineno(), columnno(), asc);}
    else if (strcmp(var_type, "string") == 0) { if (!string_expression && strcmp(var_type, var2_type) != 0) throw_string_assignment_error(lineno(), columnno(), asc);}
    else if (strcmp(var_type, var2_type) != 0) throw_assignment_error(lineno(), columnno(), asc);
    if (rtype_error) throw_rtype_error(lineno(), columnno(), asc);
    
    int_var = false;
    real_var = false;
    bool_var = false;
    str_var = false;
    ch_var = false;
    a_var = false;
    r_var = false;
    f_var = false;
    var_type = "";
    var2_type = "";
    rtype_name = "";
    function_call = false;
    rtype = false;
    check_var = false;
    rtype_error = false;
    falsify();
}
|   memop {heap_type = false;} assignable { gen(); } SEMI_COLON {
    

    if (asc != NULL) fprintf(asc, ";");
    if (!heap_type) throw_non_heap_type_error(lineno(), columnno(), asc);
    
}
;

case_singular: CASE { put("case"); if (asc != NULL) fprintf(asc, "case"); int_const = false;} constant { put(yytext); gen(); } COLON {
    
    if (asc != NULL) fprintf(asc, ": ");
    if (!int_const) throw_switch_case_error(lineno(), columnno(), asc);
    int_const = false;
    
} sblock {put("finishThisCase"); gen();} case_plural
;

case_plural:
|   case_singular
;

assignable: /*{check_var = true;}*/ identifier {
    put(yytext);
    if (is_function($1)) {function_call = true; function_call_name = $1;}
    if (is_rtype($1)) {rtype = true; rtype_name = $1;}
    //if (!exists($2)) throw_dec_error_2(lineno(), columnno(), $2, asc);
    if (is_atype($1) || is_rtype($1)) heap_type = true;
	f_var = false;
    if (inside_function && strcmp(function_name, $1) == 0) f_var = true;
	else if (is_atype($1)) a_var = true;

    if (check_var){
        if (get_type($1) != NULL && strcmp(get_type($1), "integer") == 0) var_type = "integer";
        else if (get_type($1) != NULL && strcmp(get_type($1), "real") == 0) var_type = "real";
        else if (get_type($1) != NULL && strcmp(get_type($1), "Boolean") == 0) var_type = "Boolean";
        else if (get_type($1) != NULL && strcmp(get_type($1), "character") == 0) var_type = "character";
        else if (get_type($1) != NULL && strcmp(get_type($1), "string") == 0) var_type = "string";
        else if (is_function($1) || f_var) {var_type = get_return_type($1);utype = true;}
    	else if (is_atype($1)) {var_type = get_type($1); utype = true;}
		else var_type = get_type($1);
        
    }
    else {
        if (get_type($1) != NULL && strcmp(get_type($1), "integer") == 0) int_expression = true;
        else if (get_type($1) != NULL && strcmp(get_type($1), "real") == 0) real_expression = true;
        else if (get_type($1) != NULL && strcmp(get_type($1), "Boolean") == 0) bool_expression = true;
        else if (get_type($1) != NULL && strcmp(get_type($1), "character") == 0) char_expression = true;
        else if (get_type($1) != NULL && strcmp(get_type($1), "string") == 0) string_expression = true;
        else if (is_function($1) && first_time_in) var2_type = get_return_type($1);
		else if (is_atype($1)) {printf("%s is an array with type: ", $1, get_return_type($1)); var2_type = get_type($1);}	
		else if (first_time_in) var2_type = get_type($1);
    }
}
|   assignable  {

	val3 = $1;
	if (first_time_in && is_function($1)) {var2_type = get_return_type($1); first_time_in = false;}
} ablock { gen(); }
|   assignable DOT {if (asc != NULL) fprintf(asc, "."); if (!rtype) rtype_error = true; } identifier {
    put("period"); put(yytext); gen(); 
    val3 =$1; val4 = $4;
    if (exists($4)) {
        
        
        if (check_var) var_type = get_type($4);
        else if (first_time_in) var2_type = get_type($4);
    }

    function_call = false;
    function_call_name = false;
    rtype = false;
    rtype_name = "";
    
}
;

expression: SUB_OR_NEG { put(yytext); if (asc != NULL) fprintf(asc, "-"); int_expression = false; real_expression = false; math_expression = false;} expression {
    gen();
    if (!real_expression && !int_expression) {
        real_expression_error = true;
        int_expression_error = true;
    }
}

|   NOT { put("!"); if (asc != NULL) fprintf(asc, "!"); bool_expression = false;}
    expression {
    gen();
    if(!bool_expression) bool_expression_error = true;}

|   INT2REAL { put("i2r"); if (asc != NULL) fprintf(asc, " i2r "); }
    expression {
   
    gen();
    if (!int_expression || int_expression_error) throw_itr_error(lineno(), columnno(), asc);
	int_expression = false; real_expression = true;
}
	
|   REAL2INT { put("r2i"); if (asc != NULL) fprintf(asc, " r2i "); }
    expression {
    gen();
    if (!real_expression || real_expression_error) throw_rti_error(lineno(), columnno(), asc);
	int_expression = true; real_expression = false;
}
|   expression IS_NULL { put("== null"); if (asc != NULL) fprintf(asc, " isNull");} {gen(); bool_expression = true;}

|   assignable

/******************BEGIN ADD EXPRESSION**********************/

|   expression

    ADD { put(yytext); if (asc != NULL) fprintf(asc, "+");
    if (int_expression) { chained_int_expression[chained_int_expression_level] = true; chained_real_expression[chained_int_expression_level] = false; }
    else if (real_expression) { chained_real_expression[chained_int_expression_level] = true; chained_int_expression[chained_int_expression_level] = false; }
    left_coercion = false;
    right_coercion = false;
    chained_int_expression_level++;
    }

    expression {
    chained_int_expression_level-=1;
    if (!int_expression || !chained_int_expression[chained_int_expression_level]) int_expression_error = true;
    else if (!real_expression || !chained_real_expression[chained_int_expression_level]) real_expression_error = true;
    if (int_expression && chained_real_expression[chained_int_expression_level]) right_coercion = true;
    else if (real_expression && chained_int_expression[chained_int_expression_level]) left_coercion = true;
    
    
    if (right_coercion == true) {
        char t1[9];
        strcpy(t1, top[topSize-1]);
        strcpy(top[topSize-1], "i2r");
        topSize++;
        strcpy(top[topSize-1], t1);
        gen();
        gen();
        falsify();
        real_expression = true;
    }
    else if (left_coercion == true) {
        topSize+=2;
        strcpy(top[topSize-2], "i2r");
        strcpy(top[topSize-1], top[topSize-5]);
        gen();
        strcpy(top[topSize-4], top[topSize-1]);
        strcpy(top[topSize-1], "");
        topSize--;
        gen();
        falsify();
        real_expression = true;
    } else {
        gen();
        if (chained_real_expression[chained_int_expression_level] || real_expression) {
            falsify();
            real_expression = true;
        } else {
            falsify();
            int_expression = true;
        }
    }
    right_coercion = false;
    left_coercion = false;
    chained_int_expression[chained_int_expression_level] = false;
    chained_real_expression[chained_int_expression_level] = false;
}

/******************END ADD EXPRESSION************************/

/******************BEGIN SUB EXPRESSION**********************/

|   expression

    SUB_OR_NEG { put(yytext); if (asc != NULL) fprintf(asc, " - ");
        if (int_expression) { chained_int_expression[chained_int_expression_level] = true; chained_real_expression[chained_int_expression_level] = false; }
        else if (real_expression) { chained_real_expression[chained_int_expression_level] = true; chained_int_expression[chained_int_expression_level] = false; }
        left_coercion = false;
        right_coercion = false;
        chained_int_expression_level++;
    }

    expression {
        chained_int_expression_level-=1;
        if (!int_expression || !chained_int_expression[chained_int_expression_level]) int_expression_error = true;
        else if (!real_expression || !chained_real_expression[chained_int_expression_level]) real_expression_error = true;
        if (int_expression && chained_real_expression[chained_int_expression_level]) right_coercion = true;
        else if (real_expression && chained_int_expression[chained_int_expression_level]) left_coercion = true;
        
        
        if (right_coercion == true) {
            char t1[9];
            strcpy(t1, top[topSize-1]);
            strcpy(top[topSize-1], "i2r");
            topSize++;
            strcpy(top[topSize-1], t1);
            gen();
            gen();
            falsify();
            real_expression = true;
        }
        else if (left_coercion == true) {
            topSize+=2;
            strcpy(top[topSize-2], "i2r");
            strcpy(top[topSize-1], top[topSize-5]);
            gen();
            strcpy(top[topSize-4], top[topSize-1]);
            strcpy(top[topSize-1], "");
            topSize--;
            gen();
            falsify();
            real_expression = true;
        } else {
            gen();
            if (chained_real_expression[chained_int_expression_level] || real_expression) {
                falsify();
                real_expression = true;
            } else {
                falsify();
                int_expression = true;
            }
        }
        right_coercion = false;
        left_coercion = false;
        chained_int_expression[chained_int_expression_level] = false;
        chained_real_expression[chained_int_expression_level] = false;
}

/******************END SUB EXPRESSION************************/

/******************BEGIN MUL EXPRESSION**********************/

|   expression

    MUL { put(yytext); if (asc != NULL) fprintf(asc, " * ");
        if (int_expression) { chained_int_expression[chained_int_expression_level] = true; chained_real_expression[chained_int_expression_level] = false; }
        else if (real_expression) { chained_real_expression[chained_int_expression_level] = true; chained_int_expression[chained_int_expression_level] = false; }
        left_coercion = false;
        right_coercion = false;
        chained_int_expression_level++;
    }

    expression {
        chained_int_expression_level-=1;
        if (!int_expression || !chained_int_expression[chained_int_expression_level]) int_expression_error = true;
        else if (!real_expression || !chained_real_expression[chained_int_expression_level]) real_expression_error = true;
        if (int_expression && chained_real_expression[chained_int_expression_level]) right_coercion = true;
        else if (real_expression && chained_int_expression[chained_int_expression_level]) left_coercion = true;
        
        if (right_coercion == true) {
            char t1[9];
            strcpy(t1, top[topSize-1]);
            strcpy(top[topSize-1], "i2r");
            topSize++;
            strcpy(top[topSize-1], t1);
            gen();
            gen();
            falsify();
            real_expression = true;
        }
        else if (left_coercion == true) {
            topSize+=2;
            strcpy(top[topSize-2], "i2r");
            strcpy(top[topSize-1], top[topSize-5]);
            gen();
            strcpy(top[topSize-4], top[topSize-1]);
            strcpy(top[topSize-1], "");
            topSize--;
            gen();
            falsify();
            real_expression = true;
        } else {
            gen();
            if (chained_real_expression[chained_int_expression_level] || real_expression) {
                falsify();
                real_expression = true;
            } else {
                falsify();
                int_expression = true;
            }
        }
        right_coercion = false;
        left_coercion = false;
        chained_int_expression[chained_int_expression_level] = false;
        chained_real_expression[chained_int_expression_level] = false;
}

/******************END MUL EXPRESSION************************/

/******************BEGIN DIV EXPRESSION**********************/

|   expression

    DIV { put(yytext); if (asc != NULL) fprintf(asc, " / ");
        if (int_expression) { chained_int_expression[chained_int_expression_level] = true; chained_real_expression[chained_int_expression_level] = false; }
        else if (real_expression) { chained_real_expression[chained_int_expression_level] = true; chained_int_expression[chained_int_expression_level] = false; }
        left_coercion = false;
        right_coercion = false;
        chained_int_expression_level++;
    }

    expression {
        chained_int_expression_level-=1;
        if (!int_expression || !chained_int_expression[chained_int_expression_level]) int_expression_error = true;
        else if (!real_expression || !chained_real_expression[chained_int_expression_level]) real_expression_error = true;
        if (int_expression && chained_real_expression[chained_int_expression_level]) right_coercion = true;
        else if (real_expression && chained_int_expression[chained_int_expression_level]) left_coercion = true;
        
        
        if (right_coercion == true) {
            char t1[9];
            strcpy(t1, top[topSize-1]);
            strcpy(top[topSize-1], "i2r");
            topSize++;
            strcpy(top[topSize-1], t1);
            gen();
            gen();
            falsify();
            real_expression = true;
        }
        else if (left_coercion == true) {
            topSize+=2;
            strcpy(top[topSize-2], "i2r");
            strcpy(top[topSize-1], top[topSize-5]);
            gen();
            strcpy(top[topSize-4], top[topSize-1]);
            strcpy(top[topSize-1], "");
            topSize--;
            gen();
            falsify();
            real_expression = true;
        } else {
            gen();
            if (chained_real_expression[chained_int_expression_level] || real_expression) {
                falsify();
                real_expression = true;
            } else {
                falsify();
                int_expression = true;
            }
        }
        right_coercion = false;
        left_coercion = false;
        chained_int_expression[chained_int_expression_level] = false;
        chained_real_expression[chained_int_expression_level] = false;
}

/******************END DIV EXPRESSION************************/

/******************BEGIN REM EXPRESSION**********************/

|   expression

    REM { put(yytext); if (asc != NULL) fprintf(asc, " %% ");
    if (int_expression) chained_int_expression[chained_int_expression_level] = true;
    if (real_expression) non_int_rem_error = true;}

    expression {
    gen();
    if (real_expression) non_int_rem_error = true;
    if (!int_expression || !chained_int_expression[chained_int_expression_level]) int_expression_error = true;
    chained_int_expression[chained_int_expression_level] = false;
}

|   expression

    AND { put(yytext); if (asc != NULL) fprintf(asc, " & ");
    if (bool_expression) chained_b_expression = true;}

    expression {
    gen();
    if (!bool_expression || !chained_b_expression) bool_expression_error = true;
}

|   expression

    OR {
    put(yytext); if (asc != NULL) fprintf(asc, " | ");
    if (bool_expression) chained_b_expression = true;}

    expression {
    gen();
    if (!bool_expression || !chained_b_expression) bool_expression_error = true;
}

|   expression
    LESS_THAN  { put(yytext); if (asc != NULL) fprintf(asc, " < ");}
    expression { gen(); bool_expression = true;}

|   expression
    EQUAL_TO    { put("=="); if (asc != NULL) fprintf(asc, " = ");}
    expression  { gen(); bool_expression = true;}

|   L_PARANTHESIS {if (asc != NULL) fprintf(asc, "(");} expression R_PARANTHESIS {if (asc != NULL) fprintf(asc, ")");}

|   C_INTEGER {
    put(yytext); val5 = $1;
    if (asc != NULL) fprintf(asc, "%i", $1);
    bool_expression = false;
    int_expression = true;
    real_expression = false;
    math_expression = true;
    char_expression = false;
    string_expression = false;
}

|   C_REAL {
    put(yytext);
    if (asc != NULL) fprintf(asc, " %f", $1);
    bool_expression = false;
    int_expression = false;
    real_expression = true;
    math_expression = true;
    char_expression = false;
    string_expression = false;
}

|   C_TRUE {
    put(yytext);
    if (asc != NULL) fprintf(asc, " true ");
    bool_expression = true;
    int_expression = false;
    real_expression = false;
    math_expression = false;
    char_expression = false;
    string_expression = false;
}

|   C_FALSE {
    put(yytext);
    if (asc != NULL) fprintf(asc, " false ");
    bool_expression = true;
    int_expression = false;
    real_expression = false;
    math_expression = false;
    char_expression = false;
    string_expression = false;
}

|   C_CHARACTER {
    put(yytext);
    if (asc != NULL) fprintf(asc, "%c ", $1);
    bool_expression = false;
    int_expression = false;
    real_expression = false;
    math_expression = false;
    char_expression = true;
    string_expression = false;
}

|   C_STRING {
    put(yytext); val1 = "string" ; val2 = $1;
    if (asc != NULL) fprintf(asc, "%s", $1);
    bool_expression = false;
    int_expression = false;
    real_expression = false;
    math_expression = false;
    char_expression = false;
    string_expression = true;
}
;

pblock: L_PARANTHESIS {if (asc != NULL) fprintf(asc, "(");} parameter_list R_PARANTHESIS {type = $3; if (asc != NULL) fprintf(asc, ")");}
;

parameter_list:
|   non_empty_parameter_list
;

non_empty_parameter_list:   parameter_declaration non_empty_parameter_list_option1
;

non_empty_parameter_list_option1:
|   COMMA {if (asc != NULL) fprintf(asc, ", ");} non_empty_parameter_list
;

parameter_declaration: typeID {if (asc != NULL) fprintf(asc, "%s", $1);} COLON {if (asc != NULL) fprintf(asc, " : ");} identifier { 
	add_parameter_to_type(new_name, $5);
	insert_symbol($5,1,$1,"parameter");
	if (strcmp(new_name, "") != 0) add_to_type(new_name, $1, $5);
}

|     ID COLON {if (asc != NULL) fprintf(asc, ": ");} identifier {if (strcmp(new_name, "") != 0) add_to_type(new_name, $1, $4); insert_symbol($4, 1, $1, "parameter");}
;

ablock: L_PARANTHESIS { put("("); if (asc != NULL) fprintf(asc, "(");} arguement_list R_PARANTHESIS { put(")"); if (asc != NULL) fprintf(asc, ")");}
;

arguement_list:
|   non_empty_arguement_list
;

non_empty_arguement_list:   expression { val6 = val5*val6; left_coercion = false; right_coercion = false;}
|   expression  { val6 = val5*val6; } COMMA {if (asc != NULL) fprintf(asc, ", ");} non_empty_arguement_list
;

memop:  RESERVE { put("reserve"); if (asc != NULL) fprintf(asc, "reserve ");}
|   RELEASE    { put("release"); if (asc != NULL) fprintf(asc, "release ");}
;

identifier: ID {
    if (asc != NULL) fprintf(asc, "%s", $1);
    if (inside_function && strcmp(function_name, $1) == 0) f_var = true;
}
|   typeID {if (asc != NULL) fprintf(asc, " %s ", $1);}
;

constant:   identifier {if (asc != NULL) fprintf(asc, " %s ", $1);}
| C_INTEGER {if (asc != NULL) fprintf(asc, "%i", $1); int_expression = true; int_const = true;}
| C_REAL    {if (asc != NULL) fprintf(asc, "%f", $1); real_expression = true;}
| C_TRUE    {if (asc != NULL) fprintf(asc, "true"); bool_expression = true;}
| C_FALSE    {if (asc != NULL) fprintf(asc, "false");bool_expression = true;}
| C_CHARACTER    {if (asc != NULL) fprintf(asc, "%c", $1); char_expression = true;}
| C_STRING    { val1 = "string"; val2 = $1; if (asc != NULL) fprintf(asc, "%s", $1); string_expression = true;}
;

typeID: T_INTEGER
| T_REAL
| T_BOOLEAN
| T_CHARACTER
| T_STRING
;


%%
/* Additional C code */
#include "lex.yy.c"

int lineno() {
    return n_lines;
}

int columnno() {
    return n_cols;
}

char *checkTab() {
    return tempS1;
}

void yyerror(char* p){
    if (asc != NULL) fprintf(asc, "\nLINE %i:%i ** ERROR: %s\n", lineno(),columnno() , p);
    else printf("\nLINE %i:%i ** ERROR: %s\n", lineno(),columnno() , p);
}
void open_file(){
    asc = fopen("prog.asc", "w");
}

