#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "symbol_table.h" 

extern void yyparse();
extern void yyset_in(FILE *);
extern void print_list();
extern void open_file();
FILE *inputFile;

int main(int argc, char *argv[]){
    if (argc == 1){
        printf("No program to compile\n");
        return 1;
    }
    else{
        if (argc > 2) {
            int operationOrder = 0;
            const char* program = argv[argc-1];
            
            while (operationOrder < argc - 1) {
                inputFile = fopen(program, "r");
                if(!inputFile) return 0;

                
                if (strcmp(argv[operationOrder], "-st") == 0){
                    yyset_in(inputFile);
                    yyparse();
                    write_tofile();
                    char fileName[99];
                    strcpy(fileName, program);
                    strcat(fileName, ".st");
                    int renm;
                    renm = rename("prog.st", fileName);
                }
                else if (strcmp(argv[operationOrder], "-asc") == 0){
                    yyset_in(inputFile);
                    open_file();
                    yyparse();
                    char fileName[99];
                    strcpy(fileName, program);
                    strcat(fileName, ".asc");
                    int renm;
                    renm = rename("prog.asc", fileName);
                }
                else if (strcmp(argv[operationOrder], "-ir") == 0){
                    yyset_in(inputFile);
                    open_IRfile();
                    yyparse();
                    close_IRfile();
                    char fileName[99];
                    strcpy(fileName, program);
                    strcat(fileName, ".ir");
                    int renm;
                    renm = rename("prog.ir", fileName);
                }
                
                else if (strcmp(argv[operationOrder], "-opt") == 0) {
                    operationOrder++;
                    if (strcmp(argv[operationOrder], "grading") == 0) { // Printed all the supported optimizations that we want them to be graded
                        printf("52\n");
                    }
                    else if (strcmp(argv[operationOrder], "supported") == 0) { // Printed all the supported optimizations
                        printf("52\n");
                    }
                    else if (strcmp(argv[operationOrder], "52") == 0) { // All supported optimizations
                        char fileName[99];
                        strcpy(fileName, program);
                        strcat(fileName, ".ir");
                        
                        int renm;
                        
                        FILE *existIR;
                        existIR = fopen(fileName, "r");
                        if (!existIR) {
                            yyset_in(inputFile);
                            open_IRfile();
                            yyparse();
                            close_IRfile();
                            renm = rename("prog.ir", fileName);
                        }
                        
                        int status = system("gcc -o optimizations optimizations.c");
                        char commandLine[99];
                        strcpy(commandLine, "./optimizations 4 ");
                        strcat(commandLine, fileName);
                        status = system(commandLine);
                        status = system("./optimizations 32 prog.opt");
                        status = system("./optimizations 16 prog.opt");
                        int numberOfIte;
                        while (numberOfIte < 5) {   // Number of Optimization Process
                            status = system("./optimizations 32 prog.opt");
                            status = system("./optimizations 16 prog.opt");
                            numberOfIte++;
                        }
                        renm = rename("prog.opt", fileName);
                        status = system("rm -f optimizations");
                    }
                    else if (strcmp(argv[operationOrder], "32") == 0) { // Flow-of-control optimizations
                        char fileName[99];
                        strcpy(fileName, program);
                        strcat(fileName, ".ir");
                        
                        int renm;
                        
                        FILE *existIR;
                        existIR = fopen(fileName, "r");
                        
                        if (!existIR) {
                            inputFile = fopen(program, "r");
                            yyset_in(inputFile);
                            open_IRfile();
                            yyparse();
                            close_IRfile();
                            renm = rename("prog.ir", fileName);
                        } else if (existIR && argc == 4) {
                            char temp[99];
                            strcpy(temp, "rm -f ");
                            strcat(temp, fileName);
                            int removeOldFile = system(temp);
                            inputFile = fopen(program, "r");
                            yyset_in(inputFile);
                            open_IRfile();
                            yyparse();
                            close_IRfile();
                            renm = rename("prog.ir", fileName);
                        }
                        
                        int status = system("gcc -o optimizations optimizations.c");
                        char commandLine[99];
                        strcpy(commandLine, "./optimizations 32 ");
                        strcat(commandLine, fileName);
                        status = system(commandLine);
                        renm = rename("prog.opt", fileName);
                        status = system("rm -f optimizations");
                    }
                    else if (strcmp(argv[operationOrder], "16") == 0) { // Eliminating Unreachable Code
                        char fileName[99];
                        strcpy(fileName, program);
                        strcat(fileName, ".ir");
                        
                        int renm;

                        FILE *existIR;
                        existIR = fopen(fileName, "r");
                        
                        if (!existIR) {
                            inputFile = fopen(program, "r");
                            yyset_in(inputFile);
                            open_IRfile();
                            yyparse();
                            close_IRfile();
                            renm = rename("prog.ir", fileName);
                        } else if (existIR && argc == 4) {
                            char temp[99];
                            strcpy(temp, "rm -f ");
                            strcat(temp, fileName);
                            int removeOldFile = system(temp);
                            inputFile = fopen(program, "r");
                            yyset_in(inputFile);
                            open_IRfile();
                            yyparse();
                            close_IRfile();
                            renm = rename("prog.ir", fileName);
                        }
                        int status = system("gcc -o optimizations optimizations.c");
                        char commandLine[99];
                        strcpy(commandLine, "./optimizations 16 ");
                        strcat(commandLine, fileName);
                        status = system(commandLine);
                        renm = rename("prog.opt", fileName);
                        status = system("rm -f optimizations");
                    }
                    else if (strcmp(argv[operationOrder], "4") == 0) {
                        char fileName[99];
                        strcpy(fileName, program);
                        strcat(fileName, ".ir");
                        
                        int renm;
                        
                        FILE *existIR;
                        existIR = fopen(fileName, "r");
                        
                        if (!existIR) {
                            inputFile = fopen(program, "r");
                            yyset_in(inputFile);
                            open_IRfile();
                            yyparse();
                            close_IRfile();
                            renm = rename("prog.ir", fileName);
                        } else if (existIR && argc == 4) {
                            char temp[99];
                            strcpy(temp, "rm -f ");
                            strcat(temp, fileName);
                            int removeOldFile = system(temp);
                            inputFile = fopen(program, "r");
                            yyset_in(inputFile);
                            open_IRfile();
                            yyparse();
                            close_IRfile();
                            renm = rename("prog.ir", fileName);
                        }
                        int status = system("gcc -o optimizations optimizations.c");
                        char commandLine[99];
                        strcpy(commandLine, "./optimizations 4 ");
                        strcat(commandLine, fileName);
                        status = system(commandLine);
                        renm = rename("prog.opt", fileName);
                        status = system("rm -f optimizations");
                    }
                    else {
                        return 0;
                    }
                }
                operationOrder++;
            }
        }
        else if (argc == 2) {
                const char* program = argv[1];
                inputFile = fopen(program, "r");
                if (!inputFile) return 0;
                yyset_in(inputFile);
                yyparse();
        }
        else {
            return 0;
        }
    }
	return 0;
}
