#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

FILE *file = NULL;
bool flex_scope = false;

void new_scope(int sc){
	FILE * file = fopen("scope.scope", "w");

	if (file != NULL) fprintf(file, "%i",sc);
	fclose(file);
}

void set_behind(bool setter){
	flex_scope = setter;
}

bool behind(){
	return flex_scope;
}

int get_scope(){
	file = fopen("scope.scope", "r");
	int scope;
	if (file != NULL) fscanf(file, "%i", &scope);
	fclose(file);
	return scope;
}
