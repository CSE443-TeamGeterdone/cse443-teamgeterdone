#ifndef SCOPE_PASS_H
#define SCOPE_PASS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


void new_scope(int);
void set_behind(bool);
bool behind();
int get_scope();

#endif
