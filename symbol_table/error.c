#include <stdio.h>
#include <stdlib.h>

#include "error.h"

// DECLARATION ERRORS
void throw_dec_error_1(int line, int col, char *name, FILE *file){	
if(file != NULL) fprintf(file, "\n** ERROR:%i:%i: the name '%s', used here as a type, has not been declared at this point in the program.\n", line, col, name);
else printf("\n** ERROR:%i:%i: the name '%s', used here as a type, has not been declared at this point in the program.\n", line, col, name);
}

void throw_no_ftype_error(int line, int col, char *name, FILE *file){
if (file != NULL) fprintf(file, "\n** ERROR:%i:%i: the name '%s', used here as a function type, has not been declared at this point in the program.\n", line, col, name);
printf("\n** ERROR:%i:%i: the name '%s', used here as a function type, has not been declared at this point in the program.\n", line, col, name);

}



