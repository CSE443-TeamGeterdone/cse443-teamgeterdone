#ifndef ERROR_H
#define ERROR_H

#include <stdio.h>
#include <stdlib.h>



void throw_dec_error_1(int, int, char *, FILE *); // type not declared error
void throw_no_ftype_error(int, int, char *, FILE *); // ftype not declared

#endif
