%{
/* This space is for C code, such as includes, definitions, functions, and
* variables.
*/


int n_lines = 1;
int n_cols = 1;
int temp = 0;

%}
/* This is for your definitions */
/* Any options you want for the lexer will be declared here also */

%option yylineno
%option noyywrap

%%

"integer"               { n_cols+=(temp); temp = strlen(yytext); char* str = strdup(yytext); yylval.s = str; return T_INTEGER; };
"real"                  { n_cols+=(temp); temp = strlen(yytext); char* str = strdup(yytext); yylval.s = str; return T_REAL; };
"Boolean"               { n_cols+=(temp); temp = strlen(yytext); char* str = strdup(yytext); yylval.s = str; return T_BOOLEAN; };
"character"             { n_cols+=(temp); temp = strlen(yytext); char* str = strdup(yytext); yylval.s = str; return T_CHARACTER; };
"string"                { n_cols+=(temp); temp = strlen(yytext); char* str = strdup(yytext); yylval.s = str; return T_STRING; };

"reserve"               { n_cols+=(temp); temp = strlen(yytext); return RESERVE;}
"release"               { n_cols+=(temp); temp = strlen(yytext); return RELEASE;}
"for"                   { n_cols+=(temp); temp = strlen(yytext); return FOR;}
"while"                 { n_cols+=(temp); temp = strlen(yytext); return WHILE;}
"if"                    { n_cols+=(temp); temp = strlen(yytext); return IF;}
"then"                  { n_cols+=(temp); temp = strlen(yytext); return THEN;}
"else"                  { n_cols+=(temp); temp = strlen(yytext); return ELSE;}
"switch"                { n_cols+=(temp); temp = strlen(yytext); return SWITCH;}
"case"                  { n_cols+=(temp); temp = strlen(yytext); return CASE;}
"otherwise"             { n_cols+=(temp); temp = strlen(yytext); return OTHERWISE;}
"type"                  { n_cols+=(temp); temp = strlen(yytext); return TYPE;}
"function"              { n_cols+=(temp); temp = strlen(yytext); return FUNCTION;}

"i2r"                   { n_cols+=(temp); temp = strlen(yytext); return INT2REAL;}
"r2i"                   { n_cols+=(temp); temp = strlen(yytext); return REAL2INT;}
"isNull"                { n_cols+=(temp); temp = strlen(yytext); return IS_NULL;}

"true"                  { n_cols+=(temp); temp = strlen(yytext); yylval.b = true; return C_TRUE;};
"false"                 { n_cols+=(temp); temp = strlen(yytext); yylval.b = false; return C_FALSE;};

("+"|"-")?[0-9]+                                                  { n_cols+=(temp); temp = strlen(yytext); yylval.i = atoi(yytext); return C_INTEGER;};
("+"|"-")?[0-9]+"."[0-9]+(("e"|"E")("+"|"-")?[0-9]+)?             { n_cols+=(temp); temp = strlen(yytext); yylval.f = atof(yytext); return C_REAL;};
\'(\\.|.)\'             { n_cols+=(temp); temp = strlen(yytext); yylval.ch = yytext[1]; return C_CHARACTER;};

"("                     { n_cols+=(temp); temp = strlen(yytext); return L_PARANTHESIS;}
")"                     { n_cols+=(temp); temp = strlen(yytext); return R_PARANTHESIS;}
"{"                     { n_cols+=(temp); temp = strlen(yytext); return L_BRACE;}
"}"                     { n_cols+=(temp); temp = strlen(yytext); return R_BRACE;}
"["                     { n_cols+=(temp); temp = strlen(yytext); return L_BRACKET;}
"]"                     { n_cols+=(temp); temp = strlen(yytext); return R_BRACKET;}
"'"                     { n_cols+=(temp); temp = strlen(yytext); return S_QUOTE;}
["]                     { n_cols+=(temp); temp = strlen(yytext); return D_QUOTE;}

";"                     { n_cols+=(temp); temp = strlen(yytext); return SEMI_COLON;}
":"                     { n_cols+=(temp); temp = strlen(yytext); return COLON;}
","                     { n_cols+=(temp); temp = strlen(yytext); return COMMA;}
"->"                    { n_cols+=(temp); temp = strlen(yytext); return ARROW;}

"+"                     { n_cols+=(temp); temp = strlen(yytext); return ADD;}
"-"                     { n_cols+=(temp); temp = strlen(yytext); return SUB_OR_NEG;}
"*"                     { n_cols+=(temp); temp = strlen(yytext); return MUL;}
"/"                     { n_cols+=(temp); temp = strlen(yytext); return DIV;}
"%"                     { n_cols+=(temp); temp = strlen(yytext); return REM;}

"."                     { n_cols+=(temp); temp = strlen(yytext); return DOT;}
"<"                     { n_cols+=(temp); temp = strlen(yytext); return LESS_THAN;}
"="                     { n_cols+=(temp); temp = strlen(yytext); return EQUAL_TO;}
":="                    { n_cols+=(temp); temp = strlen(yytext); return ASSIGN;}
"!"                     { n_cols+=(temp); temp = strlen(yytext); return NOT;}
"&"                     { n_cols+=(temp); temp = strlen(yytext); return AND;}
"|"                     { n_cols+=(temp); temp = strlen(yytext); return OR;}

"(*"([^*]*[*]*)(([^*)][^*]*[*]*)*)"*)"  { n_cols+=(temp); temp = strlen(yytext); for (int i=0; i<strlen(yytext);i++) { if (yytext[i]=='\n') { n_lines+=1;} };}
[A-Za-z][A-Za-z0-9]*    { n_cols+=(temp); temp = strlen(yytext); char* str = strdup(yytext); yylval.s = str; return ID;};
\"([^\n\"]|\\.)*\"      { n_cols+=(temp); temp = strlen(yytext); char* str = strdup(yytext); yylval.s = str; return C_STRING; };

[ \t]*                  n_cols+=(strlen(yytext));
[\n]                    {temp = 1; n_cols=0; n_lines++;}
" "*                    n_cols+=strlen(yytext);


%%
