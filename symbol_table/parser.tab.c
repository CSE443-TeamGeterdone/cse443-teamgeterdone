/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "parser.y" /* yacc.c:339  */

    #include <stdio.h>
    #include <stdbool.h>

    #include "symbol_table.h"
    #include "error.h"

    extern int lineno();
    extern int columnno();
    int yylex();
    
    void yyerror(char* p);
    
    char *name;
    char *new_name;
    char *type;
    char *variable;
    int scope = 0;

    FILE *asc = NULL;

    struct Node {
        int i;
        char* s;
        bool b;
        char ch;
        float f;
    };
    
    

#line 98 "parser.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    ID = 101,
    T_INTEGER = 201,
    T_REAL = 202,
    T_BOOLEAN = 203,
    T_CHARACTER = 204,
    T_STRING = 205,
    C_INTEGER = 301,
    C_REAL = 302,
    C_CHARACTER = 303,
    C_STRING = 304,
    C_TRUE = 305,
    C_FALSE = 306,
    NULL_PTR = 401,
    RESERVE = 402,
    RELEASE = 403,
    FOR = 404,
    WHILE = 405,
    IF = 406,
    THEN = 407,
    ELSE = 408,
    SWITCH = 409,
    CASE = 410,
    OTHERWISE = 411,
    TYPE = 412,
    FUNCTION = 413,
    CLOSURE = 414,
    L_PARANTHESIS = 501,
    R_PARANTHESIS = 502,
    L_BRACKET = 503,
    R_BRACKET = 504,
    L_BRACE = 505,
    R_BRACE = 506,
    S_QUOTE = 507,
    D_QUOTE = 508,
    SEMI_COLON = 551,
    COLON = 552,
    COMMA = 553,
    ARROW = 554,
    BACKSLASH = 555,
    ADD = 601,
    SUB_OR_NEG = 602,
    MUL = 603,
    DIV = 604,
    REM = 605,
    DOT = 606,
    LESS_THAN = 607,
    EQUAL_TO = 608,
    ASSIGN = 609,
    INT2REAL = 610,
    REAL2INT = 611,
    IS_NULL = 612,
    NOT = 613,
    AND = 614,
    OR = 615,
    COMMENT = 700
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 102 "parser.y" /* yacc.c:355  */

    int i;
    bool b;
    char* s;
    char ch;
    struct Node n;
    float f;

#line 202 "parser.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);



/* Copy the second part of user declarations.  */

#line 219 "parser.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   303

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  58
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  89
/* YYNRULES -- Number of rules.  */
#define YYNRULES  148
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  236

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   701

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     3,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     4,     5,     6,     7,     8,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     9,    10,    11,    12,    13,    14,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    29,    30,    31,    32,    33,    34,    35,    36,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    37,    38,    39,    40,    41,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      57,     2
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   159,   159,   159,   162,   163,   163,   166,   166,   166,
     167,   167,   167,   175,   175,   176,   176,   176,   182,   182,
     190,   191,   191,   194,   194,   195,   195,   198,   198,   201,
     201,   202,   205,   205,   205,   206,   206,   209,   212,   213,
     213,   214,   214,   217,   218,   218,   221,   222,   225,   225,
     225,   225,   225,   226,   226,   227,   227,   227,   227,   228,
     228,   228,   228,   229,   230,   230,   230,   231,   234,   234,
     234,   237,   238,   241,   242,   243,   243,   246,   246,   247,
     247,   248,   248,   249,   249,   250,   251,   252,   252,   253,
     253,   254,   254,   255,   255,   256,   256,   257,   257,   258,
     258,   259,   259,   260,   260,   261,   261,   262,   263,   264,
     265,   266,   267,   270,   270,   273,   274,   277,   280,   281,
     281,   284,   284,   284,   285,   285,   288,   288,   291,   292,
     295,   296,   296,   299,   300,   303,   304,   307,   308,   309,
     310,   311,   312,   313,   316,   317,   318,   319,   320
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "ID", "T_INTEGER", "T_REAL", "T_BOOLEAN",
  "T_CHARACTER", "T_STRING", "C_INTEGER", "C_REAL", "C_CHARACTER",
  "C_STRING", "C_TRUE", "C_FALSE", "NULL_PTR", "RESERVE", "RELEASE", "FOR",
  "WHILE", "IF", "THEN", "ELSE", "SWITCH", "CASE", "OTHERWISE", "TYPE",
  "FUNCTION", "CLOSURE", "L_PARANTHESIS", "R_PARANTHESIS", "L_BRACKET",
  "R_BRACKET", "L_BRACE", "R_BRACE", "S_QUOTE", "D_QUOTE", "SEMI_COLON",
  "COLON", "COMMA", "ARROW", "BACKSLASH", "ADD", "SUB_OR_NEG", "MUL",
  "DIV", "REM", "DOT", "LESS_THAN", "EQUAL_TO", "ASSIGN", "INT2REAL",
  "REAL2INT", "IS_NULL", "NOT", "AND", "OR", "COMMENT", "$accept",
  "program", "$@1", "definition_list", "$@2", "definition", "$@3", "$@4",
  "$@5", "$@6", "definition_option1", "$@7", "$@8", "$@9", "$@10",
  "definition_option2", "$@11", "sblock", "$@12", "$@13", "dblock", "$@14",
  "declaration_list", "$@15", "declaration", "$@16", "$@17", "$@18",
  "identifier_list", "identifier_list_option", "$@19", "$@20",
  "comma_option", "$@21", "statement_list", "statement", "$@22", "$@23",
  "$@24", "$@25", "$@26", "$@27", "$@28", "$@29", "$@30", "$@31", "$@32",
  "$@33", "$@34", "case_singular", "$@35", "$@36", "case_plural",
  "assignable", "$@37", "expression", "$@38", "$@39", "$@40", "$@41",
  "$@42", "$@43", "$@44", "$@45", "$@46", "$@47", "$@48", "$@49", "$@50",
  "$@51", "pblock", "$@52", "parameter_list", "non_empty_parameter_list",
  "non_empty_parameter_list_option1", "$@53", "parameter_declaration",
  "$@54", "$@55", "$@56", "ablock", "$@57", "arguement_list",
  "non_empty_arguement_list", "$@58", "memop", "identifier", "constant",
  "typeID", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   701,   101,   201,   202,   203,   204,   205,   301,
     302,   303,   304,   305,   306,   401,   402,   403,   404,   405,
     406,   407,   408,   409,   410,   411,   412,   413,   414,   501,
     502,   503,   504,   505,   506,   507,   508,   551,   552,   553,
     554,   555,   601,   602,   603,   604,   605,   606,   607,   608,
     609,   610,   611,   612,   613,   614,   615,   700
};
# endif

#define YYPACT_NINF -186

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-186)))

#define YYTABLE_NINF -48

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      -6,    36,  -186,    15,  -186,     6,  -186,  -186,    -6,    35,
    -186,    71,    71,  -186,   186,    40,  -186,  -186,  -186,  -186,
    -186,  -186,     8,  -186,    51,  -186,  -186,    43,    61,    64,
      74,  -186,    57,    31,    71,    71,  -186,   186,  -186,  -186,
    -186,  -186,  -186,  -186,  -186,  -186,   -19,  -186,    33,   116,
      75,    16,    71,   186,     9,     9,     9,  -186,  -186,  -186,
    -186,  -186,    93,    80,    76,  -186,  -186,  -186,  -186,  -186,
      40,    94,     6,    98,  -186,  -186,  -186,  -186,  -186,  -186,
    -186,  -186,  -186,  -186,  -186,   -18,    62,    84,   115,     9,
      71,     9,  -186,  -186,  -186,   100,    96,   295,  -186,  -186,
    -186,  -186,     9,     9,     9,     9,     9,  -186,  -186,  -186,
    -186,  -186,  -186,  -186,  -186,  -186,  -186,  -186,   120,  -186,
     214,   112,  -186,  -186,   184,    71,   116,  -186,  -186,   106,
     117,  -186,   107,  -186,    71,     9,   144,   -12,   229,   229,
      41,     9,     9,     9,     9,     9,     9,     9,     9,     9,
    -186,   124,  -186,  -186,  -186,  -186,   -15,  -186,    71,    71,
    -186,  -186,  -186,  -186,   113,  -186,   199,  -186,   -12,   -12,
    -186,  -186,  -186,    41,    41,    41,    41,     6,  -186,   127,
       9,  -186,   283,  -186,  -186,   128,    71,   295,  -186,  -186,
     131,   283,   129,  -186,    71,  -186,  -186,  -186,  -186,  -186,
    -186,  -186,  -186,   125,  -186,  -186,  -186,    71,   186,  -186,
     137,  -186,  -186,   123,  -186,  -186,   135,     6,  -186,     6,
    -186,  -186,   283,  -186,  -186,     6,  -186,    71,   139,     6,
     124,  -186,  -186,  -186,  -186,  -186
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       5,     0,     2,     0,     1,     0,     7,    10,     5,    23,
       3,     0,     0,     6,    64,     0,   135,   144,   145,   146,
     147,   148,     0,   136,     0,   133,   134,     0,     0,     0,
       0,    63,     0,    64,     0,     0,    27,    64,     8,    11,
      48,    53,    55,    59,    24,    46,     0,    73,     0,     0,
       0,    13,     0,    64,     0,     0,     0,   126,    75,    65,
      74,    67,     0,     0,    31,    32,    26,    15,   113,     9,
       0,     0,     0,     0,   107,   108,   111,   112,   109,   110,
     105,    77,    81,    83,    79,    86,     0,     0,     0,   128,
       0,     0,    35,    28,    29,     0,     0,   115,    14,    18,
      12,    49,     0,     0,     0,     0,     0,    54,    87,    89,
      91,    93,    95,   101,   103,    85,    97,    99,     0,    60,
     130,     0,   129,    76,     0,     0,     0,    33,    16,     0,
       0,   116,   118,   121,     0,     0,     0,    78,    82,    84,
      80,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      56,     0,   131,   127,    66,    36,    38,    30,     0,     0,
     124,   114,   119,   117,     0,    19,     0,   106,    88,    90,
      92,    94,    96,   102,   104,    98,   100,     0,    68,     0,
       0,    39,     0,    37,    34,    20,     0,     0,   122,    50,
       0,     0,     0,   132,     0,   138,   139,   142,   143,   140,
     141,   137,    41,     0,    17,   125,   120,     0,    64,    57,
       0,    61,    40,    43,    21,   123,     0,     0,    69,     0,
      44,    42,     0,    51,    58,     0,    62,     0,     0,     0,
      71,    45,    22,    52,    72,    70
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -186,  -186,  -186,   147,  -186,  -186,  -186,  -186,  -186,  -186,
    -186,  -186,  -186,  -186,  -186,  -186,  -186,    -5,  -186,  -186,
     108,  -186,    50,  -186,  -186,  -186,  -186,  -186,  -154,  -186,
    -186,  -186,  -186,  -186,    -7,   -52,  -186,  -186,  -186,  -186,
    -186,  -186,  -186,  -186,  -186,  -186,  -186,  -186,  -186,   -51,
    -186,  -186,  -186,    34,  -186,   -47,  -186,  -186,  -186,  -186,
    -186,  -186,  -186,  -186,  -186,  -186,  -186,  -186,  -186,  -186,
    -186,  -186,  -186,    -4,  -186,  -186,  -186,  -186,  -186,  -186,
    -186,  -186,  -186,     0,  -186,  -186,    -9,  -185,   -44
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     5,     2,     3,     8,    11,    51,    12,    52,
      69,    70,    96,   159,   134,   204,   222,    31,    14,    15,
      37,    49,    63,   126,    64,    95,   158,   125,   155,   183,
     194,   213,   221,   227,    32,    33,    53,   135,   208,   229,
      54,    55,   177,   217,    56,   151,   219,    34,    91,   179,
     191,   225,   235,    85,    90,   120,   103,   106,   104,   105,
     141,   142,   143,   144,   145,   148,   149,   146,   147,   102,
      71,    97,   130,   131,   163,   187,   132,   164,   207,   186,
      60,    89,   121,   122,   180,    35,    47,   202,    23
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      10,    73,    22,    24,   184,    65,   210,    86,    87,    88,
      57,    57,    16,    17,    18,    19,    20,    21,    74,    75,
      76,    77,    78,    79,   181,    67,    45,    -4,    58,    58,
      50,    59,   110,   111,   112,   182,     4,   228,    80,     9,
     212,     6,     7,    72,   124,    68,    38,    25,    26,    27,
      28,    29,    81,   133,    30,   136,   137,   138,   139,   140,
      82,    83,    57,    84,     9,   -47,   -25,   100,    46,    48,
      61,    36,    40,   231,    16,    17,    18,    19,    20,    21,
      58,   123,    65,   108,   109,   110,   111,   112,   166,    39,
      41,    44,   107,    42,   168,   169,   170,   171,   172,   173,
     174,   175,   176,    43,   108,   109,   110,   111,   112,    66,
     113,   114,    93,    94,   118,   115,   156,   116,   117,    62,
      17,    18,    19,    20,    21,   165,   108,   109,   110,   111,
     112,    92,   113,   114,    99,   101,   128,   115,   127,   116,
     117,   150,   153,   133,   160,   119,   162,   161,   178,   156,
     185,   188,   192,   209,   214,    13,   216,   108,   109,   110,
     111,   112,   220,   113,   114,   223,   203,   211,   115,   232,
     116,   117,   190,   201,   167,   218,   157,   205,    98,   234,
     193,     0,   201,   206,     0,   156,   108,   109,   110,   111,
     112,     0,   113,   114,     0,     0,     0,   115,   215,   116,
     117,     0,    25,    26,    27,    28,    29,     0,     0,    30,
       0,     0,   224,   201,   226,     0,     0,     0,   156,     9,
     230,   154,     0,     0,   233,     0,   108,   109,   110,   111,
     112,     0,   113,   114,     0,     0,   189,   115,     0,   116,
     117,   108,   109,   110,   111,   112,     0,   113,   114,     0,
       0,     0,   115,   152,   116,   117,   108,   109,   110,   111,
     112,     0,   113,   114,     0,     0,     0,   115,     0,   116,
     117,   108,   109,   110,   111,   112,     0,   113,   114,     0,
       0,     0,   115,     0,   116,   117,    16,    17,    18,    19,
      20,    21,   195,   196,   197,   198,   199,   200,   129,    17,
      18,    19,    20,    21
};

static const yytype_int16 yycheck[] =
{
       5,    53,    11,    12,   158,    49,   191,    54,    55,    56,
      29,    29,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    13,    14,    39,     9,    33,    33,    47,    47,
      37,    50,    44,    45,    46,    50,     0,   222,    29,    33,
     194,    26,    27,    52,    91,    29,    38,    16,    17,    18,
      19,    20,    43,    97,    23,   102,   103,   104,   105,   106,
      51,    52,    29,    54,    33,    34,    31,    72,    34,    35,
      37,    31,    29,   227,     3,     4,     5,     6,     7,     8,
      47,    90,   126,    42,    43,    44,    45,    46,   135,    38,
      29,    34,    30,    29,   141,   142,   143,   144,   145,   146,
     147,   148,   149,    29,    42,    43,    44,    45,    46,    34,
      48,    49,    32,    37,    30,    53,   125,    55,    56,     3,
       4,     5,     6,     7,     8,   134,    42,    43,    44,    45,
      46,    38,    48,    49,    40,    37,    40,    53,    38,    55,
      56,    21,    30,   187,    38,    30,    39,    30,    24,   158,
     159,    38,    25,    22,    29,     8,   208,    42,    43,    44,
      45,    46,    39,    48,    49,    30,    38,    38,    53,    30,
      55,    56,   177,   182,    30,    38,   126,   186,    70,   230,
     180,    -1,   191,   187,    -1,   194,    42,    43,    44,    45,
      46,    -1,    48,    49,    -1,    -1,    -1,    53,   207,    55,
      56,    -1,    16,    17,    18,    19,    20,    -1,    -1,    23,
      -1,    -1,   217,   222,   219,    -1,    -1,    -1,   227,    33,
     225,    37,    -1,    -1,   229,    -1,    42,    43,    44,    45,
      46,    -1,    48,    49,    -1,    -1,    37,    53,    -1,    55,
      56,    42,    43,    44,    45,    46,    -1,    48,    49,    -1,
      -1,    -1,    53,    39,    55,    56,    42,    43,    44,    45,
      46,    -1,    48,    49,    -1,    -1,    -1,    53,    -1,    55,
      56,    42,    43,    44,    45,    46,    -1,    48,    49,    -1,
      -1,    -1,    53,    -1,    55,    56,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    13,    14,     3,     4,
       5,     6,     7,     8
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    59,    61,    62,     0,    60,    26,    27,    63,    33,
      75,    64,    66,    61,    76,    77,     3,     4,     5,     6,
       7,     8,   144,   146,   144,    16,    17,    18,    19,    20,
      23,    75,    92,    93,   105,   143,    31,    78,    38,    38,
      29,    29,    29,    29,    34,    92,   111,   144,   111,    79,
      92,    65,    67,    94,    98,    99,   102,    29,    47,    50,
     138,    37,     3,    80,    82,   146,    34,     9,    29,    68,
      69,   128,   144,    93,     9,    10,    11,    12,    13,    14,
      29,    43,    51,    52,    54,   111,   113,   113,   113,   139,
     112,   106,    38,    32,    37,    83,    70,   129,    78,    40,
      75,    37,   127,   114,   116,   117,   115,    30,    42,    43,
      44,    45,    46,    48,    49,    53,    55,    56,    30,    30,
     113,   140,   141,   144,   113,    85,    81,    38,    40,     3,
     130,   131,   134,   146,    72,    95,   113,   113,   113,   113,
     113,   118,   119,   120,   121,   122,   125,   126,   123,   124,
      21,   103,    39,    30,    37,    86,   144,    80,    84,    71,
      38,    30,    39,   132,   135,   144,   113,    30,   113,   113,
     113,   113,   113,   113,   113,   113,   113,   100,    24,   107,
     142,    39,    50,    87,    86,   144,   137,   133,    38,    37,
      75,   108,    25,   141,    88,     9,    10,    11,    12,    13,
      14,   144,   145,    38,    73,   144,   131,   136,    96,    22,
     145,    38,    86,    89,    29,   144,    93,   101,    38,   104,
      39,    90,    74,    30,    75,   109,    75,    91,   145,    97,
      75,    86,    30,    75,   107,   110
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    58,    60,    59,    61,    62,    61,    64,    65,    63,
      66,    67,    63,    69,    68,    70,    71,    68,    72,    68,
      73,    74,    73,    76,    75,    77,    75,    79,    78,    81,
      80,    80,    83,    84,    82,    85,    82,    86,    87,    88,
      87,    89,    87,    90,    91,    90,    92,    92,    94,    95,
      96,    97,    93,    98,    93,    99,   100,   101,    93,   102,
     103,   104,    93,    93,   105,   106,    93,    93,   108,   109,
     107,   110,   110,   111,   111,   112,   111,   114,   113,   115,
     113,   116,   113,   117,   113,   113,   113,   118,   113,   119,
     113,   120,   113,   121,   113,   122,   113,   123,   113,   124,
     113,   125,   113,   126,   113,   127,   113,   113,   113,   113,
     113,   113,   113,   129,   128,   130,   130,   131,   132,   133,
     132,   135,   136,   134,   137,   134,   139,   138,   140,   140,
     141,   142,   141,   143,   143,   144,   144,   145,   145,   145,
     145,   145,   145,   145,   146,   146,   146,   146,   146
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     3,     0,     0,     3,     0,     0,     6,
       0,     0,     7,     0,     2,     0,     0,     6,     0,     4,
       0,     0,     5,     0,     4,     0,     5,     0,     4,     0,
       4,     1,     0,     0,     5,     0,     4,     2,     0,     0,
       3,     0,     4,     0,     0,     3,     2,     1,     0,     0,
       0,     0,    13,     0,     5,     0,     0,     0,    11,     0,
       0,     0,    11,     1,     0,     0,     6,     3,     0,     0,
       7,     0,     1,     1,     2,     0,     4,     0,     3,     0,
       3,     0,     3,     0,     3,     2,     1,     0,     4,     0,
       4,     0,     4,     0,     4,     0,     4,     0,     4,     0,
       4,     0,     4,     0,     4,     0,     4,     1,     1,     1,
       1,     1,     1,     0,     4,     0,     1,     2,     0,     0,
       3,     0,     0,     5,     0,     4,     0,     4,     0,     1,
       1,     0,     4,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 159 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%i:%i:%i: ", lineno(), 1, scope);}
#line 1543 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 163 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%i:%i:%i: ", lineno(), columnno(), scope);}
#line 1549 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 166 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "type ");}
#line 1555 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 166 "parser.y" /* yacc.c:1646  */
    {new_name = (yyvsp[-1].s); insert_symbol((yyvsp[-1].s), 0, "", ""); if (asc != NULL) fprintf(asc, ": ");}
#line 1561 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 167 "parser.y" /* yacc.c:1646  */
    {scope++; if (asc != NULL) fprintf(asc, "function ");}
#line 1567 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 167 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " : ");}
#line 1573 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 167 "parser.y" /* yacc.c:1646  */
    {

	if (type_exists((yyvsp[-1].s), true)) insert_symbol((yyvsp[-4].s), 0, (yyvsp[-1].s), "function");
	else throw_no_ftype_error(lineno(), columnno(), (yyvsp[-1].s), asc);
	scope--;
}
#line 1584 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 175 "parser.y" /* yacc.c:1646  */
    {change_extra(new_name, "rtype");}
#line 1590 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 176 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " %i", (yyvsp[0].i));}
#line 1596 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 176 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "-> ");}
#line 1602 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 176 "parser.y" /* yacc.c:1646  */
    {
	change_extra(new_name, "atype"); 
	change_type(new_name, (yyvsp[-1].s));
	add_array_size(new_name, (yyvsp[-5].i));
	new_name = "";
}
#line 1613 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 182 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "-> ");}
#line 1619 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 182 "parser.y" /* yacc.c:1646  */
    {
	change_type(new_name, (yyvsp[0].s)); 
	change_extra(new_name, "ftype"); 
	new_name = "";
	if (asc != NULL) fprintf(asc, "\n");
}
#line 1630 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 191 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ": (");}
#line 1636 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 191 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ")");}
#line 1642 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 194 "parser.y" /* yacc.c:1646  */
    {scope++; if (asc != NULL) fprintf(asc, "{\n");}
#line 1648 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 194 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%i:%i:%i: }\n", lineno(), columnno(), scope);}
#line 1654 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 195 "parser.y" /* yacc.c:1646  */
    {scope++; if (asc != NULL) fprintf(asc, " {\n");}
#line 1660 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 195 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%i:%i:%i: }\n", lineno(), 1, scope);}
#line 1666 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 198 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%i:%i:%i: [ ", lineno(),1, scope);}
#line 1672 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 198 "parser.y" /* yacc.c:1646  */
    {new_name = "";if (asc != NULL) fprintf(asc, " ]\n");}
#line 1678 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 201 "parser.y" /* yacc.c:1646  */
    {add_to_type(new_name, variable); if (asc != NULL) fprintf(asc, "; ");}
#line 1684 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 202 "parser.y" /* yacc.c:1646  */
    {add_to_type(new_name, variable);}
#line 1690 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 205 "parser.y" /* yacc.c:1646  */
    {variable = (yyvsp[0].s);}
#line 1696 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 205 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%s : ", (yyvsp[-2].s));}
#line 1702 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 206 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%s : ", (yyvsp[-1].s)); }
#line 1708 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 206 "parser.y" /* yacc.c:1646  */
    { if (exists((yyvsp[-3].s))) {throw_dec_error_1(lineno(), columnno(), (yyvsp[-3].s), asc); }}
#line 1714 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 209 "parser.y" /* yacc.c:1646  */
    { insert_symbol((yyvsp[-1].s), scope, variable, "local");}
#line 1720 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 213 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ", ");}
#line 1726 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 214 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ":= ");}
#line 1732 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 218 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ", ");}
#line 1738 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 225 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "for (");}
#line 1744 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 225 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "; ");}
#line 1750 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 225 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "; ");}
#line 1756 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 225 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ")");}
#line 1762 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 226 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "while (");}
#line 1768 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 226 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ")");}
#line 1774 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 227 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%i:%i:%i: if (", lineno(), columnno(), scope);}
#line 1780 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 227 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ") then");}
#line 1786 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 227 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%i:%i:%i: else", lineno(), columnno(), scope);}
#line 1792 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 228 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "switch (");}
#line 1798 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 228 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ")");}
#line 1804 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 228 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "otherwise : ");}
#line 1810 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 230 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%i:%i:%i: ", lineno(), 1, scope);}
#line 1816 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 230 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " := ");}
#line 1822 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 230 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ";\n");}
#line 1828 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 231 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ";\n");}
#line 1834 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 234 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "case");}
#line 1840 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 234 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ": ");}
#line 1846 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 243 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ".");}
#line 1852 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 246 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "-");}
#line 1858 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 247 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "!");}
#line 1864 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 248 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " i2r ");}
#line 1870 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 249 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " r2i ");}
#line 1876 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 250 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " isNull ");}
#line 1882 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 87:
#line 252 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "+");}
#line 1888 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 89:
#line 253 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " - ");}
#line 1894 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 91:
#line 254 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " * ");}
#line 1900 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 93:
#line 255 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " / ");}
#line 1906 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 95:
#line 256 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " %% ");}
#line 1912 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 97:
#line 257 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " & ");}
#line 1918 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 99:
#line 258 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " | ");}
#line 1924 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 101:
#line 259 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " < ");}
#line 1930 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 103:
#line 260 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " = ");}
#line 1936 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 105:
#line 261 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "(");}
#line 1942 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 106:
#line 261 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ")");}
#line 1948 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 107:
#line 262 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " %i ", (yyvsp[0].i));}
#line 1954 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 108:
#line 263 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " %f", (yyvsp[0].f));}
#line 1960 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 109:
#line 264 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " true ");}
#line 1966 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 110:
#line 265 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " false ");}
#line 1972 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 111:
#line 266 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " %c ", (yyvsp[0].ch));}
#line 1978 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 112:
#line 267 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " %s ", (yyvsp[0].s));}
#line 1984 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 113:
#line 270 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "(");}
#line 1990 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 114:
#line 270 "parser.y" /* yacc.c:1646  */
    {type = (yyvsp[-1].s); if (asc != NULL) fprintf(asc, ")");}
#line 1996 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 119:
#line 281 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ", ");}
#line 2002 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 121:
#line 284 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " %s", (yyvsp[0].s));}
#line 2008 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 122:
#line 284 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " : ");}
#line 2014 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 123:
#line 284 "parser.y" /* yacc.c:1646  */
    {insert_symbol((yyvsp[0].s),1,(yyvsp[-4].s),"parameter");}
#line 2020 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 124:
#line 285 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ": ");}
#line 2026 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 125:
#line 285 "parser.y" /* yacc.c:1646  */
    {insert_symbol((yyvsp[0].s), 1, (yyvsp[-3].s), "parameter");}
#line 2032 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 126:
#line 288 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "(");}
#line 2038 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 127:
#line 288 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ")");}
#line 2044 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 131:
#line 296 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, ", ");}
#line 2050 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 133:
#line 299 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "reserve ");}
#line 2056 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 134:
#line 300 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "release ");}
#line 2062 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 135:
#line 303 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%s", (yyvsp[0].s));}
#line 2068 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 136:
#line 304 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " %s ", (yyvsp[0].s));}
#line 2074 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 137:
#line 307 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, " %s ", (yyvsp[0].s));}
#line 2080 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 138:
#line 308 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%i", (yyvsp[0].i));}
#line 2086 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 139:
#line 309 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%f", (yyvsp[0].f));}
#line 2092 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 140:
#line 310 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "true");}
#line 2098 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 141:
#line 311 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "false");}
#line 2104 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 142:
#line 312 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%c", (yyvsp[0].ch));}
#line 2110 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 143:
#line 313 "parser.y" /* yacc.c:1646  */
    {if (asc != NULL) fprintf(asc, "%s", (yyvsp[0].s));}
#line 2116 "parser.tab.c" /* yacc.c:1646  */
    break;


#line 2120 "parser.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 324 "parser.y" /* yacc.c:1906  */

/* Additional C code */
#include "lex.yy.c"

int lineno() {
    return n_lines;
}

int columnno() {
    return n_cols;
}

void yyerror(char* p){
    if (asc != NULL) fprintf(asc, "\nLINE %i:%i ** ERROR: %s\n", lineno(),columnno() , p);
}
void open_file(){
	asc = fopen("prog.asc", "w");
}
