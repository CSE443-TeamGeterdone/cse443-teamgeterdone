%{
    #include <stdio.h>
    #include <stdbool.h>

    #include "symbol_table.h"
    #include "error.h"

    extern int lineno();
    extern int columnno();
    int yylex();
    
    void yyerror(char* p);
    
    char *name;
    char *new_name;
    char *type;
    char *variable;
    int scope = 0;

    FILE *asc = NULL;

    struct Node {
        int i;
        char* s;
        bool b;
        char ch;
        float f;
    };
    
    %}
/* Bison declarations */

%token ID 101

/*type names*/
%token T_INTEGER 201
%token T_REAL 202
%token T_BOOLEAN 203
%token T_CHARACTER 204
%token T_STRING 205

/*constants*/
%token C_INTEGER 301
%token C_REAL 302
%token C_CHARACTER 303
%token C_STRING 304
%token C_TRUE 305
%token C_FALSE 306

/*keywords*/
%token NULL_PTR 401
%token RESERVE 402
%token RELEASE 403
%token FOR 404
%token WHILE 405
%token IF 406
%token THEN 407
%token ELSE 408
%token SWITCH 409
%token CASE 410
%token OTHERWISE 411
%token TYPE 412
%token FUNCTION 413
%token CLOSURE 414

/*punctuation - grouping*/
%token L_PARANTHESIS 501
%token R_PARANTHESIS 502
%token L_BRACKET 503
%token R_BRACKET 504
%token L_BRACE 505
%token R_BRACE 506
%token S_QUOTE 507
%token D_QUOTE 508

/*other punctuation*/
%token SEMI_COLON 551
%token COLON 552
%token COMMA 553
%token ARROW 554
%token BACKSLASH 555

/*operators*/
%token ADD 601
%token SUB_OR_NEG 602
%token MUL 603
%token DIV 604
%token REM 605
%token DOT 606
%token LESS_THAN 607
%token EQUAL_TO 608
%token ASSIGN 609
%token INT2REAL 610
%token REAL2INT 611
%token IS_NULL 612
%token NOT 613
%token AND 614
%token OR 615

%token COMMENT 700

%union{
    int i;
    bool b;
    char* s;
    char ch;
    struct Node n;
    float f;
}

/* Define operator precedence */
%right ASSIGN DOT/* lowest precedence */
%left INT2REAL REAL2INT
%left AND OR LESS_THAN EQUAL_TO NOT IS_NULL
%left ADD SUB_OR_NEG
%left MUL DIV REM /* Highest precedence */
%left L_PARANTHESIS R_PARANTHESIS
/* Define types of tokens */

%type <s> ID

%type <i> C_INTEGER
%type <f> C_REAL
%type <b> C_TRUE
%type <b> C_FALSE
%type <ch> C_CHARACTER
%type <s> C_STRING
%type <s> T_INTEGER
%type <s> T_REAL
%type <s> T_BOOLEAN
%type <s> T_CHARACTER
%type <s> T_STRING
%type <s> definition
%type <s> definition_list
%type <s> identifier
%type <n> constant
%type <s> parameter_declaration
%type <s> non_empty_parameter_list
%type <s> parameter_list
%type <s> definition_option1
%type <s> pblock
%type <s> declaration
%type <s> declaration_list
%type <s> identifier_list
%type <s> assignable
%type <n> expression
%type <s> typeID

/*%type <s> COMMENT
 
 %typROR:%i:%i: the name '%s', used here as a type, has not been declared at this point in the program.", line, col, name);

 */
/* Where the grammar starts */
%start program

%%

program: definition_list {if (asc != NULL) fprintf(asc, "%i:%i:%i: ", lineno(), 1, scope);} sblock
;

definition_list:
| {if (asc != NULL) fprintf(asc, "%i:%i:%i: ", lineno(), columnno(), scope);} definition definition_list 
;

definition: TYPE {if (asc != NULL) fprintf(asc, "type ");} identifier COLON {new_name = $3; insert_symbol($3, 0, "", ""); if (asc != NULL) fprintf(asc, ": ");} definition_option1  
|   FUNCTION {scope++; if (asc != NULL) fprintf(asc, "function ");} identifier COLON {if (asc != NULL) fprintf(asc, " : ");} identifier sblock {

	if (type_exists($6, true)) insert_symbol($3, 0, $6, "function");
	else throw_no_ftype_error(lineno(), columnno(), $6, asc);
	scope--;
}
;

definition_option1: {change_extra(new_name, "rtype");} dblock
|  C_INTEGER {if (asc != NULL) fprintf(asc, " %i", $1);} ARROW {if (asc != NULL) fprintf(asc, "-> ");} identifier definition_option2 {
	change_extra(new_name, "atype"); 
	change_type(new_name, $5);
	add_array_size(new_name, $1);
	new_name = "";
} 
|  pblock ARROW {if (asc != NULL) fprintf(asc, "-> ");} identifier {
	change_type(new_name, $4); 
	change_extra(new_name, "ftype"); 
	new_name = "";
	if (asc != NULL) fprintf(asc, "\n");
}
;

definition_option2:
|  COLON L_PARANTHESIS {if (asc != NULL) fprintf(asc, ": (");} constant R_PARANTHESIS {if (asc != NULL) fprintf(asc, ")");}
;

sblock: L_BRACE{scope++; if (asc != NULL) fprintf(asc, "{\n");} statement_list R_BRACE {if (asc != NULL) fprintf(asc, "%i:%i:%i: }\n", lineno(), columnno(), scope);}
| L_BRACE {scope++; if (asc != NULL) fprintf(asc, " {\n");} dblock statement_list R_BRACE {if (asc != NULL) fprintf(asc, "%i:%i:%i: }\n", lineno(), 1, scope);}
;

dblock: L_BRACKET{if (asc != NULL) fprintf(asc, "%i:%i:%i: [ ", lineno(),1, scope);} declaration_list R_BRACKET {new_name = "";if (asc != NULL) fprintf(asc, " ]\n");}
;

declaration_list: declaration SEMI_COLON {add_to_type(new_name, variable); if (asc != NULL) fprintf(asc, "; ");} declaration_list
| declaration {add_to_type(new_name, variable);}
;

declaration: typeID {variable = $1;} COLON {if (asc != NULL) fprintf(asc, "%s : ", $1);} identifier_list 
|  ID COLON {if (asc != NULL) fprintf(asc, "%s : ", $1); }identifier_list { if (exists($1)) {throw_dec_error_1(lineno(), columnno(), $1, asc); }}
;

identifier_list: identifier identifier_list_option { insert_symbol($1, scope, variable, "local");}
;

identifier_list_option:
|   COMMA {if (asc != NULL) fprintf(asc, ", ");} identifier_list 
|   ASSIGN constant {if (asc != NULL) fprintf(asc, ":= ");} comma_option
;

comma_option:
|   COMMA {if (asc != NULL) fprintf(asc, ", ");} identifier_list
;

statement_list: statement statement_list
|   statement
;

statement: FOR L_PARANTHESIS {if (asc != NULL) fprintf(asc, "for (");} statement SEMI_COLON {if (asc != NULL) fprintf(asc, "; ");} expression SEMI_COLON {if (asc != NULL) fprintf(asc, "; ");} statement R_PARANTHESIS {if (asc != NULL) fprintf(asc, ")");} sblock
|   WHILE L_PARANTHESIS {if (asc != NULL) fprintf(asc, "while (");} expression R_PARANTHESIS {if (asc != NULL) fprintf(asc, ")");}
|   IF L_PARANTHESIS {if (asc != NULL) fprintf(asc, "%i:%i:%i: if (", lineno(), columnno(), scope);} expression R_PARANTHESIS THEN {if (asc != NULL) fprintf(asc, ") then");} sblock ELSE {if (asc != NULL) fprintf(asc, "%i:%i:%i: else", lineno(), columnno(), scope);} sblock
|   SWITCH L_PARANTHESIS {if (asc != NULL) fprintf(asc, "switch (");} expression R_PARANTHESIS {if (asc != NULL) fprintf(asc, ")");}case_singular OTHERWISE COLON {if (asc != NULL) fprintf(asc, "otherwise : ");}sblock
|   sblock
|   {if (asc != NULL) fprintf(asc, "%i:%i:%i: ", lineno(), 1, scope);} assignable ASSIGN {if (asc != NULL) fprintf(asc, " := ");}expression SEMI_COLON {if (asc != NULL) fprintf(asc, ";\n");}
|   memop assignable SEMI_COLON {if (asc != NULL) fprintf(asc, ";\n");}
;

case_singular: CASE {if (asc != NULL) fprintf(asc, "case");} constant COLON {if (asc != NULL) fprintf(asc, ": ");} sblock case_plural
;

case_plural:
|   case_singular
;

assignable: identifier 
|   assignable ablock
|   assignable DOT {if (asc != NULL) fprintf(asc, ".");} identifier 
;

expression: SUB_OR_NEG {if (asc != NULL) fprintf(asc, "-");} expression
|   NOT {if (asc != NULL) fprintf(asc, "!");} expression
|   INT2REAL {if (asc != NULL) fprintf(asc, " i2r ");} expression
|   REAL2INT {if (asc != NULL) fprintf(asc, " r2i ");} expression
|   expression IS_NULL {if (asc != NULL) fprintf(asc, " isNull ");}
|   assignable
|   expression ADD {if (asc != NULL) fprintf(asc, "+");} expression
|   expression SUB_OR_NEG {if (asc != NULL) fprintf(asc, " - ");} expression
|   expression MUL {if (asc != NULL) fprintf(asc, " * ");} expression
|   expression DIV {if (asc != NULL) fprintf(asc, " / ");} expression
|   expression REM {if (asc != NULL) fprintf(asc, " %% ");} expression
|   expression AND {if (asc != NULL) fprintf(asc, " & ");} expression
|   expression OR {if (asc != NULL) fprintf(asc, " | ");} expression
|   expression LESS_THAN {if (asc != NULL) fprintf(asc, " < ");} expression
|   expression EQUAL_TO {if (asc != NULL) fprintf(asc, " = ");} expression  
|   L_PARANTHESIS {if (asc != NULL) fprintf(asc, "(");} expression R_PARANTHESIS {if (asc != NULL) fprintf(asc, ")");}
|   C_INTEGER {if (asc != NULL) fprintf(asc, " %i ", $1);}
|   C_REAL {if (asc != NULL) fprintf(asc, " %f", $1);}
|   C_TRUE {if (asc != NULL) fprintf(asc, " true ");}
|   C_FALSE {if (asc != NULL) fprintf(asc, " false ");}
|   C_CHARACTER {if (asc != NULL) fprintf(asc, " %c ", $1);}
|   C_STRING {if (asc != NULL) fprintf(asc, " %s ", $1);}
;

pblock: L_PARANTHESIS {if (asc != NULL) fprintf(asc, "(");} parameter_list R_PARANTHESIS {type = $3; if (asc != NULL) fprintf(asc, ")");}
;

parameter_list:
|   non_empty_parameter_list
;

non_empty_parameter_list:   parameter_declaration non_empty_parameter_list_option1
;

non_empty_parameter_list_option1:
|   COMMA {if (asc != NULL) fprintf(asc, ", ");} non_empty_parameter_list
;

parameter_declaration: typeID {if (asc != NULL) fprintf(asc, " %s", $1);} COLON {if (asc != NULL) fprintf(asc, " : ");} identifier {insert_symbol($5,1,$1,"parameter");}
| 	ID COLON {if (asc != NULL) fprintf(asc, ": ");} identifier {insert_symbol($4, 1, $1, "parameter");}
;

ablock: L_PARANTHESIS {if (asc != NULL) fprintf(asc, "(");} arguement_list R_PARANTHESIS {if (asc != NULL) fprintf(asc, ")");}
;

arguement_list:
|   non_empty_arguement_list
;

non_empty_arguement_list:   expression
|   expression COMMA {if (asc != NULL) fprintf(asc, ", ");} non_empty_arguement_list
;

memop:  RESERVE {if (asc != NULL) fprintf(asc, "reserve ");}
|   RELEASE	{if (asc != NULL) fprintf(asc, "release ");}
;

identifier: ID {if (asc != NULL) fprintf(asc, "%s", $1);}
|   typeID {if (asc != NULL) fprintf(asc, " %s ", $1);}
;

constant:   identifier {if (asc != NULL) fprintf(asc, " %s ", $1);}
| C_INTEGER {if (asc != NULL) fprintf(asc, "%i", $1);}
| C_REAL	{if (asc != NULL) fprintf(asc, "%f", $1);}
| C_TRUE	{if (asc != NULL) fprintf(asc, "true");}
| C_FALSE	{if (asc != NULL) fprintf(asc, "false");}
| C_CHARACTER	{if (asc != NULL) fprintf(asc, "%c", $1);}
| C_STRING	{if (asc != NULL) fprintf(asc, "%s", $1);}
;

typeID: T_INTEGER 
| T_REAL 	
| T_BOOLEAN	
| T_CHARACTER	
| T_STRING	
;


%%
/* Additional C code */
#include "lex.yy.c"

int lineno() {
    return n_lines;
}

int columnno() {
    return n_cols;
}

void yyerror(char* p){
    if (asc != NULL) fprintf(asc, "\nLINE %i:%i ** ERROR: %s\n", lineno(),columnno() , p);
}
void open_file(){
	asc = fopen("prog.asc", "w");
}
