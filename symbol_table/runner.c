#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "symbol_table.h" 

extern void yyparse();
extern void yyset_in(FILE *);
extern void print_list();
extern void open_file();
FILE *inputFile;

int main(int argc, char *argv[]){
    if (argc == 1){
        printf("No program to compile\n");
        return 1;
    }
    else{
	if (strcmp(argv[1], "-st") == 0){
		const char* program = argv[2];
		inputFile = fopen(program, "r");
		yyset_in(inputFile);
		yyparse();
		write_tofile();
	}
	else if (strcmp(argv[1], "-asc") == 0){
		const char* program = argv[2];
		inputFile = fopen(program, "r");
		yyset_in(inputFile);
		open_file();
		yyparse();
	}
	
	else{
	        const char* program = argv[1];
	        inputFile = fopen(program, "r");
	        yyset_in(inputFile);
	        yyparse();
	}
        
    }
	return 0;
}
