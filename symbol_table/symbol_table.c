#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "symbol_table.h"

struct Type{
	char *type;
	struct Type *next;
};

struct Node{
	char *name;
	int scope;
	int type_size;
	struct Type *type;
	char *extra; //extra information 
	struct Node* next;
	int asize;
};

struct Node* head;
struct Node* symbol;
int size = 0;

void init(char *n, int s, char *t, char *e){
	struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));
	new_node->name = n;
	new_node->scope = s;
	new_node->type_size = 1;
	new_node->asize = 0;

	struct Type* t_type = (struct Type*) malloc(sizeof(struct Type));
	t_type->type = t;
	t_type->next = NULL;
	new_node->type = t_type;
	new_node->extra = e;

	head = (struct Node*) malloc(sizeof(struct Node));
	head = new_node;
}

void insert_symbol(char *n, int s, char *t, char *e){//inserts node at head only for now
	if (size == 0){
		init(n, s, t, e);
		size++;
	}
	else{
		struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));
		new_node->name  = n;
		new_node->scope = s;
		new_node->type_size = 1;
		new_node->asize = 0;

		struct Type* t_type = (struct Type*) malloc(sizeof(struct Type));
		t_type->type = t;
		t_type->next = NULL;
		new_node->type = t_type;
		new_node ->extra = e;		
		new_node->next = head;
		head = new_node;
		size++;
	}
}

struct Node* find_node(char *n){
	struct Node *node = head;
	while (node != NULL){
		if (node->name == n) return node;
		else node = node->next;
	}
	//printf("\nNODE NOT FOUND....\n");
	return NULL;
}

struct Type* reverse_type(struct Type* head){

	struct Type* prev   = NULL;
	struct Type* current = head;
	struct Type* next;

	while (current != NULL){
		next  = current->next;  
		current->next = prev;   
		prev = current;
		current = next;
	}						        
	return prev;
}

struct Node* reverse_node(){
	struct Node* prev = NULL;
	struct Node* current = head;
	struct Node* next;

	while(current != NULL){
		next = current->next;
		current->next = prev;
		prev = current;
		current = next;
	}
	return prev;
}

bool exists(char *name){
	if(find_node(name) == NULL) {
		printf("\nnode found\n");
		return true;
		
	//printf("\nnode not found\n");
	return false;
	}
}

bool type_exists(char *type, bool function){
	if (strcmp(type, "integer")== 0) return true;
	else if(strcmp(type, "string") == 0) return true;
	else if(strcmp(type, "Boolean") == 0) return true;
	else if(strcmp(type, "real") == 0) return true;
	else if(strcmp(type, "character") == 0) return true;
	else if (function){
		struct Node* temp = head;
		while(temp != NULL){
			if ((strcmp(temp->extra, "ftype") == 0) && (strcmp(temp->name, type) == 0)) return true;
			else temp = temp->next;	
		}	
	}
	else return false;
}

bool is_function(char *name){

	if (find_node(name) != NULL){
		if (strcmp(find_node(name)->extra, "function") == 0) return true;
		else return false;
	}
	return false;
}

void change_name(char *old_name, char *new_name){
	if (find_node(old_name) == NULL) return;//printf("\nNODE NOT IN LIST\n");
	else find_node(old_name)->name = new_name;
}

void change_scope(char *name){
	if (find_node(name) != NULL) find_node(name)->scope++;
	//else printf("\nNODE NOT FOUND\n");
}

void change_type(char *name, char *new_type){
	if (find_node(name) != NULL) find_node(name)->type->type = new_type;
	//else printf("\nNODE NOT FOUND\n");
}

void change_extra(char *name, char *new_extras){
	if (find_node(name) != NULL) find_node(name)->extra = new_extras;
	//else printf("\nNODE NOT FOUND\n");
}

void add_array_size(char *name, int size){
	if (find_node(name) != NULL) find_node(name)->asize = size;
}

int get_array_size(char *name){
	if(find_node(name) != NULL) return find_node(name)->asize;
}

void make_symbol(char *n, int s, char *t, char *e){
	symbol = (struct Node*) malloc(sizeof(struct Node));
	symbol->name = n;
	symbol->scope = s;
	symbol->type->type = t;
	symbol->extra = e;
	symbol->next = NULL;
}

void write_tofile(){
	FILE *stable;
       	stable = fopen("prog.st", "w");
	fprintf(stable, "NAME\t\t:SCOPE :\tTYPE\t\t:EXTRA NOTATION\n\n");
	//fclose(stable);

	struct Node *node = reverse_node(head);
	while (node != NULL){

		fprintf(stable, "%s\t\t: %i : ", node->name, node->scope);
		
		if (strcmp(node->extra, "rtype") == 0){
			fprintf(stable, "[");
			struct Type* temp = reverse_type(node->type)->next;
			while(temp != NULL) {
				if (temp->next == NULL) fprintf(stable, "%s", temp->type);
				else fprintf(stable, "%s, ", temp->type);
				temp = temp->next;
			}
			fprintf(stable, "]");
		}
		else if(strcmp(node->extra, "atype") == 0){
			fprintf(stable, "(");
			for(int i = 0; i < get_array_size(node->name); i++){
				if(i + 1 == get_array_size(node->name)) {
					fprintf(stable, "%s) -> %s", node->type->type, node->type->type);
				}
				else fprintf(stable, "%s, ", node->type->type);
			}	
		}
		else if (strcmp(node->extra, "ftype") == 0){
			fprintf(stable, "(%s) -> %s", node->type->type, node->type->type);
		
		}
		else fprintf(stable, "%s", node->type->type);
		fprintf(stable, "\t\t: %s\n",node->extra);
		node = node->next;
	}
}

void add_to_type(char *name, char *new_type){
	if (find_node(name) != NULL) {
		struct Type *temp = find_node(name)->type;
		while(temp->next != NULL) temp = temp->next;
		
		struct Type* new_struct = (struct Type*) malloc(sizeof(struct Type));
		new_struct->type = new_type;
		new_struct->next = find_node(name)->type;                
	        find_node(name)->type = new_struct;
		find_node(name)->type_size++;
	}
}

	//else printf("\nCOULD NOT ADD TYPE...\n"); //to be used for testing purposes
