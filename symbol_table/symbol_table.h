#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

#include <stdbool.h>

//GLOBAL VARIABLES
struct Node;
extern struct Node *head; // actual symbol table
extern int size; // tracks size of table

//FUNCTIONS WHICH ADD TO LIST
void init(char *, int, char *, char *); // initialization of list, handled locally
void insert_symbol(char *, int, char *, char *); // insert symbol into list
void make_symbol(char *, int, char *, char *); // make a new symbol, using global variable

//ACCESS FUNCTIONS FOR TABLE
struct Node* find_node(char *); // find node in list
bool exists(char *); // checks if symbol exists
bool type_exists(char *, bool); // checks if type exists or has been declared for function
bool is_function(char *); // checks if node is a function
int get_array_size(char *); // returns size of an array

//MUTATOR FUNCTIONS FOR TABLE
void change_type(char *, char *); 
void change_name(char *, char *); // change name of node in the list
void change_scope(char *); // increments scope of symbol
void change_extra(char *, char *); // updates extra
void add_array_size(char *, int); // writes an array size
void add_to_type(char *, char *); // used to set type to multiple types

// FUNCTION FOR "-st" OPTION
void write_tofile(); // writes symbol table to file

#endif
