#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "symbol_table.h"

struct Type{
	char *type;
	struct Type *next;
	char *var_name;
	int pos;
    int pos_byte;
	
    //DARASY
    int widthFunction;
    int var_name_size;
};

struct Node{
	char *name;
	int scope;
	int type_size;
	struct Type *type;
	char *return_type;
	char *extra; //extra information 
	struct Node* next;
	int asize;
	
	// DARASY
	int addr; 
    int stringL;
    int arrayList[99]; 
    char parameter[9][99][99];
    int parameterSize;
    int width;
    char val[99];         // <-------------------- 05/06/19 to store the initial value of array
};

struct TVariable {
    char *name;
    int wid;
    int arrayList[99]; // <------------------------------ I ADD 5/5/2018
    struct TVariable *next;
};

struct TVariable* tHead;
int tSize = 0;

struct Node* head;
struct Node* symbol;
int size = 0;

// Darasy
int offset = 0;             // Offset for the location of memory, use after each width assignment

int tIndexNum= 0;
int topSize = 0;
int labelIndex = 0;
int labelIndexElse = 0;
char top[999][2000];
char tab_or_label[10] = "    ";
int elseIndex[999];
int elseIS = 0;
int nextIndex[999];
int nextIS = 0;
int elseJump = 0;

int indexOfV[99];
int switchConditionInLevel[99];
int afterSwitchConditionInLevel[99];
int switchLevel = -1;
char switchConditionVariableInLevel[99][2000];
char valueInEachCase[99][99][99];
char labelOfCasesInLevel[99][99][99];

char storeDeclareArrayElement[999][999][999];
int storeDeclareArrayElementSize;

char forLoopLevel[99][99];
int forLoopLevelSize;
char exitForLoopLevel[99][99];
int exitForLoopLevelSize;

char functionName[99];
char topIF[99][99][99];     // May 03, 2018
int topIFSize[99];

FILE *ir = NULL;

// Timothy
int noOfWhile = 0;
int whileCount[99];
int whileLevel = -1;
int whileCount2[99];
int whileLevel2 = -1;

void init(char *n, int s, char *t, char *e){
	struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));
	new_node->name = n;
	new_node->scope = s;
	new_node->type_size = 1;
	new_node->asize = 0;

	struct Type* t_type = (struct Type*) malloc(sizeof(struct Type));
	t_type->type = t;
	t_type->next = NULL;
	t_type->var_name = "";
	t_type->pos = 0;
    t_type->pos_byte = 0;       // <------------------- 04/18/19
	new_node->type = t_type;
	new_node->return_type = NULL;
	new_node->extra = e;

	head = (struct Node*) malloc(sizeof(struct Node));
	head = new_node;
}

void insert_symbol(char *n, int s, char *t, char *e){//inserts node at head only for now
	if (size == 0){
		init(n, s, t, e);
		size++;
	}
	else{
		struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));
		new_node->name  = n;
		new_node->scope = s;
		new_node->type_size = 1;
		new_node->asize = 0;

		struct Type* t_type = (struct Type*) malloc(sizeof(struct Type));
		t_type->type = t;
		t_type->next = NULL;
		t_type->var_name = "";
		t_type->pos = 0;
        t_type->pos_byte = 0;       // <------------------- 04/18/19
		new_node->type = t_type;
		new_node ->extra = e;		
		new_node->next = head;
		head = new_node;
		size++;
	}
}

struct Node* find_node(char *n){
	struct Node *node = head;
	while (node != NULL){
		if (strcmp(node->name, n) == 0) return node;
		else node = node->next;
	}
	//printf("\nNODE NOT FOUND....\n");
	return NULL;
}

struct Type* reverse_type(struct Type* head){

	struct Type* prev   = NULL;
	struct Type* current = head;
	struct Type* next;

	while (current != NULL){
		next  = current->next;  
		current->next = prev;   
		prev = current;
		current = next;
	}						        
	return prev;
}

struct Node* reverse_node(){
	struct Node* prev = NULL;
	struct Node* current = head;
	struct Node* next;

	while(current != NULL){
		next = current->next;
		current->next = prev;
		prev = current;
		current = next;
	}
	return prev;
}
 
void list_var_names(char *name){
	if (find_node(name) != NULL){
		struct Type* node_type = find_node(name)->type;
		while (node_type != NULL){
			printf("\nname: %s at position %i\n", node_type->var_name, node_type->pos);
			node_type = node_type->next;
		}	
	}
}

int get_pos(char *node_name, char *var_name){
    if (find_node(node_name) != NULL) {
        struct Type* node_type = find_node(node_name)->type;
        while(node_type != NULL){
            if (strcmp(node_type->var_name, var_name) == 0) return node_type->pos;
            node_type = node_type->next;
        }
    }
    return -1;
}

int get_pos_byte(char *node_name, char *var_name){                      // <------------------------------- 04/18/19
	if (find_node(node_name) != NULL) {                                 // <------------------------------- 04/18/19
		struct Node* node = find_node(node_name)->type->type;           // <------------------------------- 04/18/19
        struct Type* node_type = find_node(node)->type;                 // <------------------------------- 04/18/19
		while(node_type != NULL){                                       // <------------------------------- 04/18/19
			if (strcmp(node_type->var_name, var_name) == 0) return node_type->pos_byte;  // <------------------------------- 04/18/19
			node_type = node_type->next;	    // <------------------------------- 04/18/19
		}   // <------------------------------- 04/18/19
	}// <------------------------------- 04/18/19
	return -1;// <------------------------------- 04/18/19
}// <------------------------------- 04/18/19

bool exists(char *name){
	
	if(find_node(name) != NULL) return true;
		
	return false;
	
}

bool type_exists(char *type){
	if (strcmp(type, "integer")== 0) return true;
	else if(strcmp(type, "string") == 0) return true;
	else if(strcmp(type, "Boolean") == 0) return true;
	else if(strcmp(type, "real") == 0) return true;
	else if(strcmp(type, "character") == 0) return true;
	else{
		struct Node* temp = head;
		while (temp != NULL){
			if (strcmp(temp->name, type) == 0 && (strcmp(temp->extra, "atype") == 0)) return true;
			else if (strcmp(temp->name, type) == 0 && (strcmp(temp->extra, "rtype") == 0)) return true;
			else temp = temp->next;
		}	
	}
	return false;
}

bool is_ftype(char *name){

	struct Node* temp = head;
	while(temp != NULL){
		if (strcmp(temp->name, name) == 0 &&  (strcmp(temp->extra, "ftype") == 0)) return true;
		else temp = temp->next;
	}
	return false;
}

bool is_atype(char *name){
	if (find_node(name) != NULL) {
		if (strcmp(find_node(find_node(name)->type->type)->extra, "atype") == 0) return true;
	
	}
        return false;
}

bool is_rtype(char *name){
	
	
	struct Node* temp = head;
	while (temp != NULL) {
		if (strcmp(temp->name, name) == 0) break;
		else temp = temp->next;
	}
	struct Node *temp2 = head;
	while (temp2 != NULL) {
		if (strcmp(temp2->name, temp->type->type) == 0 && strcmp(temp2->extra, "rtype") == 0) return true;
		temp2 = temp2->next;
	
	}
	
	
	return false;
}

bool is_function(char *name){

	if (find_node(name) != NULL){
		if (strcmp(find_node(name)->extra, "function") == 0) return true;
		else return false;
	}
	return false;
}

void change_name(char *old_name, char *new_name){
	if (find_node(old_name) == NULL) return;//printf("\nNODE NOT IN LIST\n");
	else find_node(old_name)->name = new_name;
}

void change_scope(char *name){
	if (find_node(name) != NULL) find_node(name)->scope++;
	//else printf("\nNODE NOT FOUND\n");
}

void change_type(char *name, char *new_type){
	if (find_node(name) != NULL) find_node(name)->type->type = new_type;
	//else printf("\nNODE NOT FOUND\n");
}

void change_extra(char *name, char *new_extras){
	if (find_node(name) != NULL) find_node(name)->extra = new_extras;
	//else printf("\nNODE NOT FOUND\n");
}

void change_return_type(char *name, char *new_type){
	if (find_node(name) != NULL) find_node(name)->return_type = new_type;
}

void add_array_size(char *name, int size){
	if (find_node(name) != NULL) find_node(name)->asize = size;
}

int get_array_size(char *name){
	if(find_node(name) != NULL) return find_node(name)->asize;
	//else return find_node(name)->asize;;
}

char * get_type(char *name){
	struct Node* temp = head;
	while(temp != NULL){
		if (strcmp(temp->name, name) == 0 ) return temp->type->type;
		else temp = temp->next;
	}
	return NULL;
}

char * get_return_type(char *name){
	struct Node* temp = head;
	while(temp != NULL){
		if (strcmp(temp->name, name) == 0) return temp->return_type;
		else temp = temp->next;
	}
	return NULL;
}

char *get_extra(char *name){
	struct Node* temp = head;
	while (temp != NULL) {
		if (strcmp(temp->name, name) == 0) return temp->extra;
		else temp = temp->next;
	}
	return NULL;
}

void make_symbol(char *n, int s, char *t, char *e){
	symbol = (struct Node*) malloc(sizeof(struct Node));
	symbol->name = n;
	symbol->scope = s;
	symbol->type->type = t;
	symbol->extra = e;
	symbol->next = NULL;
}

void write_tofile(){
	FILE *stable;
       	stable = fopen("prog.st", "w");
	fprintf(stable, "NAME\t\t:SCOPE :\tTYPE\t\t:EXTRA NOTATION\n\n");
	//fclose(stable);

	struct Node *node = reverse_node(head);
	while (node != NULL){

		fprintf(stable, "%s\t\t: %i : ", node->name, node->scope);
		
		if (strcmp(node->extra, "rtype") == 0){
			fprintf(stable, "[");
			struct Type* temp = reverse_type(node->type)->next;
			while(temp != NULL) {
				if (temp->next == NULL) fprintf(stable, "%s", temp->type);
				else fprintf(stable, "%s, ", temp->type);
				temp = temp->next;
			}
			fprintf(stable, "]");
		}
		else if(strcmp(node->extra, "atype") == 0){
			fprintf(stable, "(");
			int size = node->asize;
			for(int i = 0; i < size; i++){
				if(i + 1 == size) {
					fprintf(stable, "%s) -> %s", node->type->type, node->type->type);
				}
				else fprintf(stable, "%s, ", node->type->type);
			}	
		}
		else if (strcmp(node->extra, "ftype") == 0){
			fprintf(stable, "(");
			struct Type* temp = reverse_type(node->type)->next;
			while(temp != NULL) {
				if (temp->next == NULL) fprintf(stable, "%s", temp->type);
				else fprintf(stable, "%s, ", temp->type);
				temp = temp->next;
			}
			fprintf(stable, ") -> %s", node->return_type);
		
		}
		else fprintf(stable, "%s", node->type->type);
		fprintf(stable, "\t\t: %s\n",node->extra);
		node = node->next;
	}
}

void add_to_type(char *name, char *new_type, char*var_name){
	if (find_node(name) != NULL) {
		struct Type *temp = find_node(name)->type;
		while(temp->next != NULL) temp = temp->next;
		
		struct Type* new_struct = (struct Type*) malloc(sizeof(struct Type));
		new_struct->type = new_type;
		new_struct->var_name = var_name;
        new_struct->pos = find_node(name)->type->pos + 1;
		new_struct->next = find_node(name)->type;
        
        if (find_node2(name)->extra == "rtype"){                        // <-------------------- 04/18/19
            new_struct->pos_byte = find_node(name)->width;                   // <-------------------- 04/18/19
            new_struct->var_name_size = assign_width_rtype(new_type);   // <-------------------- 04/18/19
            find_node(name)->width += new_struct->var_name_size;        // <-------------------- 04/18/19
        }                                                               // <-------------------- 04/18/19
        find_node(name)->type = new_struct;
        find_node(name)->type_size++;
	}
}

bool correct_function_call(char **args, char *name){
	struct Node *node = find_node(name);
	return true;

}

// DARASY

void gen() {
    
        char tIndex[17];
        sprintf(tIndex, "%d", tIndexNum);
        tIndexNum++;
        char new_t[2000];
        strcpy(new_t,"t");
        strcat(new_t,tIndex);
    
        if (existInArray("endOf2Statement")==1 && existInArray("endOfSecondStatement")==0) {    // Use to avoid operating FOR-statement 2 (i = i+1)
            int t9 = 0;
        }
    
        else if (existInArray("    goto Main\n") == 1) {
            if (ir!=NULL) { fprintf(ir, "%s",top[0]); }
            tIndexNum = 0;
            memset(top, 0, sizeof top);
            topSize = 0;
        }
    
        else if (existInArray("Main") == 1) {
            if (ir!=NULL) { fprintf(ir, "Main:");}
            strcpy(tab_or_label, " ");
            memset(top, 0, sizeof top);
            topSize = 0;
        }
    
        else if (existInArray("exit") == 1) {
            if (ir!=NULL) { fprintf(ir, "    exit");}
            memset(top, 0, sizeof top);
            topSize = 0;
        }
    
        else if (existInArray("function")==1) {
            
            if (ir!=NULL) { fprintf(ir, "%s:", top[1]); }
            strcpy(tab_or_label, " ");
            strcpy(functionName, top[1]);

            memset(top, 0, sizeof top);
            topSize = 0;
            
        }
    
        else if (existInArray("finishDefineFunction")==1) {
            strcpy(functionName, "");
            memset(top, 0, sizeof top);
            topSize = 0;
        }
    
        else if ( (existInArray("reserve")==0) && (existInArray("(")==1) && (existInArray(")")==1)) {

            int indexOfArrayOrFunction = 0;
            while (strcmp(top[indexOfArrayOrFunction], "(") != 0) {
                indexOfArrayOrFunction++;
            }
            indexOfArrayOrFunction = indexOfArrayOrFunction - 1;
            char *variableType = find_node2(top[indexOfArrayOrFunction])->type->type;
            
            if (strcmp(find_node2(variableType)->extra, "atype")==0) {
            
                char iList[99][99];
                int i = topSize - 2;
                int j = 0;
                int ln = 0;
                char old_t[9];
                while (strcmp(top[i], "(") != 0) {
                    strcpy(iList[j], top[i]);
                    j++;
                    ln++;
                    i--;
                }
                
                j--;
                char iList2[99][99];
                int indexIList = 0;
                while (j > -1) {
                    strcpy(iList2[indexIList], iList[j]);
                    j--;
                    indexIList++;
                }
                indexIList--;
                j++;
                
                char arrayName[99];
                strcpy(arrayName, top[i-1]);
                int addrCal = 0;
                
                if (indexIList == 0) { // 1 Dimension Array when index 'indexIList' = 0
                    strcpy(new_t, iList2[0]);
                } else {    // K >= 2 Dimension Array
                    tIndexNum++;
                    sprintf(tIndex, "%d", tIndexNum);
                    strcpy(new_t,"t");
                    strcat(new_t,tIndex);
                    if (ir!=NULL) { fprintf(ir, "%s%s = %s * %d\n", tab_or_label, new_t, iList2[0], find_node2(arrayName)->arrayList[1]); }
                    strcpy(old_t, new_t);
                    tIndexNum++;
                    sprintf(tIndex, "%d", tIndexNum);
                    strcpy(new_t,"t");
                    strcat(new_t,tIndex);
                    if (ir!=NULL) { fprintf(ir, "%s%s = %s + %s\n", tab_or_label, new_t, old_t, iList2[1]); }
                    j = 2;
                    while (indexIList >= j) {
                        strcpy(old_t, new_t);
                        tIndexNum++;
                        sprintf(tIndex, "%d", tIndexNum);
                        strcpy(new_t,"t");
                        strcat(new_t,tIndex);
                        if (ir!=NULL) { fprintf(ir, "%s%s = %s * %d\n", tab_or_label, new_t, old_t, find_node2(arrayName)->arrayList[j]); }
                        strcpy(old_t, new_t);
                        tIndexNum++;
                        sprintf(tIndex, "%d", tIndexNum);
                        strcpy(new_t,"t");
                        strcat(new_t,tIndex);
                        if (ir!=NULL) { fprintf(ir, "%s%s = %s + %d\n", tab_or_label, new_t, old_t, iList2[j]); }
                        j++;
                    }
                }

                char temp[99];
                strcpy(temp, find_node2(arrayName)->type->type);
                
                strcpy(old_t, new_t);
                tIndexNum++;
                sprintf(tIndex, "%d", tIndexNum);
                strcpy(new_t,"t");
                strcat(new_t,tIndex);
                
                if (ir!=NULL) { fprintf(ir, "%s%s = %s * %d\n", tab_or_label, new_t, old_t, (find_node2(temp)->width/find_node2(temp)->asize)); }

                strcpy(old_t, new_t);
                tIndexNum++;
                sprintf(tIndex, "%d", tIndexNum);
                strcpy(new_t,"t");
                strcat(new_t,tIndex);
                
                topSize = i-1;
                if (ir!=NULL) { fprintf(ir, "%s%s = %s + %d\n", tab_or_label, new_t, old_t, find_node2(temp)->width); }
                strcpy(top[topSize], arrayName);
                strcat(top[topSize], "[");
                strcat(top[topSize], new_t);
                strcat(top[topSize], "]");
                strcpy(tab_or_label, "    ");
                topSize++;
                tIndexNum++;
            } else if (strcmp(find_node2(variableType)->extra, "ftype")==0) {
                char iList[99][99];
                int i = topSize-2;
                int j = 0;
                while (strcmp(top[i], "(") != 0) {
                    strcpy(iList[j], top[i]);
                    j++;
                    i--;
                }
                int q = j;
                j--;
                while (j >= 0) {
                    if (ir!=NULL) { fprintf(ir, "%sparam %s\n", tab_or_label, iList[j]); }
                    strcpy(tab_or_label, "    ");
                    j--;
                }
                char ttIndex[17];
                sprintf(ttIndex, "%d", q);
                strcpy(top[topSize], "call");
                strcat(top[topSize], "(");
                strcat(top[topSize], top[i-1]);
                strcat(top[topSize], ",");
                strcat(top[topSize], ttIndex);
                strcat(top[topSize], ")");
                strcpy(tab_or_label, "    ");

                if (ir!=NULL) { fprintf(ir, "%s%s = %s\n",tab_or_label, new_t, top[topSize]); }
                int w;
                if (find_node2(top[i-1])!=NULL) {
                    if (find_node2(top[i-1])->type->widthFunction != 0) {
                        w = find_node2(top[i-1])->type->widthFunction;
                    }
                }
                topSize = i;
                char temp3[99];
                strcpy(temp3, top[i-1]);
                strcpy(top[topSize-1], new_t);
                insert_t_variabel(top[topSize-1], w);
                int ii = 0;
                while (find_node2(temp3)->arrayList[ii] != 0) {
                    find_t_variabel(top[topSize-1])->arrayList[ii] = find_node2(temp3)->arrayList[ii];
                    ii++;
                }
            }
        }
        
        else if ((isdigit(top[topSize-3][0])==1 || isalpha(top[topSize-3][0])==1) && ((top[topSize-2][0] == '-') || (top[topSize-2][0] == '+')
                                                                                 || (top[topSize-2][0] == '*') || (top[topSize-2][0] == '/') || (top[topSize-2][0] == '%'))) {
            if (ir!=NULL) { fprintf(ir, "%s%s = %s %s %s\n", tab_or_label, new_t, top[topSize-3], top[topSize-2], top[topSize-1]); }
            strcpy(top[topSize-3], new_t);
            topSize -= 2;
            strcpy(tab_or_label, "    ");
        }
        else if (strcmp(top[topSize-2], "!") == 0 && isalpha(top[topSize-1][0])==1)  {
            if (ir!=NULL) { fprintf(ir, "%s%s = %s %s\n", tab_or_label, new_t, top[topSize-2], top[topSize-1]); }
            strcpy(top[topSize-2], new_t);
            topSize -= 1;
            strcpy(tab_or_label, "    ");
        }
        else if (strcmp(top[topSize-1], "== null") == 0 && isalpha(top[topSize-2][0])==1 && existInArray("if")==0) {
            if (ir!=NULL) { fprintf(ir, "%s%s = %s %s\n", tab_or_label, new_t, top[topSize-2], top[topSize-1]); }
            strcpy(top[topSize-2], new_t);
            topSize -= 1;
            strcpy(tab_or_label, "    ");
        }
        else if (top[topSize-2][0] == '-' || strcmp(top[topSize-2], "i2r")==0 || strcmp(top[topSize-2], "r2i")==0 ) {
            if (ir!=NULL) { fprintf(ir, "%s%s = %s %s\n", tab_or_label, new_t, top[topSize-2], top[topSize-1]); }
            strcpy(top[topSize-2], new_t);
            topSize -= 1;
            strcpy(tab_or_label, "    ");
        }
        else if ((isdigit(top[topSize-3][0])==1 || isalpha(top[topSize-3][0])==1) && (strcmp(top[topSize-2], "==") == 0 || strcmp(top[topSize-2], "<") == 0 ) &&
                 existInArray("if")==0) {
            if (ir!=NULL) { fprintf(ir, "%s%s = %s %s %s\n", tab_or_label, new_t, top[topSize-3], top[topSize-2], top[topSize-1]); }
            strcpy(top[topSize-3], new_t);
            topSize -= 2;
            strcpy(tab_or_label, "    ");
        }
        else if ((strcmp(top[topSize-2], "&")==0 || strcmp(top[topSize-2], "|")==0 ) && (isalpha(top[topSize-1][0])==1) && (isalpha(top[topSize-3][0])==1)) {
            if (ir!=NULL) { fprintf(ir, "%s%s = %s %s %s\n", tab_or_label, new_t, top[topSize-3], top[topSize-2], top[topSize-1]); }
            strcpy(top[topSize-3], new_t);
            topSize -= 2;
            strcpy(tab_or_label, "    ");
        }
        else if (strcmp(top[topSize-2], "=")==0 ) {                 // ASSIGN WIDTH AND ADDRESS, AND INCREMENT OFFSET cos of assignment

            if (find_node2(top[topSize-3]) != NULL) {
                if (top[topSize-1][0] == '"' ) {
                    assign_width(top[topSize-3], "", strlen(top[topSize-1]));
                } else if (find_t_variabel(top[topSize-1])!=NULL && find_t_variabel(top[topSize-1])->name[0] == 't') { // <------------------------------- 04/18/19
                    if (find_t_variabel(top[topSize-1])->wid!=0) {
                        find_node2(top[topSize-3])->width = find_t_variabel(top[topSize-1])->wid;
                        int ii = 0;
                        while (find_t_variabel(top[topSize-1])->arrayList[ii] != 0) {
                            find_node2(top[topSize-3])->arrayList[ii] = find_t_variabel(top[topSize-1])->arrayList[ii];
                            ii++;
                        }
                    }
                }
                else {
                    assign_width(top[topSize-3], "", 0);
                }
                find_node2(top[topSize-3])->addr = offset;
                offset = offset + find_node2(top[topSize-3])->width;
            } else {
                char* cExist = strchr(top[topSize-3], '(');
                if (cExist != NULL) {
                    int a = 0;
                    int bytePos = 0;
                    int ct = 0; // rec name initial index
                    int ct2 = 0; // index value initial index
                    char recName[99];
                    strcpy(recName, "");
                    
                    while (top[topSize-3][ct] != '[') {
                        recName[ct] = top[topSize-3][ct];
                        ct++;
                    }
                    ct++;
                    while (top[topSize-3][ct] != ']') {
                        a = top[topSize-3][ct];
                        a-= 48;
                        bytePos = (bytePos * 10) + a;
                        ct++;
                    }

                    if (find_node2(recName) != NULL) {
                        char *typeName = find_node(recName)->type->type;
                        if (find_node2(typeName) != NULL) {
                            if (strcmp(find_node2(typeName)->extra, "rtype")==0) {
                                int temp_width = 0;
                                struct Type* typeN = find_node2(typeName)->type;
                                while (typeN->pos_byte != bytePos) {
                                    temp_width += typeN->var_name_size;
                                    typeN = typeN->next;
                                }
                                if (top[topSize-1][0] == '"' ) {
                                    typeN->var_name_size = strlen(top[topSize-1]) + 2;
                                    temp_width += typeN->var_name_size;
                                    typeN = typeN->next;
                                    if (typeN != NULL) {
                                        while (typeN != NULL) {
                                            typeN->pos_byte += typeN->var_name_size;
                                            temp_width += typeN->var_name_size;
                                            typeN = typeN->next;
                                        }
                                        find_node2(recName)->width = temp_width;
                                        if (ir!=NULL) { fprintf(ir, "%s%s = malloc(%d)\n", tab_or_label, recName, find_node2(recName)->width); }
                                        find_node2(recName)->addr = offset;
                                        offset += find_node2(recName)->width;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (find_node2(top[topSize-3]) != NULL) {
                if (strcmp(find_node2(top[topSize-3])->extra, "function")==0) {
                    if (ir!=NULL) { fprintf(ir, "%sreturn %s\n", tab_or_label, top[topSize-1]); }
                }
                else {
                    if (ir!=NULL) { fprintf(ir, "%s%s %s %s\n", tab_or_label, top[topSize-3], top[topSize-2], top[topSize-1]); }
                }
            }
            else {
                if (ir!=NULL) { fprintf(ir, "%s%s %s %s\n", tab_or_label, top[topSize-3], top[topSize-2], top[topSize-1]); }
            }
            
            topSize = 0;
            tIndexNum-=1;
            strcpy(tab_or_label, "    ");
        }
        else if (existInArray("reserve") == 1) {                    // ASSIGN WIDTH AND ADDRESS, AND INCREMENT OFFSET cos of reserve
            
            char var1[20];
            strcpy(var1, top[1]);
            char aRrType[20];
            strcpy(aRrType,  find_node2(var1)->type->type);
            int arrSize;
            if (find_node2(aRrType)!=NULL) {
                if (strcmp(find_node2(aRrType)->extra, "atype")==0) {
                    int i = 0;
                    arrSize = 1;
                    if (existInArray("(")==1) {
                        while (strcmp(top[i+3],")") != 0) {
                            arrSize = arrSize * atoi(top[i+3]);
                            find_node(var1)->arrayList[i] = atoi(top[i+3]);
                            i++;
                        }
                        assign_width_atype(var1, arrSize);
                    } else {
                        assign_width_atype(var1, 1);
                    }
                    find_node2(var1)->addr = offset;
                    offset = offset + find_node2(var1)->width;
                }
                
                else if (strcmp(find_node2(aRrType)->extra, "rtype")==0)  {
                    if (get_type(var1) != NULL) {                               // <------------------- 04/18/19
                        char *temp123 = get_type(var1);                         // <------------------- 04/18/19
                        find_node2(var1)->width = find_node2(temp123)->width;   // <------------------- 04/18/19
                    } else { find_node2(var1)->width = 0; }                     // <------------------- 04/18/19
                    find_node2(var1)->addr = offset;                            // <------------------- 04/18/19
                    offset = offset + find_node2(var1)->width;                  // <------------------- 04/18/19
                }
                
                // type->widthFunction
                if (ir!=NULL) { fprintf(ir, "%s%s = malloc(%d)\n", tab_or_label, var1, find_node2(var1)->width);
                
                    // Generate number of elements in each dimension of the array
                    if (strcmp(find_node2(aRrType)->extra, "atype")==0) {
                        int i = 0;                   //  <----------------------------- 05/06/18
                        if (existInArray("(")==1) {  //  <----------------------------- 05/06/18
                            while (strcmp(top[i+3],")") != 0) {  //  <----------------------------- 05/06/18
                                if (ir!=NULL) { fprintf(ir, "    %s[%d] = %s\n", var1, 4*i, top[i+3]); }  //  <----------------------------- 05/06/18
                                i++;  //  <----------------------------- 05/06/18
                            }  //  <----------------------------- 05/06/18
                        }  //  <----------------------------- 05/06/18
                        char *temp4 = get_type(var1);
                        char tempVal[99];
                        strcpy(tempVal,find_node(temp4)->val);

                        // Generate initial values if exist
                        if (strlen(tempVal) != 0) {
                            int j = 0;
                            int j1 = 4*i;
                            while (j < arrSize) {
                                fprintf(ir, "    %s[%d] = %s\n", var1, j1, tempVal);
                                j1 += (find_node2(temp4)->width)/find_node2(temp4)->asize;
                                j++;
                            }
                        }
                    }
                }
                
                
                if (strcmp(functionName, "")!=0) {
                    find_node2(functionName)->type->widthFunction = find_node2(var1)->width;
                    int ii = 0;
                    while (find_node2(var1)->arrayList[ii] != 0) {
                        find_node2(functionName)->arrayList[ii] = find_node2(var1)->arrayList[ii];
                        ii++;
                    }
                    find_node2(var1)->width = 0;
                    offset = 0;
                    find_node2(var1)->addr = 0;
                }

                strcpy(tab_or_label, "    ");
            }
            
            else if (strcmp(aRrType, "string")==0) {
                int i = atoi(top[3]) + 2;
                assign_width(var1, "", i);
                find_node2(var1)->addr = offset;
                offset = offset + find_node2(var1)->width;
                if (ir!=NULL) { fprintf(ir, "%s%s = malloc(%d)\n", tab_or_label, var1, find_node2(var1)->width); }
                strcpy(tab_or_label, "    ");
            }
            
            memset(top, 0, sizeof top);
            topSize = 0;
        }
        else if (existInArray("release") == 1) {                    // RELEASE WIDTH AND ADDRESS, cos of release
            
            char var1[20];
            strcpy(var1, top[1]);
            char aRrType[20];
            strcpy(aRrType,  find_node2(var1)->type->type);
            
            if (find_node2(aRrType)!=NULL  || (strcmp(aRrType,"string")==0)) {
                if (ir!=NULL) { fprintf(ir, "%srelease(%s,%d)\n", tab_or_label, var1, find_node2(var1)->width); }

                find_node2(var1)->addr = 0;
                find_node2(var1)->width = 0;
                strcpy(tab_or_label, "    ");
            }
            
            memset(top, 0, sizeof top);
            topSize = 0;
        } else if (existInArray("period")==1) {
            int noNeedToConcat = 0;
            int m = topSize-1;
            int x = 0;
            int count = 0;
            int a = 0;
            int b = 0;
            
            bool arrAccess = false;
            
            char rule[2000];
            strcpy(rule,"");
            char tempRule[2000];
            strcpy(tempRule,"");
            
            char arrPos[2000];
            strcpy(arrPos, "");
            char recPos[17];
            strcpy(recPos, "");
            
            if ('_' == top[m][0]) {
                arrAccess = true;
                strncpy( tempRule, top[m]+1, strlen(top[m])-1);
            }
                    
            if (!arrAccess) {
                strcpy(tempRule,top[m]);
            }

            strcpy(rule, top[m-2]);
            
            if (!arrAccess) {
                x = get_pos_byte(rule, tempRule);
                
                sprintf(recPos, "%d", x);
                
                strcat(rule, "[");
                strcat(rule, recPos);
                strcat(rule, "]");
                topSize-=3;
                
                strcpy(top[topSize], rule);
                topSize++;
            }
            
            else {
                if (find_node(rule) != NULL) {
                    for (count = 0; tempRule[count] != NULL; count++) {
                        a = tempRule[count];
                        a-= 48;
                        b = (b * 10) + a;
                    }
                    
                    x = find_node(rule)->arrayList[b];
                    sprintf(arrPos, "%d", x);
                    topSize-=3;
                    
                    strcpy(top[topSize], arrPos);
                    topSize++;
                }
            }
        }
    
        else if (existInArray("then")==1) {
            
            int noNeedToConcat = 0;
            int m = 1;
            labelIndex++;
            elseJump = labelIndex;
            labelIndex++;
            int iLabel;
            iLabel = labelIndex;
            
            char lIndex[17];    // new label for THEN statement
            sprintf(lIndex, "%d", labelIndex);
            char new_l[2000];
            strcpy(new_l,"L");
            strcat(new_l,lIndex);
            
            char rule[2000];        // Rule concatenation
            strcpy(rule,"");
            
            strcpy(tab_or_label, "    ");  // Space to put Label
            
            while (m != topSize) {
                if (strcmp("|",top[m])==0){     // Check for OR condition
                    if (ir!=NULL) { fprintf(ir, "%sif %s goto %s\n", tab_or_label, rule, new_l); }

                    strcpy(tab_or_label, "    ");
                    strcpy(rule, "");
                    noNeedToConcat = 1;
                }
                else if (strcmp("&",top[m])==0){        // Check for AND condition
                    iLabel++;
                    char lIndex[17];    // new label
                    sprintf(lIndex, "%d", iLabel);
                    char new_li[2000];
                    strcpy(new_li,"L");
                    strcat(new_li,lIndex);
                    
                    char laIndex[17];    // new label
                    sprintf(laIndex, "%d", elseJump);
                    char new_lia[2000];
                    strcpy(new_lia,"L");
                    strcat(new_lia,laIndex);
                    
                    if (ir!=NULL) { fprintf(ir, "%sif %s goto %s\n", tab_or_label, rule, new_li); }
                    if (ir!=NULL) { fprintf(ir, "    goto %s\n", new_lia); }

                    labelIndex = iLabel;
                    strcpy(tab_or_label, new_li);
                    strcat(tab_or_label, ": ");
                    strcpy(rule, "");
                    noNeedToConcat = 1;
                }
                else if (strcmp("then",top[m])==0){     // End of IF condition
                    if (ir!=NULL) { fprintf(ir, "%sif %s goto %s\n", tab_or_label, rule, new_l); }

                    strcpy(rule, "");
                    
                    labelIndexElse = elseJump;
                    elseIndex[elseIS] = labelIndexElse;
                    char lIndex[17];    // new label for ELSE statement
                    sprintf(lIndex, "%d", labelIndexElse);
                    char new_li[2000];
                    strcpy(new_li,"L");
                    strcat(new_li,lIndex);
                    if (ir!=NULL) { fprintf(ir, "    goto %s\n", new_li); }

                    strcpy(tab_or_label, "");
                    strcat(tab_or_label, new_l);
                    strcat(tab_or_label, ": ");
                    
                    noNeedToConcat = 1;
                    memset(top, 0, sizeof top);
                    topSize = 0;
                    elseIS++;
                    break;
                }
                
                if (noNeedToConcat == 0) {
                    strcat(rule, " ");
                    strcat(rule,top[m]);
                }
                noNeedToConcat = 0;
                m+=1;
            }
        }
        else if (existInArray("else")==1) {
            
            labelIndex++;
            char laIndex[17];    // new label for jump to after THEN statement
            sprintf(laIndex, "%d", labelIndex);
            
            nextIndex[nextIS] = labelIndex;
            nextIS+=1;
            
            char new_l[2000];
            strcpy(new_l,"L");
            strcat(new_l,laIndex);
            if (ir!=NULL) { fprintf(ir, "    goto %s\n", new_l); }

            char lIndex[17];    // label of else
            elseIS=elseIS - 1;
            sprintf(lIndex, "%d", elseIndex[elseIS]);
            char new_li[2000];
            strcpy(new_li,"L");
            strcat(new_li,lIndex);
            strcpy(tab_or_label, "");
            strcat(tab_or_label, new_li);
            strcat(tab_or_label, ": ");
            memset(top, 0, sizeof top);
            topSize = 0;
        }
        else if (existInArray("endOfIf")==1) {
            
            char lIndex[17];    // label of else
            sprintf(lIndex, "%d", elseIndex[elseIS]);
            char new_li[2000];
            strcpy(new_li,"L");
            strcat(new_li,lIndex);
            strcpy(tab_or_label, "");
            strcat(tab_or_label, new_li);
            strcat(tab_or_label, ": ");
            
            char laIndex[17];    // new label for jump to after ELSE statement
            nextIS-=1;
            sprintf(laIndex, "%d", nextIndex[nextIS]);
            char new_l[2000];
            strcpy(new_l,"L");
            strcat(new_l,laIndex);
            if (ir!=NULL) { fprintf(ir, "    goto %s\n", new_l); }

            if (ir!=NULL) { fprintf(ir, "%s:\n", new_l); }

            strcpy(tab_or_label, "    ");
            memset(top, 0, sizeof top);
            topSize = 0;
        }
    
        else if (existInArray("endOf1Statement")==1) {
            labelIndex++;
            char lIndex[17];    // new label for start switch expression or "test" in the book
            sprintf(lIndex, "%d", labelIndex);
            char new_l[2000];
            strcpy(new_l,"L");
            strcat(new_l,lIndex);
            strcpy(tab_or_label, new_l);
            strcat(tab_or_label, ": ");
            labelIndex++;
            
            strcpy(forLoopLevel[forLoopLevelSize], new_l);
            forLoopLevelSize++;
            
            memset(top, 0, sizeof top);
            topSize = 0;
        }
    
        else if (existInArray("endOfFirstStatement")==1) {
            
            char temp[9][9];
            char laIndex[17];    // new label for start switch expression or "test" in the book
            sprintf(laIndex, "%d", labelIndex);
            char new_la[2000];
            strcpy(new_la,"L");
            strcat(new_la,laIndex);
            labelIndex++;
            
            strcpy(exitForLoopLevel[exitForLoopLevelSize], new_la);
            exitForLoopLevelSize++;
            
            if (ir!=NULL) { fprintf(ir, "%sifFalse %s goto %s\n", tab_or_label, top[0], new_la); }

            strcpy(tab_or_label, "    ");
            
            memset(top, 0, sizeof top);
            topSize = 0;
        }
    
        else if (existInArray("endOfSecondStatement") == 1) {
            
            int t9 = 0;
            while (t9 < topSize-1) {
                strcpy(topIF[forLoopLevelSize-1][t9], top[t9+1]);
                t9++;
            }
            topIFSize[forLoopLevelSize-1] = t9-1;
            
            memset(top, 0, sizeof top);
            topSize = 0;
        }
    
        else if (existInArray("endOfForLoop")==1) {
            forLoopLevelSize--;
            exitForLoopLevelSize--;

            //endOfSecondStatement
            int t9 = 0;
            while (topIFSize[forLoopLevelSize] > t9) {
                strcpy(top[t9], topIF[forLoopLevelSize][t9]);
                t9++;
            }
            topSize = t9;
            while (topSize != 0 ) {
                gen();
            }
            
            if (ir!=NULL) { fprintf(ir, "%sgoto %s\n", tab_or_label, forLoopLevel[forLoopLevelSize]); }
            
            strcpy(tab_or_label, exitForLoopLevel[exitForLoopLevelSize]);
            strcat(tab_or_label, ": ");
            if (ir!=NULL) { fprintf(ir, "%s", tab_or_label); }
            strcpy(tab_or_label, "");
            memset(top, 0, sizeof top);
            topSize = 0;
        }
    
        else if (existInArray("switch")==1) {
            
            labelIndex++;
            switchLevel++;
            switchConditionInLevel[switchLevel] = labelIndex;
            labelIndex++;
            afterSwitchConditionInLevel[switchLevel] = labelIndex;
            memset(top, 0, sizeof top);
            topSize = 0;
        }
        else if (existInArray("switchExpression")==1) {
            
            strcpy(switchConditionVariableInLevel[switchLevel], top[topSize-2]);

            char lIndex[17];    // new label for start switch expression or "test" in the book
            sprintf(lIndex, "%d", switchConditionInLevel[switchLevel]);
            char new_l[2000];
            strcpy(new_l,"L");
            strcat(new_l,lIndex);
            if (ir!=NULL) { fprintf(ir, "    goto %s\n", new_l); }
            
            memset(top, 0, sizeof top);
            topSize = 0;
            
        }
        else if (existInArray("case") == 1) {
            strcpy(valueInEachCase[switchLevel][indexOfV[switchLevel]], top[topSize-1]);
            labelIndex++;
            
            char lIndex[17];    // new label for start switch expression or "test" in the book
            sprintf(lIndex, "%d", labelIndex);
            char new_l[2000];
            strcpy(new_l,"L");
            strcat(new_l,lIndex);
            
            strcpy(labelOfCasesInLevel[switchLevel][indexOfV[switchLevel]], new_l);
            indexOfV[switchLevel]++;
            
            strcpy(tab_or_label, new_l);
            strcat(tab_or_label, ": ");
            
            memset(top, 0, sizeof top);
            topSize = 0;
        }
        else if (existInArray("finishThisCase") == 1) {
            char lIndex[17];
            sprintf(lIndex, "%d", afterSwitchConditionInLevel[switchLevel]);
            char new_l[2000];
            strcpy(new_l,"L");
            strcat(new_l,lIndex);
            if (ir!=NULL) { fprintf(ir, "    goto %s\n", new_l); }

            memset(top, 0, sizeof top);
            topSize = 0;
        }
        else if (existInArray("otherwise") == 1) {
            strcpy(valueInEachCase[switchLevel][indexOfV[switchLevel]], "Otherwise");
            labelIndex++;
            
            char lIndex[17];    // new label for start switch expression or "test" in the book
            sprintf(lIndex, "%d", labelIndex);
            char new_l[2000];
            strcpy(new_l,"L");
            strcat(new_l,lIndex);
            
            strcpy(labelOfCasesInLevel[switchLevel][indexOfV[switchLevel]], new_l);
            indexOfV[switchLevel]++;
            
            strcpy(tab_or_label, new_l);
            strcat(tab_or_label, ": ");
            
            memset(top, 0, sizeof top);
            topSize = 0;
        }
        else if (existInArray("finishSwitch")==1) {
            char lIndex[17];
            sprintf(lIndex, "%d", afterSwitchConditionInLevel[switchLevel]);
            char new_l[2000];
            strcpy(new_l,"L");
            strcat(new_l,lIndex);
            if (ir!=NULL) { fprintf(ir, "    goto %s\n", new_l); }

            char laIndex[17];
            sprintf(laIndex, "%d", switchConditionInLevel[switchLevel]);
            char new_la[2000];
            strcpy(tab_or_label,"L");
            strcat(tab_or_label, laIndex);
            strcat(tab_or_label, ": ");

            int i = 0;
            while (i < indexOfV[switchLevel]) {
                if (i+1== indexOfV[switchLevel]) {
                    if (ir!=NULL) { fprintf(ir, "%sgoto %s\n", tab_or_label, labelOfCasesInLevel[switchLevel][i]); }

                }
                else {
                    if (ir!=NULL) { fprintf(ir, "%sif %s == %s goto %s\n", tab_or_label, switchConditionVariableInLevel[switchLevel], valueInEachCase[switchLevel][i], labelOfCasesInLevel[switchLevel][i]); }

                }
                strcpy(tab_or_label, "    ");
                i++;
            }
            
            char lbIndex[17];
            sprintf(lbIndex, "%d", afterSwitchConditionInLevel[switchLevel]);
            char new_lb[2000];
            strcpy(new_lb,"L");
            strcat(new_lb,lbIndex);
            if (ir!=NULL) { fprintf(ir, "%s:\n", new_lb); }

            
            indexOfV[switchLevel] = 0;
            afterSwitchConditionInLevel[switchLevel] = 0;
            switchConditionInLevel[switchLevel] = 0;
            strcpy(switchConditionVariableInLevel[switchLevel], "");
            memset(valueInEachCase[switchLevel], 0, sizeof valueInEachCase[switchLevel]);
            memset(labelOfCasesInLevel[switchLevel], 0, sizeof labelOfCasesInLevel[switchLevel]);
            switchLevel-=1;

            memset(top, 0, sizeof top);
            topSize = 0;
        }
        else if (existInArray("initWhile")==1) {
            noOfWhile++;
            whileLevel++;
            whileCount[whileLevel] = noOfWhile;
            
            char lIndex[17];    // New label for jump statements
            sprintf(lIndex, "%d", noOfWhile);
            char new_l[2000];
            strcpy(new_l,"W");
            strcat(new_l,lIndex);
            
            strcpy(tab_or_label, new_l);
            strcat(tab_or_label, ": ");
            if (ir!=NULL) { fprintf(ir, "%s\n", tab_or_label); }
            strcpy(tab_or_label, "    ");
            
            memset(top, 0, sizeof top);
            topSize = 0;
        }
        else if (existInArray("while")==1) {
            int noNeedToConcat = 0;
            int m = 0;
            int temp1 = noOfWhile + 1;
            int temp2 = noOfWhile + 2;
            
            noOfWhile++;
            char l2Index[17];    // New label for code within the loop
            sprintf(l2Index, "%d", noOfWhile);
            char new_l2[2000];
            strcpy(new_l2,"W");
            strcat(new_l2,l2Index);
            
            noOfWhile++;
            char l3Index[17];    // New empty label to end loop
            sprintf(l3Index, "%d", noOfWhile);
            char new_l3[2000];
            strcpy(new_l3,"W");
            strcat(new_l3,l3Index);
            
            whileLevel2++;
            whileCount2[whileLevel2] = noOfWhile;    // Stores the current number of while into a different array (To jump to empty label to end loop)

            char rule[2000];    // Rule concatenation
            strcpy(rule,"");
            
            strcpy(tab_or_label, "    ");    // Space to put Label
            
            while (m != topSize) {
                if (strcmp("|",top[m])==0){     // Check for OR condition
                    if (ir!=NULL) { fprintf(ir, "%sif %s goto %s\n", tab_or_label, rule, new_l2); }

                    strcpy(rule, "");
                    noNeedToConcat = 1;
                    
                }
                else if (strcmp("&",top[m])==0){        // Check for AND condition
                    if (ir!=NULL) { fprintf(ir, "%sifFalse %s goto %s\n", tab_or_label, rule, new_l3); }

                    strcpy(rule, "");
                    noNeedToConcat = 1;
                    
                }
                else if (strcmp("while",top[m])==0){     // End of WHILE condition
                    char l4Index[17];    // New label for last jump statement
                    sprintf(l4Index, "%d", noOfWhile);
                    char new_l4[2000];
                    strcpy(new_l4,"W");
                    strcat(new_l4,l4Index);
                    
                    if (ir!=NULL) { fprintf(ir, "%sifFalse %s goto %s\n", tab_or_label, rule, new_l4); }

                    strcpy(rule, "");
                    
                    strcpy(tab_or_label, new_l2);    // Print label for code within WHILE
                    strcat(tab_or_label, ": ");
                    if (ir!=NULL) { fprintf(ir, "%s\n", tab_or_label); }

                    strcpy(tab_or_label, "    ");
                    noNeedToConcat = 1;
                    memset(top, 0, sizeof top);
                    topSize = 0;
                    break;
                    
                }
                
                if (noNeedToConcat == 0) {
                    strcat(rule,top[m]);
                    
                }
                noNeedToConcat = 0;
                m+=1;
                
            }
            
        }
        else if (existInArray("endWhile")==1) {
            char lIndex[17];    // New label to allow loop
            sprintf(lIndex, "%d", whileCount[whileLevel]);
            char new_l[2000];
            strcpy(new_l,"W");
            strcat(new_l,lIndex);
            if (ir!=NULL) { fprintf(ir, "    goto %s\n", new_l); }

            
            whileLevel--;    // Decrements the counter of the first array
            
            noOfWhile++;
            char l1Index[17];    // New empty label after the end of loop
            sprintf(l1Index, "%d", whileCount2[whileLevel2]);
            char new_l2[2000];
            strcpy(new_l2,"W");
            strcat(new_l2,l1Index);
            
            whileLevel2--;    // Decrements the counter of the second array
            
            strcpy(tab_or_label, new_l2);
            strcat(tab_or_label, ": ");
            if (ir!=NULL) { fprintf(ir, "%s\n", tab_or_label); }

            strcpy(tab_or_label, "    ");
            
            memset(top, 0, sizeof top);
            topSize = 0;
            
        }
        else {
            tIndexNum-=1;
            
        }
}

void put(char *new_t) {
    strcpy(top[topSize], new_t);
    topSize++;

}

int existInArray (char *x) {
    int m = topSize;
    while (m != -1) {
        if (strcmp(x, top[m])==0) {
            return 1;
        }
        m-=1;
    }
    return 0;
}

void open_IRfile(){
    ir = fopen("prog.ir", "w");
}

/*  The function that is used to add the Node width according to its type as type has
 different sizes:
 Integer:   4 bytes = 32 bits
 Real:      8 bytes = 64 bits
 Boolean:   1 byte = 8 bits
 Character: 1 byte = 8 bits
 String:    4 bytes for storing the size as an integer + string length
 atype:     (number of dimensions * 4 bytes for storing the size of that dimension) + (size mentioned in reserve function * type_size)
 rtype:     sum of all the types inside [...]
 
 */
void assign_width(char *name, char *t, int stringLength) {
    /*
     'name' is the Node name;
     't' is the type inside record type;
     for example:    type rec = [integer: i, string: x],
     't' is either i or x, depends on which one is pasted
     'stringLength' is used for the node type string
     */
    
    if (find_node2(name) != NULL) {
        char *type = get_type(name);
        char *ext = get_extra(name);
        if ((ext!="atype") && (ext!="ftype") && (ext!="rtype")) {
            if (strcmp(type, "integer") == 0) {             // Integer: 4 Bytes
                find_node2(name)->width = sizeof(int);
            }
            else if (strcmp(type, "real") == 0) {           // Real: 8 Bytes
                find_node2(name)->width = sizeof(double);
            }
            else if (strcmp(type, "Boolean") == 0) {        // Boolean: 1 Byte
                find_node2(name)->width = sizeof(bool);
            }
            else if (strcmp(type, "character") == 0) {      // Character: 1 Byte
                find_node2(name)->width = sizeof(char);
            }
            else if (strcmp(type, "string") == 0) {         // String
                find_node2(name)->width = ( 4 + stringLength -2); // 4 bytes + string length - 2, where 2 is for 2 '\"' sign of the string
            }
        }
        else if (strcmp(ext, "atype")==0) {
            int arrSize = find_node2(name)->asize;
            if (strcmp(type, "integer") == 0) {
                find_node2(name)->width = arrSize*sizeof(int);
            }
            else if (strcmp(type, "real") == 0) {
                find_node2(name)->width = arrSize*sizeof(double);
            }
            else if (strcmp(type, "Boolean") == 0) {
                find_node2(name)->width = arrSize*sizeof(bool);
            }
            else if (strcmp(type, "character") == 0) {
                find_node2(name)->width = arrSize*sizeof(char);
            }
            else if (strcmp(type, "string") == 0) {
                if (stringLength == 0){
                    find_node2(name)->width = arrSize * 4;
                } else {
                    find_node2(name)->width = arrSize * (4 + stringLength - 2);
                }
            }
            else {
                find_node2(name)->width = arrSize*4;
            }
        }
    }
}

void assign_width_atype(char *name, int arrSize) {
    if (find_node2(name)!=NULL) {
        char *type = get_type(name);
        char *type2 = get_type(type);
        find_node2(name)->width = (find_node2(type)->asize*4) + (arrSize*(find_node2(type)->width)/find_node2(type)->asize);
    }
}

int assign_width_rtype(char *type) {                                        // <--------------------- 04/18/19
    if (strcmp(type, "integer") == 0) {             // Integer: 4 Bytes
        return sizeof(int);
    }
    else if (strcmp(type, "real") == 0) {           // Real: 8 Bytes
        return sizeof(double);
    }
    else if (strcmp(type, "Boolean") == 0) {        // Boolean: 1 Byte
        return sizeof(bool);
    }
    else if (strcmp(type, "character") == 0) {      // Character: 1 Byte
        return sizeof(char);
    }
    else if (strcmp(type, "string") == 0) {         // String
        return 4;
    }
    else {                                          //  <----------------------------- 05/06/18
        return 4;                                   //  <----------------------------- 05/06/18
    }                                               //  <----------------------------- 05/06/18
}

struct Node* find_node2(char *n) {
    struct Node *node = head;
    while (node != NULL){
        if (strcmp(node->name, n)==0) return node;
        else node = node->next;
    }
    return NULL;
}

void assign_address(char *name) {
    find_node2(name)->addr = offset;
    offset = offset + find_node2(name)->width;
}

void add_parameter_to_type(char *name, char *new_parameter){
    if (find_node2(name) != NULL) {
        strcpy(find_node2(name)->parameter[find_node2(name)->parameterSize], new_parameter);
        find_node2(name)->parameterSize++;
    }
}

// New Functions
void insert_t_variabel(char *n, int w) {
    struct TVariable* new = (struct TVariable*) malloc(sizeof(struct TVariable));
    new->name  = n;
    new->wid = w;
    if (tSize == 0) {
        tHead = (struct TVariable*) malloc(sizeof(struct TVariable));
    } else {
        new->next = tHead;
    }
    tHead = new;
    tSize++;
}

struct TVariable* find_t_variabel(char *n) {
    struct TVariable* t = tHead;
    while (t != NULL){
        if (strcmp(t->name, n)==0) {
            return t;
        }
        else t = t->next;
    }
    return NULL;
}

// <------------------- 05/06/18 Function for array to store the initial value
void assign_val(char *arrayName, char val[99]) {
    if (find_node2(arrayName) != NULL) { strcpy(find_node2(arrayName)->val, val); }
}
