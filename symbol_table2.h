#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

#include <stdbool.h>

//GLOBAL VARIABLES
struct Node;
extern struct Node *head; // actual symbol table
extern int size; // tracks size of table
struct Type;
extern int offset;
extern int topSize;
extern char top [999][2000];

struct TVariable;                // <-----------------------------------------------
extern int tSize;                // <-----------------------------------------------

//FUNCTIONS WHICH ADD TO LIST
void init(char *, int, char *, char *); // initialization of list, handled locally
void insert_symbol(char *, int, char *, char *); // insert symbol into list
void make_symbol(char *, int, char *, char *); // make a new symbol, using global variable

//ACCESS FUNCTIONS FOR TABLE
struct Node* find_node(char *); // find node in list
bool exists(char *); // checks if symbol exists
bool type_exists(char *); // checks if type exists or has been declared for function
bool is_ftype(char *);
bool is_atype(char *);
bool is_rtype(char *);
bool is_function(char *); // checks if node is a function
int get_array_size(char *); // returns size of an array
char * get_type(char *);
char * get_return_type(char *);
char *get_extra(char *);
void list_var_names(char *);
int get_pos(char *, char *);

//MUTATOR FUNCTIONS FOR TABLE
void change_type(char *, char *); 
void change_name(char *, char *); // change name of node in the list
void change_scope(char *); // increments scope of symbol
void change_extra(char *, char *); // updates extra
void change_return_type(char *, char *);
void add_array_size(char *, int); // writes an array size
void add_to_type(char *, char *, char *); // used to set type to multiple types

// FUNCTION FOR "-st" OPTION
void write_tofile(); // writes symbol table to file

void assign_width(char *, char *, int); // <---------------------------------I DEFINE-----------------------------------------
void assign_width_atype(char *, int);// <---------------------------------I DEFINE-----------------------------------------
struct Node* find_node2(char *); // <---------------------------------I DEFINE-----------------------------------------
char *get_extra(char *); // <---------------------------------I DEFINE-----------------------------------------
void put(char *); // <---------------------------------I DEFINE-----------------------------------------
void gen(); // <---------------------------------I DEFINE-----------------------------------------
void assign_address(char *); // <---------------------------------I DEFINE-----------------------------------------
int existInArray(char *); // <---------------------------------I DEFINE-----------------------------------------
void add_parameter_to_type(char *, char *); // <---------------------------------I DEFINE-----------------------------------------
void insert_t_variabel(char *, int);                // <-----------------------------------------------
struct TVariable* find_t_variabel(char *);                // <-----------------------------------------------
int assign_width_rtype(char *); // <----------------------- 04/18/18
int get_pos_byte(char *, char *);  // <----------------------- 04/18/18
void assign_val(char *, char *);  // <----------------------- 05/06/18
#endif
