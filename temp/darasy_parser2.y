%{
    #include <stdio.h>
    #include <stdbool.h>
    #include <string.h>
    
    int yylex();
    
    char *dtype[99];
    int dtypeL = 0;
    char *dvar[99];
    int dvarL = 0;
    
    void yyerror(char* p);
    
    struct Node {
        int i;
        char* s;
        bool b;
        char ch;
        float f;
    };
    
    void checkTYPEDeclared (char *inp) {
        bool exist = false;
        for (int i=0; i<dtypeL; i++){
            if (strcmp(dtype[i], inp)==0) {exist = true; break;}
        }
        if (exist == false) {
            printf("** ERROR: the name '%s', used here as a type, has not been declared at this point in the program.\n", inp);
        }
    }
    
    void checkVARDeclared (char *inp) {
        bool exist = false;
        for (int i=0; i<dvarL; i++){
            if (strcmp(dvar[i], inp)==0) {exist = true; break;}
        }
        if (exist == false) {
            printf("** ERROR: the name '%s', used here as a variable name, has not been declared at this point in the program.\n", inp); }
    }
    
    void checkTYPEExist(char *inp) {
        bool exist = false;
        if (dtypeL == 0) {dtype[dtypeL] = inp; dtypeL = 1;}
        else {
            for (int i=0; i<dtypeL; i++){
                if (strcmp(dtype[i], inp)==0) {exist = true; break;}
            }
            if (exist == false) {dtype[dtypeL] = inp; dtypeL++;}
        }
    }
    
    void checkVARExist(char *inp) {
        bool exist = false;
        for (int i=0; i<dvarL; i++){
            if (strcmp(dvar[i], inp)==0) {exist = true; break;}
        }
        if (exist == false) {dvar[dvarL] = inp; dvarL++;}
    }
    
    // Check if both x and y are integers
    bool type_int(double x, double y) {
        int i = (int)x;
        int j = (int)y;
        return ((x==i) && (y==j));
    };
    
    %}

/* Bison declarations */

%token ID 101

/*type names*/
%token <s> T_INTEGER 201
%token <s> T_REAL 202
%token <s> T_BOOLEAN 203
%token <s> T_CHARACTER 204
%token <s> T_STRING 205

/*constants*/
%token C_INTEGER 301
%token C_REAL 302
%token C_CHARACTER 303
%token C_STRING 304
%token C_TRUE 305
%token C_FALSE 306

/*keywords*/
%token NULL_PTR 401
%token RESERVE 402
%token RELEASE 403
%token FOR 404
%token WHILE 405
%token IF 406
%token THEN 407
%token ELSE 408
%token SWITCH 409
%token CASE 410
%token OTHERWISE 411
%token TYPE 412
%token FUNCTION 413
%token CLOSURE 414

/*punctuation - grouping*/
%token L_PARANTHESIS 501
%token R_PARANTHESIS 502
%token L_BRACKET 503
%token R_BRACKET 504
%token L_BRACE 505
%token R_BRACE 506
%token S_QUOTE 507
%token D_QUOTE 508

/*other punctuation*/
%token SEMI_COLON 551
%token COLON 552
%token COMMA 553
%token ARROW 554
%token BACKSLASH 555

/*operators*/
%token ADD 601
%token SUB_OR_NEG 602
%token MUL 603
%token DIV 604
%token REM 605
%token DOT 606
%token LESS_THAN 607
%token EQUAL_TO 608
%token ASSIGN 609
%token INT2REAL 610
%token REAL2INT 611
%token IS_NULL 612
%token NOT 613
%token AND 614
%token OR 615

%token COMMENT 700

%union{
    int i;
    bool b;
    char* s;
    char ch;
    struct Node n;
    float f;
}

/* Define operator precedence */
%right ASSIGN DOT/* lowest precedence */
%left INT2REAL REAL2INT
%left AND OR LESS_THAN EQUAL_TO NOT IS_NULL
%left ADD SUB_OR_NEG
%left MUL DIV REM /* Highest precedence */
%left L_PARANTHESIS R_PARANTHESIS
/* Define types of tokens */

%type <s> ID
%type <i> C_INTEGER
%type <f> C_REAL
%type <b> C_TRUE
%type <b> C_FALSE
%type <ch> C_CHARACTER
%type <s> C_STRING
%type <s> COMMENT

%type <s> definition
%type <s> identifier
%type <n> constant
%type <n> constant_no_id
%type <s> parameter_declaration
%type <s> parameter_declaration_id
%type <s> non_empty_parameter_list
%type <s> parameter_list
%type <s> definition_option1
%type <s> pblock
%type <s> declaration
%type <s> declaration_list
%type <s> identifier_list
%type <s> assignable
%type <n> expression


/* Where the grammar starts */
%start program

%%

program:   definition_list sblock
;

definition_list:
| definition   definition_list
;

definition: TYPE identifier COLON definition_option1
|   FUNCTION identifier { dvar[dvarL] = $2; dvarL++; } COLON function_id2 sblock
;

function_id2: identifier
;

definition_option1: dblock
|  constant ARROW identifier definition_option2
|  pblock ARROW identifier { if (strcmp($1, $3)==0) { checkTYPEExist($1);}}
;

definition_option2:
|  COLON L_PARANTHESIS constant R_PARANTHESIS
;

sblock: L_BRACE   statement_list   R_BRACE
| L_BRACE   dblock   statement_list   R_BRACE
;

dblock: L_BRACKET declaration_list R_BRACKET
;

declaration_list: declaration SEMI_COLON declaration_list
| declaration
;

declaration: identifier { checkTYPEDeclared($1);} COLON identifier_list
;

identifier_list: identifier { checkVARExist($1);}
|   identifier COMMA identifier_list
|   identifier ASSIGN constant
|   identifier ASSIGN constant COMMA identifier_list { checkVARExist($1);}
;

statement_list: statement statement_list
|   statement
;

statement: FOR L_PARANTHESIS statement SEMI_COLON expression SEMI_COLON statement R_PARANTHESIS sblock
|   WHILE L_PARANTHESIS expression R_PARANTHESIS
|   IF L_PARANTHESIS expression R_PARANTHESIS THEN sblock ELSE sblock
|   SWITCH L_PARANTHESIS expression R_PARANTHESIS case_singular OTHERWISE COLON sblock
|   sblock
|   assignable  {checkVARDeclared($1);} ASSIGN expression SEMI_COLON
|   memop assignable SEMI_COLON
;

case_singular: CASE constant COLON sblock case_plural
;

case_plural:
|   case_singular
;

assignable: identifier
|   assignable ablock
|   assignable DOT identifier
;

expression: SUB_OR_NEG expression
|   NOT expression
|   INT2REAL expression
|   REAL2INT expression
|   expression IS_NULL
|   assignable
|   expression ADD expression
|   expression SUB_OR_NEG expression
|   expression MUL expression
|   expression DIV expression
|   expression REM expression
|   expression AND expression
|   expression OR expression
|   expression LESS_THAN expression
|   expression EQUAL_TO expression
|   L_PARANTHESIS expression R_PARANTHESIS
|   constant_no_id
;

constant_no_id: C_INTEGER   {$$.i = $1;}
|   C_REAL  {$$.f = $1;}
|   C_TRUE  {$$.b = $1;}
|   C_FALSE {$$.b = $1;}
|   C_CHARACTER {$$.ch = $1;}
|   C_STRING    {$$.s = $1;}
;

pblock: L_PARANTHESIS parameter_list R_PARANTHESIS {if ($2!=NULL) {$$ = $2;}}
;

parameter_list: {NULL;}
|   non_empty_parameter_list
;

non_empty_parameter_list:   parameter_declaration non_empty_parameter_list_option1
;

non_empty_parameter_list_option1:
|   COMMA non_empty_parameter_list
;

parameter_declaration: identifier COLON parameter_declaration_id { $$ = $1; }
;

parameter_declaration_id: identifier
;

ablock: L_PARANTHESIS arguement_list R_PARANTHESIS
;

arguement_list:
|   non_empty_arguement_list
;

non_empty_arguement_list:   expression
|   expression COMMA non_empty_arguement_list
;

memop:  RESERVE
|   RELEASE
;

constant: C_INTEGER {$$.i = $1;}
| C_REAL    {$$.f = $1;}
| C_TRUE    {$$.b = $1;}
| C_FALSE   {$$.b = $1;}
| C_CHARACTER   {$$.ch = $1;}
| C_STRING  {$$.s = $1;}
| identifier {$$.s = $1;}
;

identifier: ID {$$ = $1; }
| T_INTEGER {$$ = $1; }
| T_REAL {$$ = $1; }
| T_BOOLEAN {$$ = $1; }
| T_CHARACTER {$$ = $1; }
| T_STRING {$$ = $1; }
;


%%
/* Additional C code */
#include "lex.yy.c"

void yyerror(char* p){
    printf("ERROR: %d: %s\n", yylineno, p);
}

