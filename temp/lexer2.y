%{
/* This space is for C code, such as includes, definitions, functions, and
* variables.
*/

%}
/* This is for your definitions */
/* Any options you want for the lexer will be declared here also */

%option yylineno
%option noyywrap

%%

"integer"               { char* str = strdup(yytext); yylval.s = str; return T_INTEGER; };
"real"                  { char* str = strdup(yytext); yylval.s = str; return T_REAL; };
"Boolean"               { char* str = strdup(yytext); yylval.s = str; return T_BOOLEAN; };
"character"             { char* str = strdup(yytext); yylval.s = str; return T_CHARACTER; };
"string"                { char* str = strdup(yytext); yylval.s = str; return T_STRING; };

"reserve"               return RESERVE;
"release"               return RELEASE;
"for"                   return FOR;
"while"                 return WHILE;
"if"                    return IF;
"then"                  return THEN;
"else"                  return ELSE;
"switch"                return SWITCH;
"case"                  return CASE;
"otherwise"             return OTHERWISE;
"type"                  return TYPE;
"function"              return FUNCTION;

"i2r"                   return INT2REAL;
"r2i"                   return REAL2INT;
"isNull"                return IS_NULL;

"true"                  { yylval.b = true; return C_TRUE;};
"false"                 { yylval.b = false; return C_FALSE;};

("+"|"-")?[0-9]+                                                  { yylval.i = atoi(yytext); return C_INTEGER;};
("+"|"-")?[0-9]+"."[0-9]+(("e"|"E")("+"|"-")?[0-9]+)?             { yylval.f = atof(yytext); return C_REAL;};
\'(\\.|.)\'             { yylval.ch = yytext[1]; return C_CHARACTER;};

"("                     return L_PARANTHESIS;
")"                     return R_PARANTHESIS;
"{"                     return L_BRACE;
"}"                     return R_BRACE;
"["                     return L_BRACKET;
"]"                     return R_BRACKET;
"'"                     return S_QUOTE;
["]                     return D_QUOTE;

";"                     return SEMI_COLON;
":"                     return COLON;
","                     return COMMA;
"->"                    return ARROW;

"+"                     return ADD;
"-"                     return SUB_OR_NEG;
"*"                     return MUL;
"/"                     return DIV;
"%"                     return REM;

"."                     return DOT;
"<"                     return LESS_THAN;
"="                     return EQUAL_TO;
":="                    return ASSIGN;
"!"                     return NOT;
"&"                     return AND;
"|"                     return OR;

"(*"([^*]*[*]*)(([^*)][^*]*[*]*)*)"*)"  ;
[A-Za-z][A-Za-z0-9]*    { char* str = strdup(yytext); yylval.s = str; return ID;};
\"([^\n\"]|\\.)*\"      { char* str = strdup(yytext); yylval.s = str; return C_STRING; };

[ \t]*                  ;
[\n]                    ;


%%
