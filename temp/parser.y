%{
    #include <stdio.h>
    #include <stdbool.h>
    
    int yylex();
    
    void yyerror(char* p);
    
    struct Node {
        int i;
        char* s;
        bool b;
        char ch;
        float f;
    };
    
    %}
/* Bison declarations */

%token ID 101

/*type names*/
%token T_INTEGER 201
%token T_REAL 202
%token T_BOOLEAN 203
%token T_CHARACTER 204
%token T_STRING 205

/*constants*/
%token C_INTEGER 301
%token C_REAL 302
%token C_CHARACTER 303
%token C_STRING 304
%token C_TRUE 305
%token C_FALSE 306

/*keywords*/
%token NULL_PTR 401
%token RESERVE 402
%token RELEASE 403
%token FOR 404
%token WHILE 405
%token IF 406
%token THEN 407
%token ELSE 408
%token SWITCH 409
%token CASE 410
%token OTHERWISE 411
%token TYPE 412
%token FUNCTION 413
%token CLOSURE 414

/*punctuation - grouping*/
%token L_PARANTHESIS 501
%token R_PARANTHESIS 502
%token L_BRACKET 503
%token R_BRACKET 504
%token L_BRACE 505
%token R_BRACE 506
%token S_QUOTE 507
%token D_QUOTE 508

/*other punctuation*/
%token SEMI_COLON 551
%token COLON 552
%token COMMA 553
%token ARROW 554
%token BACKSLASH 555

/*operators*/
%token ADD 601
%token SUB_OR_NEG 602
%token MUL 603
%token DIV 604
%token REM 605
%token DOT 606
%token LESS_THAN 607
%token EQUAL_TO 608
%token ASSIGN 609
%token INT2REAL 610
%token REAL2INT 611
%token IS_NULL 612
%token NOT 613
%token AND 614
%token OR 615

%token COMMENT 700

%union{
    int i;
    bool b;
    char* s;
    char ch;
    struct Node n;
    float f;
}

/* Define operator precedence */
%right ASSIGN DOT/* lowest precedence */
%left INT2REAL REAL2INT
%left AND OR LESS_THAN EQUAL_TO NOT IS_NULL
%left ADD SUB_OR_NEG
%left MUL DIV REM /* Highest precedence */
%left L_PARANTHESIS R_PARANTHESIS
/* Define types of tokens */
%type <s> ID
%type <i> C_INTEGER
%type <f> C_REAL
%type <b> C_TRUE
%type <b> C_FALSE
%type <ch> C_CHARACTER
%type <s> C_STRING
%type <s> COMMENT

%type <n> constant
%type <n> expression

/* Where the grammar starts */
%start program

%%

program:  expression_list {printf("\n");}
| L_BRACKET {printf("[");} declaration_list R_BRACKET {printf("]\n");} expression_list {printf("\n");}
;

declaration_list: declaration
| declaration SEMI_COLON declaration_list
;

declaration: ID COLON identifier_list
;

identifier_list: ID
| ID ASSIGN constant
| ID COMMA identifier_list
| ID ASSIGN constant COMMA identifier_list
;

expression_list:
| expression SEMI_COLON {printf(";\n");} expression_list
| assignment SEMI_COLON {printf(";\n");} expression_list
| error SEMI_COLON {printf(";\n");} expression_list
;

expression: constant {$$.i = $1.i;}                                                    // constant
| SUB_OR_NEG {printf("-");} expression      {$$.i = -$3.i;}
| NOT {printf("!");} expression             {$$.b = -$3.b;}
| INT2REAL {printf(" i2r ");} expression    {$$.f = (double)$3.i;}
| REAL2INT {printf(" r2i ");} expression    {$$.i = (int)$3.f;}
| expression IS_NULL {printf(" isNull ");}
| expression ADD {printf(" + ");} expression {$$.i = $1.i + $4.i;}                      //  +
| expression SUB_OR_NEG {printf(" - ");} expression {$$.i = $1.i - $4.i;}               //  -
| expression MUL {printf(" * ");} expression {$$.i = $1.i * $4.i;}                      //  *
| expression DIV {printf(" / ");} expression {$$.i = $1.f / $4.i;}                      //  /
| expression REM {printf(" %% ");} expression                                           //  %
| expression AND {printf(" & ");} expression                                            // and
| expression OR {printf(" | ");} expression                                             // or
| expression LESS_THAN {printf(" < ");} expression                                      // less than
| expression EQUAL_TO {printf(" = ");} expression                                       // equal to
| L_PARANTHESIS {printf(" ( ");}  expression R_PARANTHESIS {printf(" ) ");} {$$ = $3;}  // ( expression )
;

constant:   ID {$$.s = $1; printf("%s", $1);}
| C_INTEGER {$$.i = $1; printf("%i", $1);}
| C_REAL {$$.f = $1; printf("%f", $1);}
| C_TRUE {$$.b = $1; printf("%s", $1 ? "true" : "false");}
| C_FALSE {$$.b = $1; printf("%s", $1 ? "true" : "false");}
| C_CHARACTER {$$.ch = $1; printf("%c", $1);}
| C_STRING {$$.s = $1; printf("%s", $1);}
| COMMENT {$$.s = $1; printf("%s", $1);}
;

assignment: ID ASSIGN {printf("%s := ", $1);} expression
;

%%
/* Additional C code */
#include "lex.yy.c"

void yyerror(char* p){
    printf("ERROR: %d: %s\n", yylineno, p);
}
