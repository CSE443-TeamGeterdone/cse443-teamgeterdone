#include <stdio.h>
#include "lex.yy.c"

extern int yylex();
extern int yylineno;
extern char* yytext;

int main(int argc, char **argv){
	if (argc == 1) {
  	prinf("no program to compile\n");
	return 1;
	}
	else {
  	FILE *inputfile = fopen(argv[1], "r");
	  yyset_in(inputfile);
	  yyparse();
	  return 0;
	}

}
