%{
#include <stdio.h>
#include <stdlib.h>

extern int yylex();
extern char* yytext;
void yyerror(char *msg);
%}

%union {
	float f;
	int i;
}
%token <f> NUM
%type <f> E T F

%%


S : E		{printf("%f\n", $1);}
  ;

E : E'+'T 	{$$ = $1 + $3;}
  | E'-'T	{$$ = $1 - $3;}
  | T		{$$ = $1;}
  ;

T : T '*' F	{$$ = $1 * $3;}
  | T '/' F	{$$ = $1 / $3;}
  | F		{$$ = $1;}
  ;

F : '(' E ')' 	{$$ = $2;}
  | '-' F	{$$ = -$2;}
  | NUM	{$$ = $1;}
  ;



%%

void yyerror(char *msg) {
	fprintf(stderr, "%s\n", msg);
	exit(1);
}

int main(int argc, char **argv) {
	FILE *inputfile = fopen(argv[1], "r");
	yyset_in(inputfile);
	
	yyparse();
	return 0;
}
