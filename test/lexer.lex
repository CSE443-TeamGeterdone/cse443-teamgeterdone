%{
#include <stdio.h>
#include <stdlib.h>

#include "calc.tab.h"

// identifier
#define ID 	101

// type names
#define T_INTEGER 	201
#define T_REAL 		202
#define T_BOOLEAN 	203
#define T_CHARACTER 	204
#define T_STRING 	205

// constants
#define C_INTEGER 	301
#define C_REAL 		302
#define C_CHARACTER 	303
#define C_STRING 	304
#define C_TRUE		305
#define C_FALSE 	306

// keywords
#define NULL_PTR 	401
#define RESERVE 	402
#define RELEASE 	403
#define FOR 		404
#define WHILE 		405
#define IF 		406
#define THEN 		407
#define ELSE 		408
#define SWITCH		409
#define CASE 		410
#define OTHERWISE 	411
#define TYPE 		412
#define FUNCTION	413
#define CLOSURE 	414

// punctuation - grouping
#define L_PARANTHESIS 	501
#define R_PARANTHESIS 	502
#define L_BRACKET 	503
#define R_BRACKET 	504
#define L_BRACE 	505
#define R_BRACE 	506
#define S_QUOTE 	507
#define D_QUOTE 	508

// punctuation - other
#define SEMI_COLON 	551
#define COLON 		552
#define COMMA 		553
#define ARROW 		554
#define BACKSLASH 	555

// operators
#define ADD 		601
#define SUB_OR_NEG 	602
#define MUL 		603
#define DIV 		604
#define REM 		605
#define DOT 		606
#define LESS_THAN 	607
#define EQUAL_TO 	608
#define ASSIGN 		609
#define INT2REAL 	610
#define REAL2INT 	611
#define IS_NULL 	612
#define NOT 		613
#define AND 		614
#define OR 		615

// comments
#define COMMENT 	700

int n_lines = 1;
int n_cols = 1;

%}

%option yylineno
%option noyywrap

%%

"integer"	{ printf("%3d %s %d %d\n", T_INTEGER, yytext, n_lines, n_cols); n_cols+=(strlen(yytext));} 
"real" 		{ printf("%3d %s %d %d\n", T_REAL, yytext, n_lines, n_cols); n_cols+=(strlen(yytext));} 
"Boolean" 	{ printf("%3d %s %d %d\n", T_BOOLEAN, yytext, n_lines, n_cols); n_cols+=(strlen(yytext));} 
"character" 	{ printf("%3d %s %d %d\n", T_CHARACTER, yytext, n_lines, n_cols); n_cols+=(strlen(yytext));} 
"string"	{ printf("%3d %s %d %d\n", T_STRING, yytext, n_lines, n_cols); n_cols+=(strlen(yytext));} 

[+-]?[0-9]+ 		{ printf("%3d %s %d %d\n", C_INTEGER, yytext, n_lines, n_cols); n_cols+=(strlen(yytext)); yylval.f = atof(yytext); return NUM;} 
[+-]?[0-9]+"."[0-9]+([eE][+-]?[0-9]+)?		{ printf("%3d %s %d %d\n", C_REAL, yytext, n_lines, n_cols); n_cols+=(strlen(yytext)); yylval.f = atof(yytext); return NUM;} 



\"([^\n\"]|\\.)*\" 		{ printf("%3d %s %d %d\n", C_STRING, yytext, n_lines, n_cols); n_cols+=(strlen(yytext));} 



\'(\\.|.)\' 	{ printf("%3d %s %d %d\n", C_CHARACTER, yytext, n_lines, n_cols); n_cols+=(strlen(yytext));} 
"true" 		{ printf("%3d %s %d %d\n", C_TRUE, yytext, n_lines, n_cols); n_cols+=(strlen(yytext));} 
"false" 	{ printf("%3d %s %d %d\n", C_FALSE, yytext, n_lines, n_cols);  n_cols+=(strlen(yytext));}

"null_ptr" 	{ printf("%3d %s %d %d\n", NULL_PTR , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"null" 		{ printf("%3d %s %d %d\n", NULL_PTR , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"reserve" 	{ printf("%3d %s %d %d\n", RESERVE , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"release" 	{ printf("%3d %s %d %d\n", RELEASE , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"for" 		{ printf("%3d %s %d %d\n", FOR , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"while" 	{ printf("%3d %s %d %d\n", WHILE , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"if" 		{ printf("%3d %s %d %d\n", IF , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"then" 		{ printf("%3d %s %d %d\n", THEN , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"else" 		{ printf("%3d %s %d %d\n", ELSE , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"switch"	{ printf("%3d %s %d %d\n", SWITCH , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"case" 		{ printf("%3d %s %d %d\n", CASE , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"otherwise" 	{ printf("%3d %s %d %d\n", OTHERWISE , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"type" 		{ printf("%3d %s %d %d\n", TYPE , yytext, yylineno, n_cols); n_cols+=(strlen(yytext));}
"function"	{ printf("%3d %s %d %d\n", FUNCTION , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"closure" 	{ printf("%3d %s %d %d\n", CLOSURE , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}

"(" 		{ printf("%3d %s %d %d\n", L_PARANTHESIS  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext)); return yytext[0];}
")" 		{ printf("%3d %s %d %d\n", R_PARANTHESIS  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext)); return yytext[0];}
"{" 		{ printf("%3d %s %d %d\n", L_BRACE , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"}" 		{ printf("%3d %s %d %d\n", R_BRACE , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"["		{ printf("%3d %s %d %d\n", L_BRACKET , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"]" 		{ printf("%3d %s %d %d\n", R_BRACKET , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"'" 		{ printf("%3d %s %d %d\n", S_QUOTE , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
["] 		{ printf("%3d %s %d %d\n", D_QUOTE , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}

";"		{ printf("%3d %s %d %d\n", SEMI_COLON  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
":"		{ printf("%3d %s %d %d\n", COLON  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
","		{ printf("%3d %s %d %d\n", COMMA  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"->"		{ printf("%3d %s %d %d\n", ARROW  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}


"\\"		{ printf("%3d %s %d %d\n", BACKSLASH  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}


"+"		{ printf("%3d %s %d %d\n", ADD  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext)); return yytext[0];}
"-"		{ printf("%3d %s %d %d\n", SUB_OR_NEG  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext)); return yytext[0];}
"*"		{ printf("%3d %s %d %d\n", MUL  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext)); return yytext[0];}
"/"		{ printf("%3d %s %d %d\n", DIV  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext)); return yytext[0];}
"%"		{ printf("%3d %s %d %d\n", REM  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}

"."		{ printf("%3d %s %d %d\n", DOT  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"<"		{ printf("%3d %s %d %d\n", LESS_THAN  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"="		{ printf("%3d %s %d %d\n", EQUAL_TO  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
":="		{ printf("%3d %s %d %d\n", ASSIGN  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"i2r"		{ printf("%3d %s %d %d\n", INT2REAL  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"r2i"		{ printf("%3d %s %d %d\n", REAL2INT  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"isNull"	{ printf("%3d %s %d %d\n", IS_NULL  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"!"		{ printf("%3d %s %d %d\n", NOT  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"&"		{ printf("%3d %s %d %d\n", AND  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}
"|"		{ printf("%3d %s %d %d\n", OR  , yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}

"(*"([^*]*[*]*)(([^*)][^*]*[*]*)*)"*)"	{ printf("%3d %s %d %d\n", COMMENT, yytext, n_lines, n_cols); n_cols+=(strlen(yytext));
		 for (int i=0; i<strlen(yytext);i++) { if (yytext[i]=='\n') { n_lines+=1;} }
		} //comment

[a-zA-Z_][a-zA-Z0-9_]* 	{ printf("%3d %s %d %d\n", ID, yytext, n_lines, n_cols); n_cols+=(strlen(yytext));}

\n 		{ n_cols = 1; n_lines++; }
		
" "*		n_cols+=strlen(yytext);
[\t]*		n_cols+=strlen(yytext);

%%
